import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared';
const routes: Routes = [
  {
    path: 'login', loadChildren: './login/login.module#LoginModule',
    pathMatch: 'full'
  },
  {
    path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: '', loadChildren: './layout/layout.module#LayoutModule',
    canActivate: [AuthGuard],
    pathMatch: 'prefix'
  },
  { path: '**', redirectTo: 'not-found' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
