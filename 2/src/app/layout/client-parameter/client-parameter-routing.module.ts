import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared';

import { ClientParamComponent } from './client-parameter.component';
const routes: Routes = [
  {
    path: '', component: ClientParamComponent,
    canActivate: [AuthGuard],
    pathMatch: 'full',
  },
  { path: '**', redirectTo: '/not-found' },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientParamRoutingModule { }
