import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { MainService } from '../main.service';
import { NgZone } from '@angular/core';
import { AlertSchema, Helper, ModalComponent, constants } from '../../shared';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { AddParamComponent } from './components/add-param/add-param.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-client-parameter',
  templateUrl: './client-parameter.component.html',
  styleUrls: ['./client-parameter.component.scss']
})
export class ClientParamComponent implements OnInit {
  @Output() notify: EventEmitter<Object> = new EventEmitter<Object>();
  public allParameters: any = [
    { name: 'qualification', id: 'qualification' },
    { name: 'designation', id: 'designation' },
    { name: 'identity-proof', id: 'identity_proof' },
    { name: 'student courses', id: 'student_course' },
    { name: 'working-at', id: 'working_at' },
    { name: 'consultation charges currency', id: 'consultation_currency' },
  ];
  constructor(
    public router: Router, private mainService: MainService, private helper: Helper, private dialogService: DialogService, sanitizer: DomSanitizer
  ) {
  }
  ngOnInit() {}
  showParam(index) {
    this.dialogService.addDialog(AddParamComponent, {
      paramData: this.allParameters[index],
      mainService: this.mainService
    })
    .subscribe((res: any) => {});
  }
}
