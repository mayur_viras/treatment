import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { ClientParamRoutingModule } from './client-parameter-routing.module';
import { ClientParamComponent } from './client-parameter.component';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { AddParamComponent } from './components/add-param/add-param.component';
import { LoaderModule, AlertModule, AuthService } from '../../shared';
import { FileUploadModule } from 'ng2-file-upload';
import { MainService } from '../main.service';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
@NgModule({
  imports: [
    AlertModule,
    CommonModule,
    FormsModule,
    AngularMultiSelectModule,
    ChartsModule,
    FileUploadModule,
    ClientParamRoutingModule,
    LoaderModule,
    ReactiveFormsModule,
    BootstrapModalModule.forRoot({ container: document.body })
  ],
  declarations: [
    ClientParamComponent,
    ConfirmComponent,
    AddParamComponent,
  ],
  entryComponents: [
    ConfirmComponent,
    AddParamComponent,
  ],
  providers: [
    MainService,
    AuthService,
  ],
})
export class ClientParamModule { }
