import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { DialogService, DialogComponent } from 'ng2-bootstrap-modal';
import { MainService } from '../../../main.service';
import { NgZone } from '@angular/core';
import { ConfirmComponent } from '../confirm/confirm.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-add-param',
  templateUrl: './add-param.component.html',
  styleUrls: ['./add-param.component.scss']
})
export class AddParamComponent extends DialogComponent<any, any> implements OnInit {
  @Output() notify: EventEmitter<Object> = new EventEmitter<Object>();
  public alerts: Array<any> = [];
  public allParamData: any = [];
  public param = '';
  public paramData;
  constructor(
    public router: Router, private mainService: MainService, public dialogService: DialogService, sanitizer: DomSanitizer
  ) {
    super(dialogService);
  }
  ngOnInit() {
    this.getAllParam();
  }
  addNewParam() {
    this.mainService.addNewParam(this.paramData.id, this.param)
    .subscribe((res) => {
      if (res.body.isError) {
        alert(res.body.message);
      } else {
        this.param = '';
        this.closeEdit();
        this.allParamData.push(res.body.data.param);
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  removeParam(index) {
    this.dialogService.addDialog(ConfirmComponent, { message: 'Are you sure you want to remove this param?' }).subscribe((res: any) => {
      if (res) {
        this.mainService.removeParam(this.paramData.id, this.allParamData[index].id)
        .subscribe((res) => {
          if (res.body.isError) {
            alert(res.body.message);
          } else {
            this.allParamData.splice(index, 1);
          }
        }, (error) => {
          this.mainService.notLoggedIn(error);
          console.log(error, 'errr');
        });
      }
    });
  }
  public edit_id;
  public edit_name;
  editParam(i) {
    this.edit_id = this.allParamData[i].id;
    this.edit_name = this.allParamData[i].name;
  }
  closeEdit() {
    this.edit_id = '';
    this.edit_name = '';
  }
  saveEdit(i) {
    this.mainService.editParam(this.paramData.id, this.edit_id, this.edit_name)
    .subscribe((res) => {
      if (res.body.isError) {
        alert(res.body.message);
      } else {
        this.allParamData[i].name = this.edit_name;
        this.closeEdit();
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  getAllParam() {
    this.mainService.getAllParam(this.paramData.id)
    .subscribe((res) => {
      this.allParamData = [];
      if (res.body.isError) {
        alert(res.body.message);
      } else {
        this.allParamData = res.body.data.allParams || [];
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
}
