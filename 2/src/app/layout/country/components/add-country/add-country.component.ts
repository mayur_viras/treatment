import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { DialogService, DialogComponent } from 'ng2-bootstrap-modal';
import { MainService } from '../../../main.service';
import { NgZone } from '@angular/core';
import { ConfirmComponent } from '../confirm/confirm.component';
import { DomSanitizer } from '@angular/platform-browser';
import { CityComponent } from '../city/city.component';

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.scss']
})
export class AddCountryComponent extends DialogComponent<any, any> implements OnInit {
  public isContentLoading = false;
  @Output() notify: EventEmitter<Object> = new EventEmitter<Object>();
  public alerts: Array<any> = [];
  public allState: any = [];
  public state = '';
  public country;
  constructor(
    public router: Router, private mainService: MainService, public dialogService: DialogService, sanitizer: DomSanitizer
  ) {
    super(dialogService);
    this.router.events.subscribe((event: object) => {
      this.isContentLoading = (event instanceof NavigationStart) ? true : this.isContentLoading;
      this.isContentLoading = (event instanceof NavigationEnd) ? false : this.isContentLoading;
    });
  }
  ngOnInit() {
    this.getAllState();
  }
  addNewState() {
    this.mainService.addNewState(this.state, this.country.id)
    .subscribe((res) => {
      if (res.body.isError) {
        alert(res.body.message);
      } else {
        this.state = '';
        this.closeEdit();
        this.allState.push(res.body.data.state);
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  removeState(index) {
    this.dialogService.addDialog(ConfirmComponent, { message: 'Are you sure you want to remove this state?' }).subscribe((res: any) => {
      if (res) {
        this.mainService.removeState(this.allState[index].id)
        .subscribe((res) => {
          if (res.body.isError) {
            alert(res.body.message);
          } else {
            this.allState.splice(index, 1);
          }
        }, (error) => {
          this.mainService.notLoggedIn(error);
          console.log(error, 'errr');
        });
      }
    });
  }
  public edit_id;
  public edit_name;
  editState(i) {
    this.edit_id = this.allState[i].id;
    this.edit_name = this.allState[i].name;
  }
  closeEdit() {
    this.edit_id = '';
    this.edit_name = '';
  }
  saveEdit(i) {
    this.mainService.editState(this.edit_id, this.edit_name)
    .subscribe((res) => {
      if (res.body.isError) {
        alert(res.body.message);
      } else {
        this.allState[i].name = this.edit_name;
        this.closeEdit();
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  getAllState() {
    this.mainService.getAllState(this.country.id)
    .subscribe((res) => {
      this.allState = [];
      if (res.body.isError) {
        alert(res.body.message);
      } else {
        this.allState = res.body.data.allStates || [];
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  cities(index: number) {
    this.dialogService.addDialog(CityComponent, {
      state: this.allState[index],
      mainService: this.mainService
    })
    .subscribe((res: any) => {});
  }
}
