import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { DialogService, DialogComponent } from 'ng2-bootstrap-modal';
import { MainService } from '../../../main.service';
import { NgZone } from '@angular/core';
import { ConfirmComponent } from '../confirm/confirm.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent extends DialogComponent<any, any> implements OnInit {
  @Output() notify: EventEmitter<Object> = new EventEmitter<Object>();
  public alerts: Array<any> = [];
  public allCity: any = [];
  public city = '';
  public state;
  constructor(
    public router: Router, private mainService: MainService, public dialogService: DialogService, sanitizer: DomSanitizer
  ) {
    super(dialogService);
  }
  ngOnInit() {
    this.getAllCity();
  }
  addNewCity() {
    this.mainService.addNewCity(this.city, this.state.id)
    .subscribe((res) => {
      if (res.body.isError) {
        alert(res.body.message);
      } else {
        this.city = '';
        this.closeEdit();
        this.allCity.push(res.body.data.city);
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  removeCity(index) {
    this.dialogService.addDialog(ConfirmComponent, { message: 'Are you sure you want to remove this city?' }).subscribe((res: any) => {
      if (res) {
        this.mainService.removeCity(this.allCity[index].id)
        .subscribe((res) => {
          if (res.body.isError) {
            alert(res.body.message);
          } else {
            this.allCity.splice(index, 1);
          }
        }, (error) => {
          this.mainService.notLoggedIn(error);
          console.log(error, 'errr');
        });
      }
    });
  }
  public edit_id;
  public edit_name;
  editCity(i) {
    this.edit_id = this.allCity[i].id;
    this.edit_name = this.allCity[i].name;
  }
  closeEdit() {
    this.edit_id = '';
    this.edit_name = '';
  }
  saveEdit(i) {
    this.mainService.editCity(this.edit_id, this.edit_name)
    .subscribe((res) => {
      if (res.body.isError) {
        alert(res.body.message);
      } else {
        this.allCity[i].name = this.edit_name;
        this.closeEdit();
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  getAllCity() {
    this.mainService.getAllCity(this.state.id)
    .subscribe((res) => {
      this.allCity = [];
      if (res.body.isError) {
        alert(res.body.message);
      } else {
        this.allCity = res.body.data.allCity || [];
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
}
