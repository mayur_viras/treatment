import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared';

import { CountryComponent } from './country.component';
const routes: Routes = [
  {
    path: '', component: CountryComponent,
    canActivate: [AuthGuard],
    pathMatch: 'full',
  },
  { path: '**', redirectTo: '/not-found' },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountryRoutingModule { }
