import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { MainService } from '../main.service';
import { NgZone } from '@angular/core';
import { AlertSchema, Helper, ModalComponent, constants } from '../../shared';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { AddCountryComponent } from './components/add-country/add-country.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {
  public isContentLoading = false;
  @Output() notify: EventEmitter<Object> = new EventEmitter<Object>();
  public alerts: Array<any> = [];
  public allCountry: any = [];
  public country = '';
  constructor(
    public router: Router, private mainService: MainService, private helper: Helper, private dialogService: DialogService, sanitizer: DomSanitizer
  ) {
    this.router.events.subscribe((event: object) => {
      this.isContentLoading = (event instanceof NavigationStart) ? true : this.isContentLoading;
      this.isContentLoading = (event instanceof NavigationEnd) ? false : this.isContentLoading;
    });
    this.getAllCountry();
  }
  ngOnInit() {}
  addNewCountry() {
    this.mainService.addNewCountry(this.country)
    .subscribe((res) => {
      if (res.body.isError) {
        alert(res.body.message);
      } else {
        this.country = '';
        this.closeEdit();
        this.allCountry.push(res.body.data.country);
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  removeCountry(index) {
    this.dialogService.addDialog(ConfirmComponent, { message: 'Are you sure you want to remove this country, and all it\'s states?' }).subscribe((res: any) => {
      if (res) {
        this.mainService.removeCountry(this.allCountry[index].id)
        .subscribe((res) => {
          if (res.body.isError) {
            alert(res.body.message);
          } else {
            this.allCountry.splice(index, 1);
          }
        }, (error) => {
          this.mainService.notLoggedIn(error);
          console.log(error, 'errr');
        });
      }
    });
  }
  public edit_id;
  public edit_name;
  editCountry(i) {
    this.edit_id = this.allCountry[i].id;
    this.edit_name = this.allCountry[i].name;
  }
  closeEdit() {
    this.edit_id = '';
    this.edit_name = '';
  }
  saveEdit(i) {
    this.mainService.editCountry(this.edit_id, this.edit_name)
    .subscribe((res) => {
      if (res.body.isError) {
        alert(res.body.message);
      } else {
        this.allCountry[i].name = this.edit_name;
        this.closeEdit();
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  states(index) {
    this.dialogService.addDialog(AddCountryComponent, {
      country: this.allCountry[index],
      mainService: this.mainService
    })
    .subscribe((res: any) => {});
  }
  getAllCountry() {
    this.mainService.getAllCountry()
    .subscribe((res) => {
      this.allCountry = [];
      if (res.body.isError) {
        alert(res.body.message);
      } else {
        this.allCountry = res.body.data.allCountries || [];
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
}
