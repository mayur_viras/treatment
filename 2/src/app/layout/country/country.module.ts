import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { CountryRoutingModule } from './country-routing.module';
import { CountryComponent } from './country.component';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { AddCountryComponent } from './components/add-country/add-country.component';
import { CityComponent } from './components/city/city.component';
import { LoaderModule, AlertModule, AuthService } from '../../shared';
import { FileUploadModule } from 'ng2-file-upload';
import { MainService } from '../main.service';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
@NgModule({
  imports: [
    AlertModule,
    CommonModule,
    FormsModule,
    AngularMultiSelectModule,
    ChartsModule,
    FileUploadModule,
    CountryRoutingModule,
    LoaderModule,
    ReactiveFormsModule,
    BootstrapModalModule.forRoot({ container: document.body })
  ],
  declarations: [
    CountryComponent,
    ConfirmComponent,
    AddCountryComponent,
    CityComponent
  ],
  entryComponents: [
    ConfirmComponent,
    AddCountryComponent,
    CityComponent
  ],
  providers: [
    MainService,
    AuthService,
  ],
})
export class CountryModule { }
