import { OnInit, Component, ViewChild, ElementRef } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { constants } from '../../../../shared';
import { EditCategoryComponent } from '../edit-category/edit-category.component';
import { ConfirmComponent } from '../confirm/confirm.component';
import { MainService } from '../../../main.service'

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.scss']
})
export class CategoryDetailComponent extends DialogComponent<any, any> implements OnInit {
  public category: any;
  public allSpeechLanDomains: any;
  public allHearingScreeningTypes: any;
  public mainService;
  public categoryTypes = constants.CATEGORY_TYPES;
  public categoryIds = constants.CATEGORY_IDS;
  public imagePath: any;
  public imageContent: any;
  public audioPath: any;
  public audioContent: any;
  public imagePathEdit: any = {};
  public imageContentEdit: any = {};
  public audioPathEdit: any = {};
  public audioContentEdit: any = {};
  public correct_word = '';
  public correct_word_edit = '';
  public target_sound = '';
  public target_sound_edit = '';
  public areImagesLoaded = false;
  public allPictures: any;
  public editPictureId;
  public boxHeight = {};
  constructor(dialogService: DialogService, mainService: MainService) {
    super(dialogService);
  }
  ngOnInit() {
  }

  changeDecidingValue(value) {
    const decidingValue = (value == 1) ? this.category.decidingValue : this.category.decidingValue2;
    this.dialogService.addDialog(ConfirmComponent, {
      message: `Are you sure you want to change risk decider value?`
    }).subscribe((res: any) => {
      if (res) {
        this.mainService.changeDecidingValue(this.category.id, this.category.decidingValue, this.category.decidingValue2)
        .subscribe((res) => {
        }, (error) => {
          this.mainService.notLoggedIn(error);
        });
      } else {
        if (value == 1) {
          this.category.decidingValue = !this.category.decidingValue;
        }
        if (value == 2) {
          this.category.decidingValue2 = !this.category.decidingValue2;
        }
      }
    });
  }

  editPicture(index) {
    if (this.allPictures[index]) {
      this.editPictureId = this.allPictures[index].id;
      this.correct_word_edit = this.allPictures[index].correct_word;
      this.target_sound_edit = this.allPictures[index].target_sound;
      this.boxHeight[this.allPictures[index].id] = '400px';
      this.imagePathEdit = {};
      this.imageContentEdit = {};
      this.audioPathEdit = {};
      this.audioContentEdit = {};
    }
  }
  closeEditPicture(index) {
    this.editPictureId = '';
    this.correct_word_edit = '';
    this.target_sound_edit = '';
    this.boxHeight = {};
    this.imagePathEdit = {};
    this.imageContentEdit = {};
    this.audioPathEdit = {};
    this.audioContentEdit = {};
  }
  saveEditPicture(index) {
    this.mainService.editArticulationPicture(this.allPictures[index].id, this.correct_word_edit, this.target_sound_edit, this.imageContentEdit, this.audioContentEdit)
    .subscribe((res) => {
      if (!res.body.isError) {
        this.allPictures[index].correct_word = this.correct_word_edit;
        this.allPictures[index].target_sound = this.target_sound_edit;
        this.allPictures[index].audio_url = `${constants.PUBLIC_URL}${res.body.data.audio_url}`;
        this.allPictures[index].picture_url = `${constants.PUBLIC_URL}${res.body.data.picture_url}`;
        this.editPictureId = '';
        this.correct_word_edit = '';
        this.target_sound_edit = '';
        this.imagePathEdit = {};
        this.imageContentEdit = {};
        this.audioContentEdit = {};
        this.audioPathEdit = {};
      } else {
        alert(res.body.message);
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
    });
  }
  changeImageListener(event: any, isEdit = false, id): void {
    try {
      if (event.target.files && event.target.files[0]) {
        if (!event.target.files[0].type || !event.target.files[0].type.includes('image')) {
          throw new Error('seems like you have uploaded some non-image file !!');
        }
        if (isEdit) {
          this.imageContentEdit[id] = event['target'].files[0];
          const reader = new FileReader();
          reader.onload = (event2: any) => {
            this.imagePathEdit[id] = event2.target.result;
          };
          reader.readAsDataURL(this.imageContentEdit[id]);
        } else {
          this.imageContent = event['target'].files[0];
          const reader = new FileReader();
          reader.onload = (event2: any) => {
            this.imagePath = event2.target.result;
          };
          reader.readAsDataURL(this.imageContent);
        }
      }
    } catch (e) {
      window.scrollTo(0, 0);
      console.log(e, 'eeeee');
      // this.alerts.push({ type: 'danger', message: `${e.message || e.toString()}` });
    }
  }
  changeAudioListener(event: any, isEdit = false, id): void {
    try {
      if (event.target.files && event.target.files[0]) {
        if (!event.target.files[0].type || !event.target.files[0].type.includes('audio')) {
          throw new Error('seems like you have uploaded some non-audio file !!');
        }
        if (isEdit) {
          this.audioContentEdit[id] = event['target'].files[0];
          const reader = new FileReader();
          reader.onload = (event2: any) => {
            this.audioPathEdit[id] = event2.target.result;
          };
          reader.readAsDataURL(this.audioContentEdit[id]);
        } else {
          this.audioContent = event['target'].files[0];
          const reader = new FileReader();
          reader.onload = (event2: any) => {
            this.audioPath = event2.target.result;
          };
          reader.readAsDataURL(this.audioContent);
        }
      }
    } catch (e) {
      window.scrollTo(0, 0);
      console.log(e, 'eeeee');
      // this.alerts.push({ type: 'danger', message: `${e.message || e.toString()}` });
    }
  }

  addArticulationPictures() {
    if (this.imageContent && this.audioContent) {
      this.mainService.addArticulationPictures(this.imageContent, this.audioContent, this.correct_word, this.target_sound)
      .subscribe((res) => {
        if (res.body.isError) {
          console.log(res.body, 'errr1');
        } else {
          res.body.data.picture_url = `${constants.PUBLIC_URL}${res.body.data.picture_url}`;
          res.body.data.audio_url = `${constants.PUBLIC_URL}${res.body.data.audio_url}`;
          this.allPictures.push(res.body.data);
          this.imageContent = undefined;
          this.audioContent = undefined;
          this.imagePath = undefined;
          this.audioPath = undefined;
          this.correct_word = '';
          this.target_sound = '';
        }
      }, (error) => {
        this.mainService.notLoggedIn(error);
        console.log(error, 'errr');
      });
    }
  }


  @ViewChild('fileInputEdit') InputFrame: ElementRef;
  @ViewChild('fileInput') InputFrameEdit: ElementRef;

  clearImage(isEdit = false) {
    if (isEdit) {
      this.imagePathEdit = {};
      this.imageContentEdit = {};
      this.InputFrame.nativeElement.value = '';
    } else {
      this.imagePath = undefined;
      this.imageContent = undefined;
      this.InputFrameEdit.nativeElement.value = '';
    }
  }

  @ViewChild('fileInputAudioEdit') InputFrameAudio: ElementRef;
  @ViewChild('fileInputAudio') InputFrameAudioEdit: ElementRef;
  clearAudio(isEdit = false) {
    if (isEdit) {
      this.audioPathEdit = {};
      this.audioContentEdit = {};
      this.InputFrameAudio.nativeElement.value = '';
    } else {
      this.audioPath = undefined;
      this.audioContent = undefined;
      this.InputFrameAudioEdit.nativeElement.value = '';
    }
  }

  editCategory() {
    this.dialogService.addDialog(EditCategoryComponent, {
      category: this.category,
      mainService: this.mainService
    }).subscribe((res: any) => {});
  }

  removeArticulationItem(index) {
    if (this.allPictures[index]) {
      this.dialogService.addDialog(ConfirmComponent, {
        message: `Are you sure you want to remove ${this.allPictures[index].correct_word}?`
      }).subscribe((res: any) => {
        if (res) {
          this.mainService.removeArticulationItem(this.allPictures[index].id)
          .subscribe((res) => {
            if (!res.body.isError) {
              this.allPictures.splice(index, 1);
            }
          }, (error) => {
            this.mainService.notLoggedIn(error);
          });
        }
      });
    }
  }
}
