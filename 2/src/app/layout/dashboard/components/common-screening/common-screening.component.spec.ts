import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonScreeningComponent } from './common-screening.component';

describe('CommonScreeningComponent', () => {
  let component: CommonScreeningComponent;
  let fixture: ComponentFixture<CommonScreeningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonScreeningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonScreeningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
