import { OnInit, Component, Input } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { FormGroup, FormControl } from '@angular/forms';
import { MainService } from '../../../main.service';
import { constants } from '../../../../shared';
import { ConfirmComponent } from '../confirm/confirm.component';

@Component({
  selector: 'app-common-screening',
  templateUrl: './common-screening.component.html',
  styleUrls: ['./common-screening.component.scss']
})

export class CommonScreeningComponent extends DialogComponent<any, any> implements OnInit {
  @Input() categoryId: any;
  @Input() allCommonQuestions: any;
  public question = '';
  public question_edit = '';
  public question_edit_id = '';
  constructor(public mainService: MainService, dialogService: DialogService) {
    super(dialogService);
  }
  editQuestion(index) {
    this.question_edit_id = this.allCommonQuestions.data[this.categoryId][index].id;
    this.question_edit = this.allCommonQuestions.data[this.categoryId][index].question;
  }
  closeEditQuestion() {
    this.question_edit_id = '';
    this.question_edit = '';
  }
  saveEditQuestion(index) {
    this.mainService.editCommonQuestion(this.allCommonQuestions.data[this.categoryId][index].id, this.question_edit)
    .subscribe((res) => {
      if (!res.body.isError) {
        this.allCommonQuestions.data[this.categoryId][index].question = this.question_edit;
        this.question_edit = '';
        this.question_edit_id = '';
      } else {
        alert(res.body.message);
      }
    }, (error) => {
      alert(error.message);
    });
  }

  ngOnInit() {}

  addCommonScreeningQuestion() {
    if (this.question) {
      const body = {
        question: this.question,
        screening_id: this.categoryId
      };
      this.mainService.addCommonScreeningQuestion(this.categoryId, body)
      .subscribe((res) => {
        this.question = '';
        if (!res.body.isError && res.body.data && res.body.data.question) {
          this.allCommonQuestions.data[this.categoryId].push(res.body.data.question);
        }
      }, (error) => {
        this.mainService.notLoggedIn(error);
        console.log(body, error, 'errr');
      });
    }
  }

  deleteQuestion(questionId, index) {
    this.dialogService.addDialog(ConfirmComponent, {
      message: `Are you sure you want to remove this question?`
    }).subscribe((res: any) => {
      if (res) {
        this.mainService.removeCommonScreeningQuestion(questionId, this.categoryId)
        .subscribe((res) => {
          if (!res.body.isError) {
            this.allCommonQuestions.data[this.categoryId].splice(index, 1);
          } else {
            alert(res.body.message);
          }
        }, (error) => {
          alert(error.message);
        });
      }
    });
  }
}
