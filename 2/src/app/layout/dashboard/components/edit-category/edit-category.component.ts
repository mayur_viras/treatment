import { OnInit, Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { FormGroup, FormControl } from '@angular/forms';
import { MainService } from '../../../main.service';
import { constants } from '../../../../shared';


@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})

export class EditCategoryComponent extends DialogComponent<any, any> implements OnInit {
  public category;
  public form: FormGroup;
  public errors: any = { name: '', type: '', youtube_link: '', image: '', bg_color: '' };
  public fileContent: File;
  public imagePath: Blob;
  public isError = false;
  constructor(dialogService: DialogService, private mainService: MainService) {
    super(dialogService);
  }
  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(this.category.name),
      type: new FormControl(constants.CATEGORY_TYPES[this.category.type]),
      description: new FormControl(this.category.description),
      youtube_link: new FormControl(this.category.youtube_link),
      youtube_link_list_success: new FormControl(this.category.youtube_link_list_success),
      youtube_link_list_error: new FormControl(this.category.youtube_link_list_error),
      bg_color: new FormControl(this.category.bg_color)
    });
  }

  changeListener(event: any): void {
    try {
      if (event.target.files && event.target.files[0]) {
        if (!event.target.files[0].type || !event.target.files[0].type.includes('image')) {
          throw new Error('seems like you have uploaded some non-image file !!');
        }
        this.fileContent = event['target'].files[0];
        const reader = new FileReader();
        reader.onload = (event2: FileReaderProgressEvent) => {
          this.imagePath = event2.target.result;
        };
        reader.readAsDataURL(this.fileContent);
      }
    } catch (e) {
      window.scrollTo(0, 0);
      console.log(e, 'eeeee');
      // this.alerts.push({ type: 'danger', message: `${e.message || e.toString()}` });
    }
  }

  clearImage() {
    this.imagePath = undefined;
    this.fileContent = undefined;
  }

  onSubmit(data: any) {
    this.isError = false;
    const allFields = { youtube_link: 'youtube link', bg_color: 'background color' }
    for(const field in data) {
      if (!data[field]) {
        this.errors[field] = `please provide category ${allFields[field] || field}`;
        this.isError = true;
      } else {
        this.errors[field] = '';
      }
    }
    if (!this.isError) {
      if (data.type !== constants.CATEGORY_TYPES[this.category.type]) {
        this.errors.type = 'you can not change category type';
        this.isError = true;
      } else {
        this.isError = false;
        this.errors.type = '';
      }
      if (!(/^#[0-9a-f]{3}([0-9a-f]{3})?$/i.test(data.bg_color))) {
        this.errors.bg_color = 'please provide a valid color code';
        this.isError = true;
      } else {
        this.isError = false;
        this.errors.type = '';
      }
    }
    if (!this.isError) {
      this.mainService.editScreeningCategories(this.category.id, data, this.fileContent)
      .subscribe((res) => {
        if (res.body.isError) {
          console.log(res.body, 'errr1');
        } else {
          this.category.name = data.name;
          this.category.description = data.description;
          this.category.youtube_link = data.youtube_link;
          this.category.youtube_link_list_success = data.youtube_link_list_success;
          this.category.youtube_link_list_error = data.youtube_link_list_error;
          this.category.bg_color = data.bg_color;
          this.close();
        }
      }, (error) => {
        this.mainService.notLoggedIn(error);
        console.log(error, 'errr');
      });
    } else {
      console.log(this.isError, 'is error')
    }
  }
}
