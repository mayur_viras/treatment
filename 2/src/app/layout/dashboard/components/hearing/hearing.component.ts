import { OnInit, Component, Input } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { FormGroup, FormControl } from '@angular/forms';
import { MainService } from '../../../main.service';
import { constants } from '../../../../shared';
import { ConfirmComponent } from '../confirm/confirm.component';


@Component({
  selector: 'app-hearing',
  templateUrl: './hearing.component.html',
  styleUrls: ['./hearing.component.scss']
})

export class HearingComponent extends DialogComponent<any, any> implements OnInit {
  @Input() allHearingScreeningTypes: any;
  public domainPanelWidth = '50%';
  public question = '';
  public screening_type = 1;
  public selectedScreeningType = 0;
  public question_edit = '';
  public question_edit_id = '';
  public edit = false;
  constructor(public mainService: MainService, public dialogService: DialogService) {
    super(dialogService)
  }
  public screening_type_edit = '';
  editQuestion(index, selectedScreeningType) {
    this.question_edit_id = this.allHearingScreeningTypes[selectedScreeningType].hearing_screenings[index].id;
    this.question_edit = this.allHearingScreeningTypes[selectedScreeningType].hearing_screenings[index].question;
    this.screening_type_edit = this.allHearingScreeningTypes[selectedScreeningType].hearing_screenings[index].screening_type;
    this.edit = true;
  }
  closeEditQuestion() {
    this.question_edit_id = '';
    this.question_edit = '';
    this.screening_type_edit = '';
    this.edit = false;
  }
  public background = {};
  saveEditQuestion(index, selectedScreeningType) {
    if (this.question_edit) {
      this.mainService.editHearingQuestion(this.allHearingScreeningTypes[selectedScreeningType].hearing_screenings[index].id, { question: this.question_edit, screening_type: this.screening_type_edit })
      .subscribe((res) => {
        if (!res.body.isError) {
          this.allHearingScreeningTypes[selectedScreeningType].hearing_screenings[index].question = this.question_edit;
          this.question_edit_id = '';
          this.question_edit = '';
          if (this.screening_type_edit.toString() != this.selectedScreeningType.toString()) {
            const question = this.allHearingScreeningTypes[this.selectedScreeningType].hearing_screenings[index];
            this.allHearingScreeningTypes[this.selectedScreeningType].hearing_screenings.splice(index, 1);
            this.selectedScreeningType = (this.selectedScreeningType == 0) ? 1 : 0;
            question.screening_type = (question.screening_type == 1) ? 2 : 1;
            this.allHearingScreeningTypes[this.selectedScreeningType].hearing_screenings.unshift(question);
            this.screening_type_edit = '';
            this.background[question.id] = 'yellow';
            setTimeout(() => {
              delete this.background[question.id];
            }, 500);
          }
          this.edit = false;
        } else {
          alert(res.body.message);
        }
      }, (error) => {
        alert(error.error.message);
      });
    }
  }
  ngOnInit() {
    this.domainPanelWidth = `${Math.floor(100 / this.allHearingScreeningTypes.length)}%`;
    this.screening_type = this.allHearingScreeningTypes[0].id;
  }
  isNumeric(val) {
    return val !== undefined && val !== null && !isNaN(val) && val !== Infinity;
  }
  addHearingQuestion() {
    if (this.question && this.isNumeric(this.screening_type)) {
      const body = {
        question: this.question,
        screening_type: parseInt(this.screening_type.toString()),
      };
      this.mainService.addHearingQuestion(body)
      .subscribe((res) => {
        this.question = '';
        // this.screening_type = this.allHearingScreeningTypes[0].id;
        if (!res.body.isError && res.body.data && res.body.data.question) {
          for(let i = 0; i < this.allHearingScreeningTypes.length; i += 1) {
            if (this.allHearingScreeningTypes[i].id === res.body.data.question.screening_type) {
              this.selectedScreeningType = i;
              this.allHearingScreeningTypes[i].hearing_screenings.push(res.body.data.question);
            }
          }
        }
      }, (error) => {
        this.mainService.notLoggedIn(error);
        console.log(body, error, 'errr');
      });
    }
  }
  selectScreeningType(screeningType) {
    this.selectedScreeningType = screeningType;
  }
  deleteQuestion(questionId, index, selectedScreeningType) {
    this.dialogService.addDialog(ConfirmComponent, {
      message: `Are you sure you want to remove this question?`
    }).subscribe((res: any) => {
      if (res) {
        this.mainService.removeCommonScreeningQuestion(questionId, 3)
        .subscribe((res) => {
          if (!res.body.isError) {
            this.allHearingScreeningTypes[selectedScreeningType].hearing_screenings.splice(index, 1);
          } else {
            alert(res.body.message);
          }
        }, (error) => {
          alert(error.error.message);
        });
      }
    });
  }
}
