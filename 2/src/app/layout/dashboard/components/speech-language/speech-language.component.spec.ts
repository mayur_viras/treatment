import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeechLanguageComponent } from './speech-language.component';

describe('SpeechLanguageComponent', () => {
  let component: SpeechLanguageComponent;
  let fixture: ComponentFixture<SpeechLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpeechLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpeechLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
