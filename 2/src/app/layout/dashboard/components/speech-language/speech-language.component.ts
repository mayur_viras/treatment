import { OnInit, Component, Input } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { FormGroup, FormControl } from '@angular/forms';
import { MainService } from '../../../main.service';
import { constants } from '../../../../shared';
import { ConfirmComponent } from '../confirm/confirm.component';


@Component({
  selector: 'app-speech-language',
  templateUrl: './speech-language.component.html',
  styleUrls: ['./speech-language.component.scss']
})

export class SpeechLanguageComponent extends DialogComponent<any, any> implements OnInit {
  @Input() allSpeechLanDomains: any;
  public domainPanelWidth = '50%';
  public question = '';
  public domain_id = 1;
  public age = '0-3';
  public selectedDomainId = 0;
  public ageRanges = constants.SPEECH_AND_LANGUAGE_AGE_RANGES;
  constructor(public mainService: MainService, public dialogService: DialogService) {
    super(dialogService)
  }
  ngOnInit() {
    this.domainPanelWidth = `${Math.floor(100 / this.allSpeechLanDomains.length)}%`;
    this.domain_id = this.allSpeechLanDomains[0].id;
  }
  isNumeric(val) {
    return val !== undefined && val !== null && !isNaN(val) && val !== Infinity;
  }
  addSpeechLanQuestion() {
    const ages = this.age.split('-');
    const age_start = ages[0];
    const age_end = ages[1];
    if (this.question && this.domain_id && this.isNumeric(age_start) && this.isNumeric(age_end) && age_start <= age_end) {
      const body = {
        question: this.question,
        domain_id: parseInt(this.domain_id.toString()),
        age_start: parseInt(age_start.toString()),
        age_end: parseInt(age_end.toString())
      };
      this.mainService.addSpeechLanQuestion(body)
      .subscribe((res) => {
        this.question = '';
        // this.domain_id = this.allSpeechLanDomains[0].id;
        // this.age = '0-3';
        if (!res.body.isError && res.body.data && res.body.data.question) {
          for(let i = 0; i < this.allSpeechLanDomains.length; i += 1) {
            if (this.allSpeechLanDomains[i].id === res.body.data.question.domain_id) {
              this.allSpeechLanDomains[i].speech_language_screenings.push(res.body.data.question);
              this.selectedDomainId = i;
            }
          }
        }
      }, (error) => {
        this.mainService.notLoggedIn(error);
        console.log(body, error, 'errr');
      });
    }
  }
  selectDomain(domainId) {
    this.selectedDomainId = domainId;
  }
  public question_edit = '';
  public question_edit_id = '';
  public age_edit = '';
  public domain_edit = '';
  public edit = false;
  editQuestion(index, selectedDomainId) {
    this.question_edit_id = this.allSpeechLanDomains[selectedDomainId].speech_language_screenings[index].id;
    this.question_edit = this.allSpeechLanDomains[selectedDomainId].speech_language_screenings[index].question;
    this.domain_edit = this.allSpeechLanDomains[selectedDomainId].speech_language_screenings[index].domain_id;
    this.age_edit = `${this.allSpeechLanDomains[selectedDomainId].speech_language_screenings[index].age_start}-${this.allSpeechLanDomains[selectedDomainId].speech_language_screenings[index].age_end}`;
    this.edit = true;
  }
  closeEditQuestion() {
    this.question_edit_id = '';
    this.question_edit = '';
    this.domain_edit = '';
    this.age_edit = '';
    this.edit = false;
  }
  public background = {};
  saveEditQuestion(index, selectedDomainId) {
    const ages = this.age_edit.split('-');
    const age_start = ages[0];
    const age_end = ages[1];
    if (this.isNumeric(age_start) && this.isNumeric(age_end) && age_start <= age_end) {
      const body = {
        question: this.question_edit,
        age_start: parseInt(age_start.toString()),
        age_end: parseInt(age_end.toString()),
        domain_id: this.domain_edit
      };
      this.mainService.editSpeechLanQuestion(this.allSpeechLanDomains[selectedDomainId].speech_language_screenings[index].id, body)
      .subscribe((res) => {
        if (!res.body.isError) {
          this.allSpeechLanDomains[selectedDomainId].speech_language_screenings[index].question = this.question_edit;
          this.allSpeechLanDomains[selectedDomainId].speech_language_screenings[index].age_start = age_start;
          this.allSpeechLanDomains[selectedDomainId].speech_language_screenings[index].age_end = age_end;
          if (this.domain_edit !== selectedDomainId) {
            setTimeout(() => {
              const question = this.allSpeechLanDomains[this.selectedDomainId].speech_language_screenings[index];
              this.allSpeechLanDomains[this.selectedDomainId].speech_language_screenings.splice(index, 1);
              this.selectedDomainId = (selectedDomainId == 0) ? 1 : 0;
              question.domain_id = (question.domain_id == 1) ? 2 : 1;
              this.allSpeechLanDomains[this.selectedDomainId].speech_language_screenings.unshift(question);
              this.domain_edit = '';
              this.background[question.id] = 'yellow';
              setTimeout(() => {
                delete this.background[question.id];
              }, 500);
            }, 500);
          }
          this.question_edit_id = '';
          this.question_edit = '';
          this.age_edit = '';
          this.edit = false;
        } else {
          alert(res.body.message);
        }
      }, (error) => {
        alert(error.error.message);
      });
    }
  }
  deleteQuestion(questionId, index, selectedDomainId) {
    this.dialogService.addDialog(ConfirmComponent, {
      message: `Are you sure you want to remove this question?`
    }).subscribe((res: any) => {
      if (res) {
        this.mainService.removeCommonScreeningQuestion(questionId, 2)
        .subscribe((res) => {
          if (!res.body.isError) {
            this.allSpeechLanDomains[selectedDomainId].speech_language_screenings.splice(index, 1);
          } else {
            alert(res.body.message);
          }
        }, (error) => {
          alert(error.error.message);
        });
      }
    });
  }
}
