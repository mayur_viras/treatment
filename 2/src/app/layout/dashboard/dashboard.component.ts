import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { MainService } from '../main.service';
import { NgZone } from '@angular/core';
import { NotifyData, GameListSchema } from './dashboard.interface';
import { CategoryDetailComponent } from './components';
import { AlertSchema, Helper, ConfirmComponent, ModalComponent, constants } from '../../shared';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  @Output() notify: EventEmitter<Object> = new EventEmitter<Object>();
  public games: Array<GameListSchema> = [];
  public alerts: Array<AlertSchema> = [];
  public search = '';
  public oldSearch = '';
  public gamesLoading = false;
  public limit = 24;
  public offset = 0;
  public areGamesOver = false;
  public isContentLoading = false;
  public viewRef: Object;
  public allCategories: any = [];
  public categoryTypes = constants.CATEGORY_TYPES;
  public allPictures: any = [];
  public allSpeechLanDomains: any = [];
  public allHearingScreeningTypes: any = [];
  public allCommonQuestions: any = { data: {}, ids: [] };
  constructor(
    public router: Router, private mainService: MainService, private helper: Helper, private dialogService: DialogService
  ) {
    this.router.events.subscribe((event: object) => {
      this.isContentLoading = (event instanceof NavigationStart) ? true : this.isContentLoading;
      this.isContentLoading = (event instanceof NavigationEnd) ? false : this.isContentLoading;
    });
    this.getAllScreeningCategories();
  }

  ngOnInit() {}
  public isLoading = true;
  getAllHearingScreeningTypes() {
    this.mainService.getAllHearingScreeningTypes()
    .subscribe((res) => {
      this.allHearingScreeningTypes = [];
      if (res.body.isError) {
        console.log(res.body, 'errr1');
      } else {
        this.allHearingScreeningTypes = res.body.data.hearingScreeningTypes;
      }
      this.isLoading = false;
    }, (error) => {
      this.mainService.notLoggedIn(error);
      this.isLoading = false;
      console.log(error, 'errr');
    });
  }

  getAllArticulationPictures() {
    this.mainService.getAllArticulationPictures()
    .subscribe((res) => {
      this.getAllSpeechLanDomains();
      this.allPictures = [];
      if (res.body.isError) {
        console.log(res.body, 'errr1');
      } else {
        res.body.data.pictures.forEach(picture => {
          picture.picture_url = `${constants.PUBLIC_URL}${picture.picture_url}`;
          picture.audio_url = `${constants.PUBLIC_URL}${picture.audio_url}`;
          this.allPictures.push(picture);
        });
      }
    }, (error) => {
      this.getAllSpeechLanDomains();
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }

  getAllSpeechLanDomains() {
    this.mainService.getAllSpeechLanDomains()
    .subscribe((res) => {
      this.getAllHearingScreeningTypes();
      this.allSpeechLanDomains = [];
      if (res.body.isError) {
        console.log(res.body, 'errr1');
      } else {
        this.allSpeechLanDomains = res.body.data.domains;
      }
    }, (error) => {
      this.getAllHearingScreeningTypes();
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }

  getAllScreeningCategories() {
    this.mainService.getAllScreeningCategories()
    .subscribe(async (res) => {
      this.getAllArticulationPictures();
      if (res.body.isError) {
        this.alerts.push({ type: 'danger', message: res.body.message });
      } else {
        this.allCategories = res.body.data;
        for(let i = 0; i < this.allCategories.length; i += 1) {
          this.allCategories[i].image = `${constants.PUBLIC_URL}${this.allCategories[i].image}`;
          if (!this.allCategories[i].is_having_levels && this.allCategories[i].type === 2) {
            await this.getAllCommonScreeningQuestions(this.allCategories[i].id);
          }
        }
      }
    }, (error) => {
      this.getAllArticulationPictures();
      this.mainService.notLoggedIn(error);
      this.alerts.push({ type: 'danger', message: `${error.error.message || error.error.toString()}` });
    });
  }

  getAllCommonScreeningQuestions(id) {
    return new Promise(resolve => {
      this.mainService.getAllCommonScreeningQuestions(id)
      .subscribe((res) => {
        if (res.body.isError) {
          this.alerts.push({ type: 'danger', message: res.body.message });
        } else {
          this.allCommonQuestions.data[id] = res.body.data.questions;
          this.allCommonQuestions.ids.push(id);
        }
        resolve();
      }, (error) => {
        this.mainService.notLoggedIn(error);
        this.alerts.push({ type: 'danger', message: `${error.error.message || error.error.toString()}` });
        resolve();
      });
    })
  }

  viewCategory(category) {
    this.viewRef = this.dialogService.addDialog(CategoryDetailComponent, {
      category,
      mainService: this.mainService,
      allPictures: this.allPictures,
      allSpeechLanDomains: this.allSpeechLanDomains,
      allHearingScreeningTypes: this.allHearingScreeningTypes,
      allCommonQuestions: this.allCommonQuestions
    }).subscribe((res: any) => {});
  }
  onNotify(data: any): void {
    if (data && data.alerts) {
      this.notify.emit(data.alerts);
    }
  }
}
