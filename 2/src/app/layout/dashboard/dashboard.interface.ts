
import { AlertSchema, CommonResponseSchema } from '../../shared';
export interface NotifyData {
  alerts: AlertSchema;
}

export interface GameListSchema {
  _id: string;
  body: {
    expectedHF: string | number;
    expectedROE: string | number;
    expectedRTP: string | number;
    freeConfig: boolean;
    gameLogo: string;
    latestVersion: string;
    latestVersionId: number;
  };
}

export interface GamesResponseSchema extends CommonResponseSchema {
  data: Array<GameListSchema>;
}
