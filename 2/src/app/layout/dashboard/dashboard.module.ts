import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

import {
  LoaderModule, AlertModule, AuthService, ModalComponent
} from '../../shared';
import {
  CategoryDetailComponent, EditCategoryComponent, SpeechLanguageComponent,
  HearingComponent, CommonScreeningComponent, ConfirmComponent
} from './components';
import { FileUploadModule } from 'ng2-file-upload';
import { MainService } from '../main.service';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
@NgModule({
  imports: [
    AlertModule,
    CommonModule,
    FormsModule,
    AngularMultiSelectModule,
    ChartsModule,
    FileUploadModule,
    DashboardRoutingModule,
    LoaderModule,
    ReactiveFormsModule,
    BootstrapModalModule.forRoot({ container: document.body })
  ],
  declarations: [
    DashboardComponent,
    ModalComponent,
    CategoryDetailComponent,
    EditCategoryComponent,
    SpeechLanguageComponent,
    HearingComponent,
    CommonScreeningComponent,
    ConfirmComponent
  ],

  entryComponents: [
    ModalComponent,
    CategoryDetailComponent,
    EditCategoryComponent,
    ConfirmComponent
  ],
  providers: [
    MainService,
    AuthService,
  ],
})
export class DashboardModule { }
