import { OnInit, Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { FormGroup, FormControl } from '@angular/forms';
import { constants } from '../../../../shared';
import { MainService } from '../../../main.service';

@Component({
  selector: 'app-add-faq',
  templateUrl: './add-faq.component.html',
  styleUrls: ['./add-faq.component.scss']
})
export class AddFaqComponent extends DialogComponent<any, any> implements OnInit {
  public allLanguages;
  public param: any;
  public form;
  public allCategories: any;
  public errors = { title: '', language: '', description: '', category: '', video: '' };
  public allFaqs: any;
  public isEdit: boolean;
  public faq: any;
  public faqIndex: number;
  constructor(dialogService: DialogService, private mainService: MainService) {
    super(dialogService);
    this.form = new FormGroup({
      title: new FormControl(''),
      language: new FormControl(''),
      description: new FormControl(''),
      category: new FormControl(''),
      video: new FormControl('')
    });
  }


  addOrEditFaq(data) {
    if (data.title && data.language && data.description && data.category) {
      this.errors = { title: '', language: '', description: '', category: '', video: '' };
      this.mainService.addOrEditFaq(this.param, data, (this.faq) ? this.faq.id : null, this.isEdit, this.fileContent)
      .subscribe((res) => {
        if (!res.body.isError) {
          if (!this.allCategories.includes(data.category)) {
            this.allCategories.push(data.category);
          }
          if (!this.allLanguages.includes(data.language)) {
            this.allLanguages.push(data.language);
          }
          if (this.isEdit) {
            this.allFaqs[this.faqIndex].title = res.body.data[this.param].title;
            this.allFaqs[this.faqIndex].description = res.body.data[this.param].description;
            this.allFaqs[this.faqIndex].language = res.body.data[this.param].language;
            this.allFaqs[this.faqIndex].video = res.body.data[this.param].video;
          } else {
            this.allFaqs.push(res.body.data[this.param]);
          }
          this.result = true;
          this.close();
        }
      }, (error) => {
        console.log(error, 'errr');
      });
    } else if (!data.title) {
      this.errors.title = 'title is required';
    } else if (!data.language) {
      this.errors.language = 'language is required';
    } else if (!data.description) {
      this.errors.description = 'description is required';
    } else if (!data.category) {
      this.errors.category = 'category is required';
    }
  }

  ngOnInit() {
    if (this.faq) {
      this.form = new FormGroup({
        title: new FormControl(this.faq.title),
        description: new FormControl(this.faq.description),
        language: new FormControl(this.faq.language),
        category: new FormControl(this.faq.category),
        video: new FormControl(this.faq.video)
      });
    }
  }
  public fileContent: File;
  public imagePath: Blob;
  public videoContent;
  public videoData;
  changeListener(event: any): void {
    try {
      if (event.target.files && event.target.files[0]) {
        if (!event.target.files[0].type || !event.target.files[0].type.includes('image')) {
          throw new Error('seems like you have uploaded some non-image file !!');
        }
        this.fileContent = event['target'].files[0];
        const reader = new FileReader();
        reader.onload = (event2: any) => {
          this.imagePath = event2.target.result;
        };
        reader.readAsDataURL(this.fileContent);
      }
    } catch (e) {
      window.scrollTo(0, 0);
      console.log(e, 'eeeee');
      // this.alerts.push({ type: 'danger', message: `${e.message || e.toString()}` });
    }
  }
  changeVideoListener(event: any): void {
    try {
      if (event.target.files && event.target.files[0]) {
        if (!event.target.files[0].type || !event.target.files[0].type.includes('video')) {
          throw new Error('seems like you have uploaded some non-video file !!');
        }
        this.videoContent = event['target'].files[0];
        const reader = new FileReader();
        reader.onload = (event2: any) => {
          this.videoData = event2.target.result;
        };
        reader.readAsDataURL(this.videoContent);
      }
    } catch (e) {
      window.scrollTo(0, 0);
    }
  }
  clearImage() {
    this.imagePath = undefined;
    this.fileContent = undefined;
  }
  clearVideo() {
    this.videoData = undefined;
    this.videoContent = undefined;
  }
}
