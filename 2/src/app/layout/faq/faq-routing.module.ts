import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared';

import { FaqComponent } from './faq.component';
const routes: Routes = [
  {
    path: '', component: FaqComponent,
    canActivate: [AuthGuard],
    pathMatch: 'full',
  },
  { path: '**', redirectTo: '/not-found' },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FaqRoutingModule { }
