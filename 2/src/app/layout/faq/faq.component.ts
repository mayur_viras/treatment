import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { MainService } from '../main.service';
import { NgZone } from '@angular/core';
import { AlertSchema, Helper, ConfirmComponent, ModalComponent, constants } from '../../shared';
import { AddFaqComponent } from './components/add-faq/add-faq.component';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  public isContentLoading = false;
  public alerts: Array<any> = [];
  public allFaqs: any = [];
  public allCategories: any = ['common'];
  public allLanguages: any = ['English', 'Hindi', 'Kannada'];
  public viewRef: any;
  public param = 'faq';
  constructor(
    public router: Router, private mainService: MainService, private helper: Helper,
    private dialogService: DialogService
  ) {
    this.router.events.subscribe((event: object) => {
      this.isContentLoading = (event instanceof NavigationStart) ? true : this.isContentLoading;
      this.isContentLoading = (event instanceof NavigationEnd) ? false : this.isContentLoading;
    });
    if (this.router.url.includes('/blog')) {
      this.param = 'blog';
    }
    this.getAllFaq();
    this.getCategories();
    this.getAllLanguages();
  }

  getAllLanguages() {
    this.mainService.getAllLanguages(this.param)
    .subscribe((res) => {
      if (!res.body.isError) {
        this.allLanguages = res.body.data.languages;
      }
    }, (error) => {
      console.log(error, 'errr');
    });
  }

  ngOnInit() {}
  getAllFaq() {
    this.mainService.getAllFaq(this.param)
    .subscribe((res) => {
      this.allFaqs = [];
      if (res.body.isError) {
        this.alerts.push({ type: 'danger', message: res.body.message });
      } else {
        this.allFaqs = res.body.data[this.param];
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  getCategories() {
    this.mainService.getCategories(this.param)
    .subscribe((res) => {
      this.allCategories = [];
      if (res.body.isError) {
        this.alerts.push({ type: 'danger', message: res.body.message });
      } else {
        this.allCategories = res.body.data.categories;
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }

  addOrEditFaq(index: number) {
    const faqData: any = {
      mainService: this.mainService,
      allFaqs: this.allFaqs,
      allCategories: this.allCategories,
      param: this.param,
      allLanguages: this.allLanguages
    };
    if (index !== undefined) {
      faqData.faq = this.allFaqs[index];
      faqData.faqIndex = index;
      faqData.isEdit = true;
    }
    this.viewRef = this.dialogService.addDialog(AddFaqComponent, faqData).subscribe((res: any) => {
      if (res) {
        this.getAllLanguages();
      }
    });
  }

  removeFaq(index, event) {
    if (this.allFaqs[index]) {
      this.dialogService.addDialog(ConfirmComponent, { message: 'Are you sure you want to remove this FAQ?' }).subscribe((res: any) => {
        if (res) {
          this.mainService.removeFaq(this.param, this.allFaqs[index].id)
          .subscribe((res) => {
            if (res.body.isError) {
              this.alerts.push({ type: 'danger', message: res.body.message });
            } else {
              this.allFaqs.splice(index, 1);
            }
          }, (error) => {
            this.mainService.notLoggedIn(error);
            console.log(error, 'errr');
          });
        }
      });
    }
  }
}
