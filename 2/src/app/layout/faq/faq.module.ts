import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FaqRoutingModule } from './faq-routing.module';
import { FaqComponent } from './faq.component';
import { AddFaqComponent } from './components/add-faq/add-faq.component';
import { LoaderModule, AlertModule, AuthService, ConfirmComponent } from '../../shared';
import { FileUploadModule } from 'ng2-file-upload';
import { MainService } from '../main.service';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
@NgModule({
  imports: [
    AlertModule,
    CommonModule,
    FormsModule,
    AngularMultiSelectModule,
    ChartsModule,
    FileUploadModule,
    FaqRoutingModule,
    LoaderModule,
    ReactiveFormsModule,
    BootstrapModalModule.forRoot({ container: document.body })
  ],
  declarations: [
    FaqComponent,
    AddFaqComponent,
    ConfirmComponent
  ],
  entryComponents: [
    AddFaqComponent,
    ConfirmComponent
  ],
  providers: [
    MainService,
    AuthService,
  ],
})

export class FaqModule { }
