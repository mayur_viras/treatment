import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared';

import { FeedbackComponent } from './feedback.component';
const routes: Routes = [
  {
    path: '', component: FeedbackComponent,
    canActivate: [AuthGuard],
    pathMatch: 'full',
  },
  { path: '**', redirectTo: '/not-found' },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeedbackRoutingModule { }
