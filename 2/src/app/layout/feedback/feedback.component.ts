import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { MainService } from '../main.service';
import { NgZone } from '@angular/core';
import { AlertSchema, Helper, ModalComponent, constants } from '../../shared';
import { ConfirmComponent } from './components/confirm/confirm.component';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  public isContentLoading = false;
  public PUBLIC_URL = constants.PUBLIC_URL;
  @Output() notify: EventEmitter<Object> = new EventEmitter<Object>();
  public alerts: Array<any> = [];
  public allFeedback: any;
  constructor(
    public router: Router, private mainService: MainService, private helper: Helper, private dialogService: DialogService
  ) {
    this.router.events.subscribe((event: object) => {
      this.isContentLoading = (event instanceof NavigationStart) ? true : this.isContentLoading;
      this.isContentLoading = (event instanceof NavigationEnd) ? false : this.isContentLoading;
    });
    this.getAllFeedback();
  }

  ngOnInit() {}
  getAllFeedback() {
    this.mainService.getAllFeedback()
    .subscribe((res) => {
      this.allFeedback = [];
      if (res.body.isError) {
        this.alerts.push({ type: 'danger', message: res.body.message });
      } else {
        this.allFeedback = res.body.data.feedback;
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }

  removeFeedback(index) {
    if (this.allFeedback[index]) {
      this.dialogService.addDialog(ConfirmComponent, { message: 'Are you sure you want to remove this feedback?' }).subscribe((res: any) => {
        if (res) {
          this.mainService.removeFeedback(this.allFeedback[index].id)
          .subscribe((res) => {
            if (res.body.isError) {
              this.alerts.push({ type: 'danger', message: res.body.message });
            } else {
              this.allFeedback.splice(index, 1);
            }
          }, (error) => {
            this.mainService.notLoggedIn(error);
          });
        }
      });
    }
  }

  changeVisibility(index) {
    if (this.allFeedback[index]) {
      this.dialogService.addDialog(ConfirmComponent, { message: 'Are you sure you want to change visibility of this feedback?' }).subscribe((res: any) => {
        if (res) {
          this.mainService.changeVisibility(this.allFeedback[index].id)
          .subscribe((res) => {
            if (res.body.isError) {
              this.alerts.push({ type: 'danger', message: res.body.message });
            } else {
              this.allFeedback[index].isVisibleToAdminOnly = !this.allFeedback[index].isVisibleToAdminOnly;
            }
          }, (error) => {
            this.mainService.notLoggedIn(error);
          });
        }
      });
    }
  }
}
