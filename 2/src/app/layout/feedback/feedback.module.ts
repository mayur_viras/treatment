import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FeedbackRoutingModule } from './feedback-routing.module';
import { FeedbackComponent } from './feedback.component';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { LoaderModule, AlertModule, AuthService } from '../../shared';
import { FileUploadModule } from 'ng2-file-upload';
import { MainService } from '../main.service';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
@NgModule({
  imports: [
    AlertModule,
    CommonModule,
    FormsModule,
    AngularMultiSelectModule,
    ChartsModule,
    FileUploadModule,
    FeedbackRoutingModule,
    LoaderModule,
    ReactiveFormsModule,
    BootstrapModalModule.forRoot({ container: document.body })
  ],
  declarations: [
    FeedbackComponent,
    ConfirmComponent
  ],
  entryComponents: [
    ConfirmComponent
  ],
  providers: [
    MainService,
    AuthService,
  ],
})

export class FeedbackModule {}
