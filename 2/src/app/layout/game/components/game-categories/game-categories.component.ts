import { OnInit, Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { constants } from '../../../../shared';
import { MainService } from '../../../main.service';

@Component({
  selector: 'app-game-categories',
  templateUrl: './game-categories.component.html',
  styleUrls: ['./game-categories.component.scss']
})
export class GameCategoriesComponent extends DialogComponent<any, any> implements OnInit {
  public allGameCategories: any;
  public categoryName = '';
  constructor(dialogService: DialogService, private mainService: MainService) {
    super(dialogService);
  }
  addNewCategory() {
    if (this.categoryName) {
      this.mainService.addNewCategory(this.categoryName)
      .subscribe((res) => {
        this.categoryName = '';
        if (!res.body.isError && res.body.data && res.body.data.category) {
          this.allGameCategories.push(res.body.data.category);
        }
      }, (error) => {
        this.mainService.notLoggedIn(error);
        console.log(error, 'errr');
      });
    }
  }
  ngOnInit() {
    console.log('all game categories', this.allGameCategories);
  }
}
