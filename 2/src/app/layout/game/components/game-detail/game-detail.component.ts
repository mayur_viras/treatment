import { OnInit, Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { constants } from '../../../../shared';
import { MainService } from '../../../main.service';
import { ConfirmComponent } from '../confirm/confirm.component';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss']
})
export class GameDetailComponent extends DialogComponent<any, any> implements OnInit {
  public game: any;
  public allLevels: any;
  public alerts: any = [];
  public allGameCategories: any;
  public allGameIds = constants.GAMES;
  public PUBLIC_URL = constants.PUBLIC_URL;
  public images = [];
  public imageContents = [];
  public correctImage: number;
  public audio;
  public audioContent;
  public audios = [];
  public audioContents = [];
  public level;
  public category;
  public correctImages = [];
  public question = '';
  constructor(dialogService: DialogService, private mainService: MainService) {
    super(dialogService);
  }

  ngOnInit() {
    this.level = this.allLevels[0];
    if (this.allGameCategories.length) {
      this.category = this.allGameCategories[0].id;
    }
  }
  addImage(event: any) {
    try {
      if (event.target.files && event.target.files[0]) {
        if (!event.target.files[0].type || !event.target.files[0].type.includes('image')) {
          throw new Error('seems like you have uploaded some non-image file !!');
        }
        const fileContent = event['target'].files[0];
        this.imageContents.push(fileContent);
        const reader = new FileReader();
        reader.onload = (event2: FileReaderProgressEvent) => {
          this.images.push(event2.target.result);
          if (this.images.length === 1) {
            this.correctImage = this.images.length - 1;
          }
        };
        reader.readAsDataURL(fileContent);
      }
    } catch (e) {
      window.scrollTo(0, 0);
      console.log(e, 'eeeee');
      this.alerts.push({ type: 'danger', message: `${e.message || e.toString()}` });
    }
  }

  removeAudio(index) {
    this.audios.splice(index, 1);
    this.audioContents.splice(index, 1);
  }

  addAudio(event, isMemoryId = false) {
    try {
      if (event.target.files && event.target.files[0]) {
        if (!event.target.files[0].type || !event.target.files[0].type.includes('audio')) {
          throw new Error('seems like you have uploaded some non-audio file !!');
        }
        if (isMemoryId) {
          this.audioContents.push(event['target'].files[0]);
        } else {
          this.audioContent = event['target'].files[0];
        }
        const reader = new FileReader();
        reader.onload = (event2: FileReaderProgressEvent) => {
          if (isMemoryId) {
            this.audios.push(event2.target.result);
          } else {
            this.audio = event2.target.result;
          }
        };
        if (isMemoryId) {
          reader.readAsDataURL(this.audioContents[this.audioContents.length - 1]);
        } else {
          reader.readAsDataURL(this.audioContent);
        }
      }
    } catch (e) {
      window.scrollTo(0, 0);
      this.alerts.push({ type: 'danger', message: `${e.message || e.toString()}` });
    }
  }

  addDataForIdentificationGame() {
    try {
      if (this.game.id !== this.allGameIds.VOCABULARY && this.images.length < 2) {
        throw new Error('please provide atleast 2 images');
      }
      if ((this.game.id === this.allGameIds.WHAT_DO_I_DO || this.game.id === this.allGameIds.VOCABULARY || this.game.id === this.allGameIds.WHAT_DO_YOU_USE_FOR) && !this.question) {
        throw new Error('please provide instructions');
      }
      if ((this.game.id !== this.allGameIds.GUESS_ME || this.game.id !== this.allGameIds.IDENTIFICATION) || this.audio) {
        const body: any = {
          gameId: this.game.id, categoryId: this.category, level: this.level, correctImage: this.correctImage
        };
        if (this.question) {
          body.question = this.question;
        }
        if (this.game.id === this.allGameIds.WHAT_DO_YOU_USE_FOR || this.game.id === this.allGameIds.MEMORY_AND_SEQUENCING || this.game.id === this.allGameIds.MEMORY) {
          body.correctImages = this.correctImages;
          body.correctImage = null;
        }
        this.mainService.addDataForIdentificationGame(body, this.imageContents, this.audioContent, this.audioContents)
        .subscribe((res) => {
          if (res.body.isError) {
            this.alerts.push({ type: 'danger', message: res.body.message });
          } else {
            if (res.body.data.gameData && res.body.data.gameData.audio) {
              let audios = res.body.data.gameData.audio.split(' ').filter(Boolean);
              if (audios.length > 1) {
                res.body.data.gameData.audios = audios;
                delete res.body.data.gameData.audio;
              }
            }
            res.body.data.gameData.gameCategory = this.allGameCategories.find(cat => cat.id == body.categoryId) || { id: 1, name: '' };
            this.game.gameData.push(res.body.data.gameData);
            this.category = this.allGameCategories[0].id;
            this.level = this.allLevels[0];
            this.question = '';
            this.images = [];
            this.imageContents = [];
            this.audio = undefined;
            this.audios = [];
            this.audioContent = undefined;
            this.audioContents = [];
            this.correctImage = undefined;
            this.correctImages = [];
            this.alerts = [];
          }
        }, (error) => {
          this.mainService.notLoggedIn(error);
          console.log(error, 'errr');
        });
      } else {
        throw new Error('please provide audio');
      }
    } catch(e) {
      this.alerts.push({ type: 'danger', message: e.message });
    }
  }

  removeGameData(id, index) {
    this.dialogService.addDialog(ConfirmComponent, {
      message: 'are you sure you want to delete this data?'
    }).subscribe((res: any) => {
      if (res) {
        this.mainService.removeGameData(id).subscribe((res) => {
          if (res.body.isError) {
            this.alerts.push({ type: 'danger', message: res.body.message });
          } else {
            this.game.gameData.splice(index, 1);
          }
        });
      }
    });
  }

  removeVocData(id, index) {
    this.dialogService.addDialog(ConfirmComponent, {
      message: 'are you sure you want to delete this data?'
    }).subscribe((res: any) => {
      if (res) {
        this.mainService.removeVocData(id).subscribe((res) => {
          if (res.body.isError) {
            this.alerts.push({ type: 'danger', message: res.body.message });
          } else {
            this.game.gameData.splice(index, 1);
          }
        });
      }
    });
  }

  removeImage(index) {
    this.imageContents.splice(index, 1);
    this.images.splice(index, 1);
    if (this.images.length) {
      this.correctImage = 0;
    } else {
      this.correctImage = null;
    }
    this.correctImages = [];
  }

}
