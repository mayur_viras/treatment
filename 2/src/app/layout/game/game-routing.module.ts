import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared';

import { GameComponent } from './game.component';
const routes: Routes = [
  {
    path: '', component: GameComponent,
    canActivate: [AuthGuard],
    pathMatch: 'full',
  },
  { path: '**', redirectTo: '/not-found' },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameRoutingModule { }
