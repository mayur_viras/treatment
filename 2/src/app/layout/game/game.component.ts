import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { MainService } from '../main.service';
import { NgZone } from '@angular/core';
import { GameCategoriesComponent, GameDetailComponent } from './components';
import { AlertSchema, Helper, ConfirmComponent, ModalComponent, constants } from '../../shared';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  public isContentLoading = false;
  @Output() notify: EventEmitter<Object> = new EventEmitter<Object>();
  public alerts: Array<any> = [];
  public allGames: any = [];
  public allLevels: any = [];
  public allGameCategories: any;
  constructor(
    public router: Router, private mainService: MainService, private helper: Helper, private dialogService: DialogService
  ) {
    this.router.events.subscribe((event: object) => {
      this.isContentLoading = (event instanceof NavigationStart) ? true : this.isContentLoading;
      this.isContentLoading = (event instanceof NavigationEnd) ? false : this.isContentLoading;
    });
    this.getAllLevels();
    this.getALlGameCategories();
    this.getGameData();
  }

  ngOnInit() {}
  getGameData() {
    this.mainService.getGameData()
    .subscribe((res) => {
      this.allGames = [];
      if (res.body.isError) {
        this.alerts.push({ type: 'danger', message: res.body.message });
      } else {
        this.allGames = res.body.data.gameData;
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }

  getAllLevels() {
    this.mainService.getAllLevels()
    .subscribe((res) => {
      this.allLevels = [];
      if (res.body.isError) {
        this.alerts.push({ type: 'danger', message: res.body.message });
      } else {
        this.allLevels = res.body.data.allLevels;
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }

  getALlGameCategories() {
    this.mainService.getALlGameCategories()
    .subscribe((res) => {
      this.allGameCategories = [];
      if (res.body.isError) {
        this.alerts.push({ type: 'danger', message: res.body.message });
      } else {
        this.allGameCategories = res.body.data.allGameCategories;
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  gameCategories() {
    const viewRef = this.dialogService.addDialog(GameCategoriesComponent, {
      allGameCategories: this.allGameCategories,
      mainService: this.mainService
    }).subscribe((res: any) => {});
  }
  viewCategory(game) {
    const viewRef = this.dialogService.addDialog(GameDetailComponent, {
      game,
      allLevels: this.allLevels,
      allGameCategories: this.allGameCategories,
      mainService: this.mainService
    }).subscribe((res: any) => {});
  }
}
