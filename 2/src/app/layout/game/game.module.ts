import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { GameRoutingModule } from './game-routing.module';
import { GameComponent } from './game.component';
import { LoaderModule, AlertModule, AuthService } from '../../shared';
import { GameCategoriesComponent, GameDetailComponent, ConfirmComponent } from './components';
import { FileUploadModule } from 'ng2-file-upload';
import { MainService } from '../main.service';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
@NgModule({
  imports: [
    AlertModule,
    CommonModule,
    FormsModule,
    AngularMultiSelectModule,
    ChartsModule,
    FileUploadModule,
    GameRoutingModule,
    LoaderModule,
    ReactiveFormsModule,
    BootstrapModalModule.forRoot({ container: document.body })
  ],
  declarations: [
    GameComponent,
    GameCategoriesComponent,
    GameDetailComponent,
    ConfirmComponent
  ],
  entryComponents: [
    GameCategoriesComponent,
    GameDetailComponent,
    ConfirmComponent
  ],
  providers: [
    MainService,
    AuthService,
  ],
})

export class GameModule { }
