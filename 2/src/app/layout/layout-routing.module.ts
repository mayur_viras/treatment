import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { AuthGuard } from '../shared';

const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate: [AuthGuard],
        pathMatch: 'prefix',
      },
      {
        path: 'games', loadChildren: './game/game.module#GameModule', canActivate: [AuthGuard],
        pathMatch: 'prefix',
      },
      {
        path: 'educational-videos', loadChildren: './videos/videos.module#VideosModule', canActivate: [AuthGuard],
        pathMatch: 'prefix',
      },
      {
        path: 'faq', loadChildren: './faq/faq.module#FaqModule', canActivate: [AuthGuard],
        pathMatch: 'prefix',
      },
      {
        path: 'story', loadChildren: './story/story.module#StoryModule', canActivate: [AuthGuard],
        pathMatch: 'prefix',
      },
      {
        path: 'blog', loadChildren: './faq/faq.module#FaqModule', canActivate: [AuthGuard],
        pathMatch: 'prefix',
      },
      {
        path: 'feedback', loadChildren: './feedback/feedback.module#FeedbackModule', canActivate: [AuthGuard],
        pathMatch: 'prefix',
      },
      {
        path: 'country', loadChildren: './country/country.module#CountryModule', canActivate: [AuthGuard],
        pathMatch: 'prefix',
      },
      {
        path: 'client-param', loadChildren: './client-parameter/client-parameter.module#ClientParamModule', canActivate: [AuthGuard],
        pathMatch: 'prefix',
      },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
