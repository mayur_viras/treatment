import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Helper } from '../shared';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})

export class LayoutComponent implements OnInit {

  public isContentLoading = false;
  constructor(public router: Router, public helper: Helper, private title: Title) {

    this.router.events.subscribe((event: object) => {
      this.isContentLoading = (event instanceof NavigationStart) ? true : this.isContentLoading;
      this.isContentLoading = (event instanceof NavigationEnd) ? false : this.isContentLoading;
      if (!this.isContentLoading) {
        document.getElementsByTagName('body')[0].style.overflow = 'auto';
      }
    });
  }

  ngOnInit() {
  }

  onNotify(data: any) {
    if (data && data.pageHeader) {
    }
  }
}
