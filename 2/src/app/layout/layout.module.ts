
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogService } from 'ng2-bootstrap-modal';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent, LoaderModule, Helper } from '../shared';
import { MainService } from './main.service';
import { AuthService, AlertModule } from '../shared';
@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    LoaderModule,
    AlertModule
  ],
  declarations: [
    LayoutComponent,
    HeaderComponent
  ],
  providers: [
    Helper, MainService,
    AuthService, DialogService
  ],
})

export class LayoutModule {}
