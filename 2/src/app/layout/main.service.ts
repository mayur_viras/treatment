import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { constants, AuthService } from '../shared';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable()
export class MainService {
  public apiUrl: string = constants.API_URL;

  constructor(private http: HttpClient, private router: Router, private authService: AuthService) {}

  getALlGames() : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/games`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  changeDecidingValue(categoryId, decidingValue, decidingValue2) : Observable<HttpResponse<any>> {
    return this.http.put<any>(
      `${this.apiUrl}/admin/screening-category/deciding-value/${categoryId}`, { decidingValue, decidingValue2 },
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  getAllVideos() : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/educational-videos?limit=10000`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  getAllCountry() : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/country`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  editCountry(id, name) : Observable<HttpResponse<any>> {
    return this.http.put<any>(
      `${this.apiUrl}/admin/country/${id}`, { name },
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  addNewCountry(name) : Observable<HttpResponse<any>> {
    return this.http.post<any>(
      `${this.apiUrl}/admin/country`, { name },
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  removeCountry(id) : Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.apiUrl}/admin/country/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  removeGameData(id) : Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.apiUrl}/admin/games/data/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  removeVocData(id) : Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.apiUrl}/admin/vocabulary/data/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  getAllState(countryId) : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/state/${countryId}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  editState(id, name) : Observable<HttpResponse<any>> {
    return this.http.put<any>(
      `${this.apiUrl}/admin/state/${id}`, { name },
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  addNewState(name, countryId) : Observable<HttpResponse<any>> {
    return this.http.post<any>(
      `${this.apiUrl}/admin/state`, { name, countryId },
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  removeState(id) : Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.apiUrl}/admin/state/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  getAllParam(param_tag) : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/params/${param_tag}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  editParam(param_tag, id, name) : Observable<HttpResponse<any>> {
    return this.http.put<any>(
      `${this.apiUrl}/admin/params/${param_tag}/${id}`, { name },
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  addNewParam(param_tag, name) : Observable<HttpResponse<any>> {
    return this.http.post<any>(
      `${this.apiUrl}/admin/params/${param_tag}`, { name },
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  removeParam(param_tag, id) : Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.apiUrl}/admin/params/${param_tag}/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  getAllCity(stateId) : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/city/${stateId}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  editCity(id, name) : Observable<HttpResponse<any>> {
    return this.http.put<any>(
      `${this.apiUrl}/admin/city/${id}`, { name },
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  addNewCity(name, stateId) : Observable<HttpResponse<any>> {
    return this.http.post<any>(
      `${this.apiUrl}/admin/city`, { name, stateId },
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  removeCity(id) : Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.apiUrl}/admin/city/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  getAllFeedback() : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/feedback`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  removeFeedback(id) : Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.apiUrl}/admin/feedback/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  removeCommonScreeningQuestion(id, categoryId) : Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.apiUrl}/admin/screening-category/common/question/${categoryId}/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  changeVisibility(id) : Observable<HttpResponse<any>> {
    return this.http.put<any>(
      `${this.apiUrl}/admin/feedback/visibility/${id}`, {},
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  getAllFaq(param) : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/${param}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }
  getAllStory() : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/treatmentVersion/getStoriesList`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }
  getStoryContent(id: string) : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/treatmentVersion/getStoryContent/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }
  removeFaq(param, id) : Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.apiUrl}/admin/${param}/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }
  removeStory(id) : Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.apiUrl}/admin/treatmentVersion/story/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }
  addOrEditStory(body: any, id?: string) : Observable<HttpResponse<any>> {
    const formData = new FormData();
    for(const field in body) {
      if (body[field]) {
        formData.append(field, body[field]);
      }
    }
    if (id) {
      return this.http.put<any>(
        `${this.apiUrl}/admin/treatmentVersion/story/${id}`, formData,
        {
          headers: this.authService.getMultipartHeaders(),
          observe: 'response'
        }
      );
    }
    return this.http.post<any>(
      `${this.apiUrl}/admin/treatmentVersion/story`, formData,
      {
        headers: this.authService.getMultipartHeaders(),
        observe: 'response'
      }
    );
  }
  removeVideo(id) : Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.apiUrl}/admin/educational-video/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }
  removeArticulationItem(id) : Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.apiUrl}/admin/screening-category/articulation/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  addOrEditFaq(param, body, id, isEdit, fileContent) : Observable<HttpResponse<any>> {
    if (fileContent) {
      const formData = new FormData();
      for(const field in body) {
        formData.append(field, body[field]);
      }
      if (fileContent) {
        formData.append('image', fileContent);
      }
      const headers = this.authService.getMultipartHeaders();
      if (isEdit) {
        return this.http.put<any>(
          `${this.apiUrl}/admin/${param}/${id}`, formData,
          {
            headers,
            observe: 'response'
          }
        );
      }
      return this.http.post<any>(
        `${this.apiUrl}/admin/${param}`, formData,
        {
          headers,
          observe: 'response'
        }
      );
    }
    if (isEdit) {
      return this.http.put<any>(
        `${this.apiUrl}/admin/${param}/${id}`, body,
        {
          headers: this.authService.getHeaders(),
          observe: 'response'
        }
      );
    }
    return this.http.post<any>(
      `${this.apiUrl}/admin/${param}`, body,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  addEducationalVideo(body, videoContent, isEdit) : Observable<HttpResponse<any>> {
    const formData = new FormData();
    for(const field in body) {
      formData.append(field, body[field]);
      formData.append(field, body[field]);
      formData.append(field, body[field]);
    }
    if (videoContent) {
      formData.append('video', videoContent);
    }
    const headers = this.authService.getMultipartHeaders();
    if (isEdit) {
      return this.http.put<any>(
        `${this.apiUrl}/admin/educational-video/${isEdit}`, formData,
        {
          headers,
          observe: 'response'
        }
      );
    }
    return this.http.post<any>(
      `${this.apiUrl}/admin/educational-video`, formData,
      {
        headers,
        observe: 'response'
      }
    );
  }

  getAllLevels() : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/games/levels`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }
  getAllLanguages(param) : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/language/${param}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }
  getCategories(param) : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/categories/${param}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }
  getStoryTargetSounds() : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/treatmentVersion/getStoryTargetSounds`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }
  getGameData(): Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/games/data`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  addDataForIdentificationGame(body, images, audio, audios): Observable<HttpResponse<any>> {
    const formData = new FormData();
    for(const field in body) {
      formData.append(field, body[field]);
      formData.append(field, body[field]);
      formData.append(field, body[field]);
    }
    for(let i = 0; i < images.length; i++) {
      formData.append(`images-${Math.floor(Math.random() * 100000)}`, images[i]);
    }
    formData.append('audio', audio);
    if (audios && audios.length) {
      for(let i = 0; i < audios.length; i++) {
        formData.append(`audios-${Math.floor(Math.random() * 100000)}`, audios[i]);
      }
    }
    const headers = this.authService.getMultipartHeaders();
    return this.http.post<any>(
      `${this.apiUrl}/admin/games/data`, formData,
      {
        headers,
        observe: 'response'
      }
    );
  }

  getALlGameCategories() : Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/games/categories`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  addNewCategory(categoryName) : Observable<HttpResponse<any>> {
    return this.http.post<any>(
      `${this.apiUrl}/admin/games/categories`, { categoryName },
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  getAllScreeningCategories(): Observable<HttpResponse<any>> {
    return this.http.get<any>(
      `${this.apiUrl}/admin/screening-categories`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  editScreeningCategories(id, body, fileContent): Observable<HttpResponse<any>> {
    if (fileContent) {
      const formData = new FormData();
      for(const field in body) {
        formData.append(field, body[field]);
        formData.append(field, body[field]);
        formData.append(field, body[field]);
      }
      formData.append('image', fileContent);
      const headers = this.authService.getMultipartHeaders();
      return this.http.put<any>(
        `${this.apiUrl}/admin/screening-category/${id}`, formData,
        {
          headers,
          observe: 'response'
        }
      );
    }
    return this.http.put<any>(
      `${this.apiUrl}/admin/screening-category/${id}`, body,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  addArticulationPictures(picture, audio, correct_word, target_sound) {
    const body = new FormData();
    body.append('correct_word', correct_word);
    body.append('target_sound', target_sound);
    body.append('articulationPicture', picture);
    body.append('articulationAudio', audio);
    const headers = this.authService.getMultipartHeaders();
    return this.http.post<any>(
      `${this.apiUrl}/admin/screening-category/articulation`, body,
      {
        headers,
        observe: 'response'
      }
    );
  }

  editArticulationPicture(id, correct_word, target_sound, picture, audio) {
    const body = new FormData();
    body.append('correct_word', correct_word);
    body.append('target_sound', target_sound);
    if (picture && picture[id]) {
      body.append('articulationPicture', picture[id]);
    }
    if (audio && audio[id]) {
      body.append('articulationAudio', audio[id]);
    }
    const headers = this.authService.getMultipartHeaders();
    return this.http.put<any>(
      `${this.apiUrl}/admin/screening-category/articulation/${id}`, body,
      {
        headers,
        observe: 'response'
      }
    );
  }

  editCommonQuestion(id, question) {
    return this.http.put<any>(
      `${this.apiUrl}/admin/screening-category/common/question/${id}`, { question },
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  addSpeechLanQuestion(body) {
    return this.http.post<any>(
      `${this.apiUrl}/admin/screening-category/speech-language/question`, body,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  editSpeechLanQuestion(id, body) {
    return this.http.put<any>(
      `${this.apiUrl}/admin/screening-category/speech-language/question/${id}`, body,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  addHearingQuestion(body) {
    return this.http.post<any>(
      `${this.apiUrl}/admin/screening-category/hearing/question`, body,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  editHearingQuestion(id, body) {
    return this.http.put<any>(
      `${this.apiUrl}/admin/screening-category/hearing/question/${id}`, body,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  getAllArticulationPictures() {
    return this.http.get<any>(
      `${this.apiUrl}/admin/screening-category/articulation`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  getAllSpeechLanDomains() {
    return this.http.get<any>(
      `${this.apiUrl}/admin/screening-category/speech-language/domains`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  getAllHearingScreeningTypes() {
    return this.http.get<any>(
      `${this.apiUrl}/admin/screening-category/hearing/types`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  getAllCommonScreeningQuestions(id) {
    return this.http.get<any>(
      `${this.apiUrl}/admin/screening-category/common/${id}`,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }

  addCommonScreeningQuestion(id, body) {
    return this.http.post<any>(
      `${this.apiUrl}/admin/screening-category/common/question/${id}`, body,
      {
        headers: this.authService.getHeaders(),
        observe: 'response'
      }
    );
  }
  notLoggedIn(error: Response) {
    if (error.status === 401) {
      localStorage.removeItem('admin-data');
      this.router.navigate(['/login']);
    }
  }
}
