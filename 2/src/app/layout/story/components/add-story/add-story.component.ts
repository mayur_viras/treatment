import { OnInit, Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { FormGroup, FormControl } from '@angular/forms';
import { MainService } from '../../../main.service';

@Component({
  selector: 'app-add-story',
  templateUrl: './add-story.component.html',
  styleUrls: ['./add-story.component.scss']
})
export class AddStoryComponent extends DialogComponent<any, any> implements OnInit {
  public form;
  public allTargetSounds: any;
  public errors = { title: '', content: '', targetSoundId: '', audio: '', picture: '' };
  public allStories: any;
  public isEdit: boolean;

  public story: any;
  public storyContent: any;
  public storyIndex: number;

  constructor(dialogService: DialogService, private mainService: MainService) {
    super(dialogService);
    this.form = new FormGroup({
      title: new FormControl(''),
      content: new FormControl(''),
      targetSoundId: new FormControl(''),
    });
  }


  addOrEditStory(data) {
    if (data.title && data.content && data.targetSoundId) {
      this.errors = { title: '', content: '', targetSoundId: '', audio: '', picture: '' };
      console.log(data, this.story, this.imageFileContent);
      data.picture = this.imageFileContent;
      data.audio = this.audioContent;
      this.mainService.addOrEditStory(data, this.isEdit ? this.story.id : '')
      .subscribe((res) => {
        console.log(res.body);

        if (!res.body.isError) {
          if (this.isEdit) {
            this.allStories[this.storyIndex].title = data.title;
            this.allStories[this.storyIndex].content = data.content;
            this.allStories[this.storyIndex].targetSoundId = data.targetSoundId;
            this.allStories[this.storyIndex].picture = res.body.data.picture;
            this.allStories[this.storyIndex].audio = res.body.data.audio;
          } else {
            this.allStories.push(res.body.data);
          }
          this.result = true;
          this.close();
        }
      }, (error) => {
        console.log(error, 'errr');
      });
    } else if (!data.title) {
      this.errors.title = 'title is required';
    } else if (!data.content) {
      this.errors.content = 'description is required';
    } else if (!data.targetSoundId) {
      this.errors.targetSoundId = 'targetSound is required';
    }
  }

  ngOnInit() {
    if (this.story) {
      this.mainService.getStoryContent(this.story.id).subscribe((res) => {
        this.storyContent = res.body.data.story;
        this.form = new FormGroup({
          title: new FormControl(res.body.data.story.title),
          content: new FormControl(res.body.data.story.content),
          targetSoundId: new FormControl(res.body.data.story.targetSoundId),
        });
      });
    }
  }
  public imageFileContent: File;
  public imagePath: Blob;
  public audioContent;
  public audioData;
  changeListener(event: any): void {
    try {
      if (event.target.files && event.target.files[0]) {
        this.errors.picture = '';
        if (!event.target.files[0].type || !event.target.files[0].type.includes('image')) {
          throw new Error('seems like you have uploaded some non-image file !!');
        }
        this.imageFileContent = event['target'].files[0];
        const reader = new FileReader();
        reader.onload = (event2: any) => {
          this.imagePath = event2.target.result;
        };
        reader.readAsDataURL(this.imageFileContent);
      }
    } catch (e) {
      window.scrollTo(0, 0);
      this.errors.picture = e.message;
      console.log(this.errors);
    }
  }
  changeAudioListener(event: any): void {
    try {
      if (event.target.files && event.target.files[0]) {
        this.errors.audio = '';
        if (!event.target.files[0].type || !event.target.files[0].type.includes('audio')) {
          throw new Error('seems like you have uploaded some non-audio file !!');
        }
        this.audioContent = event['target'].files[0];
        const reader = new FileReader();
        reader.onload = (event2: any) => {
          this.audioData = event2.target.result;
        };
        reader.readAsDataURL(this.audioContent);
      }
    } catch (e) {
      this.errors.audio = e.message;
      window.scrollTo(0, 0);
    }
  }
  clearImage() {
    this.imagePath = undefined;
    this.imageFileContent = undefined;
  }
  clearAudio() {
    this.audioData = undefined;
    this.audioContent = undefined;
  }
}
