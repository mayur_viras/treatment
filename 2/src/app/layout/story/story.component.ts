import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { MainService } from '../main.service';
import { Helper} from '../../shared';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { AddStoryComponent } from './components/add-story/add-story.component';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})
export class StoryComponent implements OnInit {
  public isContentLoading = false;
  public alerts: Array<any> = [];
  public allStories: any = [];
  public allTargetSounds: any = ['common'];
  public viewRef: any;
  constructor(
    public router: Router, private mainService: MainService, private helper: Helper,
    private dialogService: DialogService
  ) {
    this.router.events.subscribe((event: object) => {
      this.isContentLoading = (event instanceof NavigationStart) ? true : this.isContentLoading;
      this.isContentLoading = (event instanceof NavigationEnd) ? false : this.isContentLoading;
    });
    this.getAllStory();
    this.getStoryTargetSounds();
  }

  ngOnInit() {}
  getAllStory() {
    this.mainService.getAllStory()
    .subscribe((res) => {
      this.allStories = [];
      if (res.body.isError) {
        this.alerts.push({ type: 'danger', message: res.body.message });
      } else {
        this.allStories = res.body.data.stories;
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  getStoryTargetSounds() {
    this.mainService.getStoryTargetSounds()
    .subscribe((res) => {
      this.allTargetSounds = [];
      if (res.body.isError) {
        this.alerts.push({ type: 'danger', message: res.body.message });
      } else {
        this.allTargetSounds = res.body.data.target_sounds;
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }

  addOrEditStory(index?: number) {
    const faqData: any = {
      mainService: this.mainService,
      allStories: this.allStories,
      allTargetSounds: this.allTargetSounds,
    };
    if (index !== undefined) {
      faqData.story = this.allStories[index];
      faqData.storyIndex = index;
      faqData.isEdit = true;
    }
    this.viewRef = this.dialogService.addDialog(AddStoryComponent, faqData).subscribe((res: any) => {
    });
  }

  removeStory(index, event) {
    if (this.allStories[index]) {
      this.dialogService.addDialog(ConfirmComponent, { message: 'Are you sure you want to remove this story?' }).subscribe((res: any) => {
        if (res) {
          this.mainService.removeStory(this.allStories[index].id)
          .subscribe((res) => {
            if (res.body.isError) {
              this.alerts.push({ type: 'danger', message: res.body.message });
            } else {
              this.allStories.splice(index, 1);
            }
          }, (error) => {
            this.mainService.notLoggedIn(error);
            console.log(error, 'errr');
          });
        }
      });
    }
  }
}
