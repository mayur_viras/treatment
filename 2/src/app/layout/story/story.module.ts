import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { StoryRoutingModule } from './story-routing.module';
import { StoryComponent } from './story.component';
import { AddStoryComponent } from './components/add-story/add-story.component';
import { LoaderModule, AlertModule, AuthService } from '../../shared';
import { FileUploadModule } from 'ng2-file-upload';
import { MainService } from '../main.service';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { ConfirmComponent } from './components/confirm/confirm.component';
@NgModule({
  imports: [
    AlertModule,
    CommonModule,
    FormsModule,
    AngularMultiSelectModule,
    ChartsModule,
    FileUploadModule,
    StoryRoutingModule,
    LoaderModule,
    ReactiveFormsModule,
    BootstrapModalModule.forRoot({ container: document.body })
  ],
  declarations: [
    StoryComponent,
    AddStoryComponent,
    ConfirmComponent
  ],
  entryComponents: [
    AddStoryComponent,
    ConfirmComponent
  ],
  providers: [
    MainService,
    AuthService,
  ],
})

export class StoryModule { }
