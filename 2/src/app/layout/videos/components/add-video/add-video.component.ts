import { OnInit, Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { FormGroup, FormControl } from '@angular/forms';
import { constants } from '../../../../shared';
import { MainService } from '../../../main.service';

@Component({
  selector: 'app-add-video',
  templateUrl: './add-video.component.html',
  styleUrls: ['./add-video.component.scss']
})
export class AddVideoComponent extends DialogComponent<any, any> implements OnInit {
  public allLanguages = ['English', 'Hindi', 'Kannada'];
  public form;
  public allCategories: any;
  public errors = { videoTitle: '', language: '', video: '', category: '' };
  public videoContent;
  public videoData;
  public allVideos: any;
  public video: any;
  public isEdit: any;
  constructor(dialogService: DialogService, private mainService: MainService) {
    super(dialogService);
    this.form = new FormGroup({
      videoTitle: new FormControl(''),
      language: new FormControl('English'),
      category: new FormControl(''),
      video: new FormControl('')
    });
  }
  ngOnInit() {
    if (this.video) {
      this.form = new FormGroup({
        videoTitle: new FormControl(this.video.title),
        language: new FormControl(this.video.language),
        category: new FormControl(this.video.category),
        video: new FormControl(this.video.video)
      });
    }
  }
  changeListener(event: any): void {
    try {
      if (event.target.files && event.target.files[0]) {
        console.log(event.target.files[0])
        if (!event.target.files[0].type || !event.target.files[0].type.includes('video')) {
          throw new Error('seems like you have uploaded some non-video file !!');
        }
        this.videoContent = event['target'].files[0];
        const reader = new FileReader();
        reader.onload = (event2: FileReaderProgressEvent) => {
          this.errors.video = '';
          this.videoData = event2.target.result;
        };
        reader.readAsDataURL(this.videoContent);
      }
    } catch (e) {
      window.scrollTo(0, 0);
      this.errors.video = e.message;
    }
  }
  clearImage() {
    this.videoContent = undefined;
    this.videoData = undefined;
  }

  addNewVideo(data) {
    if (data.videoTitle && data.language && data.category && data.video) {
      this.errors = { videoTitle: '', language: '', video: '', category: '' };
      this.mainService.addEducationalVideo(data, null, this.isEdit ? this.video.id : null)
      .subscribe((res) => {
        if (!res.body.isError) {
          if (!this.isEdit) {
            this.allVideos.push(res.body.data.video);
          } else {
            this.video.title = data.videoTitle;
            this.video.language = data.language;
            this.video.category = data.category;
            this.video.video = data.video;
          }
          this.result = true;
          this.close();
        } else {
          alert(res.body.message);
        }
      }, (error) => {
        alert(error)
      });
    } else if (!data.videoTitle) {
      this.errors.videoTitle = 'video title is required';
    } else if (!data.language) {
      this.errors.language = 'language is required';
    } else if (!data.video) {
      this.errors.video = 'video is required';
    } else if (!data.category) {
      this.errors.category = 'category is required';
    }
  }
}
