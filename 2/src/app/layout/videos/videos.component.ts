import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { MainService } from '../main.service';
import { NgZone } from '@angular/core';
import { AlertSchema, Helper, ModalComponent, constants } from '../../shared';
import { AddVideoComponent } from './components/add-video/add-video.component';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {
  public isContentLoading = false;
  @Output() notify: EventEmitter<Object> = new EventEmitter<Object>();
  public alerts: Array<any> = [];
  public allVideos: any = [];
  public allCategories: any = ['common'];
  constructor(
    public router: Router, private mainService: MainService, private helper: Helper, private dialogService: DialogService, sanitizer: DomSanitizer
  ) {
    this.router.events.subscribe((event: object) => {
      this.isContentLoading = (event instanceof NavigationStart) ? true : this.isContentLoading;
      this.isContentLoading = (event instanceof NavigationEnd) ? false : this.isContentLoading;
    });
    this.getAllVideos();
    this.getCategories();
  }

  ngOnInit() {}
  getAllVideos() {
    this.mainService.getAllVideos()
    .subscribe((res) => {
      this.allVideos = [];
      if (res.body.isError) {
      } else {
        this.allVideos = res.body.data.videos;
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }
  getCategories() {
    this.mainService.getCategories('videos')
    .subscribe((res) => {
      this.allCategories = [];
      if (res.body.isError) {
        this.alerts.push({ type: 'danger', message: res.body.message });
      } else {
        this.allCategories = res.body.data.categories;
      }
    }, (error) => {
      this.mainService.notLoggedIn(error);
      console.log(error, 'errr');
    });
  }

  addNewVideo() {
    const viewRef = this.dialogService.addDialog(AddVideoComponent, {
      mainService: this.mainService,
      allVideos: this.allVideos,
      allCategories: this.allCategories,
    }).subscribe((res: any) => {
      console.log(res, 'resss');
    });
  }

  editVideo(index) {
    const viewRef = this.dialogService.addDialog(AddVideoComponent, {
      mainService: this.mainService,
      allVideos: this.allVideos,
      allCategories: this.allCategories,
      video: this.allVideos[index],
      isEdit: true
    }).subscribe((res: any) => {
      if (res) {
        this.getCategories();
      }
    });
  }

  removeVideo(index) {
    if (this.allVideos[index]) {
      this.dialogService.addDialog(ConfirmComponent, { message: 'Are you sure you want to remove this video?' }).subscribe((res: any) => {
        if (res) {
          this.mainService.removeVideo(this.allVideos[index].id)
          .subscribe((res) => {
            if (res.body.isError) {
              this.alerts.push({ type: 'danger', message: res.body.message });
            } else {
              this.allVideos.splice(index, 1);
            }
          }, (error) => {
            this.mainService.notLoggedIn(error);
          });
        }
      });
    }
  }
}
