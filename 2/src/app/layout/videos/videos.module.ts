import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { VideosRoutingModule } from './videos-routing.module';
import { VideosComponent } from './videos.component';
import { AddVideoComponent } from './components/add-video/add-video.component';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { LoaderModule, AlertModule, AuthService } from '../../shared';
import { FileUploadModule } from 'ng2-file-upload';
import { MainService } from '../main.service';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
@NgModule({
  imports: [
    AlertModule,
    CommonModule,
    FormsModule,
    AngularMultiSelectModule,
    ChartsModule,
    FileUploadModule,
    VideosRoutingModule,
    LoaderModule,
    ReactiveFormsModule,
    BootstrapModalModule.forRoot({ container: document.body })
  ],
  declarations: [
    VideosComponent,
    AddVideoComponent,
    ConfirmComponent
  ],
  entryComponents: [
    AddVideoComponent,
    ConfirmComponent
  ],
  providers: [
    MainService,
    AuthService,
  ],
})

export class VideosModule { }
