import { NgModule } from '@angular/core';
import { Routes, Router, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';


const routes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class LoginRoutingModule {

  constructor(public router: Router) {
    if (localStorage.getItem('admin-data')) {
      this.router.navigate(['/dashboard']);
    }
  }

}
