import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { FormGroup, FormControl } from '@angular/forms';
import * as CryptoJS from 'crypto-js';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  public form: FormGroup;
  public errors: { email: string, password: string, server: string } = { email: '', password: '', server: '' };
  public isInProgress = false;

  constructor(public router: Router, private loginService: LoginService) {
    this.form = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  onLoggedIn(data: { email: string, password: string }) {
    if (!data.email || data.email.trim() === '') {
      this.errors.email = 'Email is required';
    } else if (!data.password || data.password === '') {
      this.errors.password = 'Password is required';
      this.errors.email = '';
    } else {

      this.isInProgress = true;
      this.errors.email = '';
      this.errors.password = '';
      this.loginService.loginUser(data)
      .subscribe((res) => {
        if (res.body.isError) {
          this.errors.server = res.body.message;
        } else {
          const encryptedData: string = CryptoJS.AES.encrypt(JSON.stringify(res.body.data), 'admin-data').toString();
          localStorage.setItem('admin-data', encryptedData);
          this.router.navigate(['/dashboard']);
        }
        this.isInProgress = false;
      }, (error) => {
        this.isInProgress = false;
        this.errors.server = error.error.message || error.error.toString();
      });
    }
  }
}
