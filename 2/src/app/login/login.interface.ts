
export interface LoginResponse {
  isError: boolean;
  message: string;
  data: {
    token: string;
  };
}
