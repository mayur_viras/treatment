import { Injectable } from '@angular/core';
import { constants } from '../shared';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { LoginResponse } from './login.interface';
import { Observable } from 'rxjs';


@Injectable()
export class LoginService {
  headers: HttpHeaders;
  public apiUrl: string = constants.API_URL;
  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type':  'application/json',
    });
  }

  loginUser(user: object): Observable<HttpResponse<LoginResponse>> {
    return this.http.post<LoginResponse>(`${this.apiUrl}/admin/login`, user, {
      headers: this.headers,
      observe: 'response'
    });
  }
}
