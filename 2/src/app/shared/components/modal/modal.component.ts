import { OnInit, Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})

export class ModalComponent extends DialogComponent<any, any> implements OnInit {

  constructor(dialogService: DialogService) {
    super(dialogService);
  }
  ngOnInit() {
    console.log('on-init');
  }
}
