import { environment } from '../../environments/environment';

let publicUrl = 'http://localhost:8080/public/';
let apiUrl = 'http://localhost:8080/api/v1';
if (environment.production) {
  publicUrl = '/public/';
  apiUrl = '/api/v1';
}

export const constants = {

  API_HOST: 'localhost',

  API_PORT: 3000,
 
  PUBLIC_URL: publicUrl,
  API_URL: apiUrl,
  CATEGORY_TYPES: {
    '1': 'picture',
    '2': 'yes-no questions'
  },
  CATEGORY_IDS: {
    ARTICULATION: 1,
    SPEECH_AND_LANGUAGE: 2,
    HEARING: 3,
    FLUENCY: 4,
    VOICE: 5,
    BEHAVIORS: 6
  },
  SPEECH_AND_LANGUAGE_AGE_RANGES: [
    { age_start: 0, age_end: 3 },
    { age_start: 4, age_end: 6 },
    { age_start: 7, age_end: 9 },
    { age_start: 10, age_end: 12 },
    { age_start: 13, age_end: 15 },
    { age_start: 16, age_end: 18 },
    { age_start: 19, age_end: 21 },
    { age_start: 22, age_end: 24 },
    { age_start: 25, age_end: 27 },
    { age_start: 28, age_end: 30 },
    { age_start: 31, age_end: 36 },
    { age_start: 37, age_end: 42 },
    { age_start: 43, age_end: 48 },
    { age_start: 49, age_end: 54 },
    { age_start: 55, age_end: 60 }
  ],
  GAMES: {
    IDENTIFICATION: 1,
    PICTURE_MATCHING: 2,
    SHADOW_MATCHING: 3,
    WHAT_DO_I_DO: 4,
    MEMORY: 5,
    MEMORY_AND_SEQUENCING: 6,
    WHAT_DO_YOU_USE_FOR: 7,
    GUESS_ME: 8,
    AUDITORY_DISCRIMINATION: 9,
    FIND_THE_PATH: 10,
    PUZZLES: 11,
    TRACE_IT: 12,
    ODD_ONE_OUT: 13,
    VOCABULARY: 999
  }
};
