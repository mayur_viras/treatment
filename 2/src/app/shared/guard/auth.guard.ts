import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot) {
    if (localStorage.getItem('admin-data')) {
      const bytes = CryptoJS.AES.decrypt(localStorage.getItem('admin-data'), 'admin-data');
      const user: { token: string } = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      if (user) {
        return true;
      } else {
        this.router.navigate(['/not-found']);
        return false;
      }
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
