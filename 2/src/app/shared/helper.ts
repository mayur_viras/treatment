import * as CryptoJS from 'crypto-js';

export class Helper {
  toCamelCase(json: any): any {
    let newO, newKey, value, origKey;
    if (json instanceof Array) {
      newO = [];
      for (origKey in json) {
        if (json[origKey]) {
          value = json[origKey];
          if (typeof value === 'object') {
            value = this.toCamelCase(value);
          }
          newO.push(value);
        }
      }
    } else {
      newO = {};
      for (origKey in json) {
        if (json.hasOwnProperty(origKey)) {
          newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString();
          value = json[origKey];
          if ((typeof value !== 'undefined' && value !== null && value.constructor === Object) || value instanceof Array) {
            value = this.toCamelCase(value);
          }
          newO[newKey] = value;
        }
      }
    }
    return newO;
  }

}
