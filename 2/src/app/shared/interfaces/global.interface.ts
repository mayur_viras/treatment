
export interface HTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}

export interface FileReaderEventTarget extends EventTarget {
  result: Blob;
}

export interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
  getMessage(): string;
}

export interface CommonResponseSchema {
  isError: boolean;
  message: string;
}
