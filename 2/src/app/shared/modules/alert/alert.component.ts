import { Component, Input } from '@angular/core';
import { AlertSchema } from './alert.interface';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})

export class AlertComponent {

  @Input() alerts: Array<AlertSchema>;

  closeAlert(alert: AlertSchema) {
    const index: number = this.alerts.indexOf(alert);
    this.alerts.splice(index, 1);
  }
}
