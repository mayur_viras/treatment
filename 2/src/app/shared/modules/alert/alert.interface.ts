
export interface AlertSchema {
  type: string;
  message: string;
}
