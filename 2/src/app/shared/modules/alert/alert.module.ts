import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AlertComponent } from './alert.component';

import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbAlertModule.forRoot(),
  ],
  declarations: [AlertComponent],
  exports: [AlertComponent]
})
export class AlertModule { }
