import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class AuthService {

  getHeaders() {

    const bytes = CryptoJS.AES.decrypt(localStorage.getItem('admin-data'), 'admin-data');
    const user: { token: string } = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    const headers: HttpHeaders = new HttpHeaders({
      'Content-Type':  'application/json',
      'x-access-token': (user && user.token) ? user.token : null
    });
    return headers;
  }

  getMultipartHeaders() {

    const bytes = CryptoJS.AES.decrypt(localStorage.getItem('admin-data'), 'admin-data');
    const user: { token: string } = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

    const headers: HttpHeaders = new HttpHeaders({
      'Accept': 'application/json',
      'x-access-token': (user && user.token) ? user.token : null
    });
    return headers;
  }

}
