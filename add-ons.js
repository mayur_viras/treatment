const db = require('./models');
const start = async () => {
  const data = {
    qualification: [
      { name: 'BASLP' },
      { name: 'MASLP' },
      { name: 'M.Sc. SLP' },
      { name: 'M.Sc. AudioLogy' }
    ],
    designation: [
      { name: 'Audiologist' },
      { name: 'Speech Language Pathologist' },
      { name: 'Other' }
    ],
    working_at: [
      { name: 'Own CLinic' },
      { name: 'Teaching Institute' },
      { name: 'School' },
      { name: 'NGO' },
      { name: 'Other' }
    ],
    identity_proof: [
      { name: 'ISHA Membership Proof' },
      { name: 'RCI Number' },
      { name: 'Other' }
    ],
    consultation_currency: [
      { name: 'USD($)' },
      { name: 'INR' }
    ],
    student_course: [
      { name: 'BASLP' },
      { name: 'MASLP' },
      { name: 'M.Sc. SLP' },
      { name: 'M.Sc. AudioLogy' },
      { name: 'Other' }
    ],
    student_language: [
      { name: 'Hindi' },
      { name: 'English' },
      { name: 'Gujarati' },
      { name: 'Other' }
    ]
  };
  for (const model in data) {
    const list = await db[model].findAll();
    if (!list.length) {
      data[model].forEach(async (element) => {
        await db[model].create(element);
      });
    } else {
      console.log(list.length, model);
    }
  }
  console.log('done');
};
start();
module.exports = start;
