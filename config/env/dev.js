const sequelizeAllias = require('../../helpers/sequelize-allias');

module.exports = {
  application: {
    env: 'development',
    port: 8080
  },
  jwtConfig: {
    secret: 'eu2y67$%^hwbg87',
    path: ['/user/login', '/user/password/forget', '/user/password/reset', '/admin/login', '/admin/logout']
  },
  emailConfig: {
    sender: '',
    username: '',
    password: '',
    name: ''
  },
  bugsnagConfig: { apiKey: 'abcd' },
  databaseConfig: {
    username: 'r0134323_inf',
    password: 'h1F85rTyTrn8',
    database: 'r0134323_infinity',
    host: 'johnny.heliohost.org',
    dialect: 'mysql',
    dialectOptions: { decimalNumbers: true },
    logging: false,
    operatorsAliases: sequelizeAllias,
    pool: {
      max: 20,
      min: 0,
      idle: 20000,
      acquire: 20000,
      handleDisconnects: true
    }
  }
};
