const sequelizeAllias = require('../../helpers/sequelize-allias');

module.exports = {
  application: {
    env: 'development',
    port: 8080
  },
  jwtConfig: {
    secret: 'eu2y67$%^hwbg87',
    path: ['/user/login', '/user/password/forget', '/user/password/reset', '/admin/login', '/admin/logout']
  },
  emailConfig: {
    sender: '',
    username: '',
    password: '',
    name: ''
  },
  bugsnagConfig: { apiKey: 'abcd' },
  databaseConfig: {
    username: 'root',
    password: 'root',
    database: 'infinity',
    host: 'localhost',
    dialect: 'mysql',
    dialectOptions: { decimalNumbers: true },
    logging: false,
    operatorsAliases: sequelizeAllias,
    pool: {
      max: 20,
      min: 0,
      idle: 20000,
      acquire: 20000,
      handleDisconnects: true
    }
  }
};
