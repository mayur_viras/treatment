/* eslint-disable brace-style, max-statements-per-line */
const respGenerator = require('../response/json-response');
const db = require('../models');
class AdminController {
  async getAllCountries(req, res) {
    try {
      const allCountries = await db.country.findAll({
        where: { isDeleted: false },
        orderBy: [['id']],
        attributes: ['name', 'id']
      });
      respGenerator.sendResponse(res, { allCountries });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async addNewCountry(req, res) {
    try {
      if (!req.body.name) {
        throw new Error('please provide country name');
      }
      const country = await db.country.create({ name: req.body.name });
      respGenerator.sendResponse(res, { country });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async removeCountry(req, res) {
    try {
      await db.country.update({ isDeleted: true }, { where: { id: parseInt(req.params.id) } });
      await db.state.update({ isDeleted: true }, { where: { country_id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { message: 'deleted' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async editCountry(req, res) {
    try {
      if (!req.body.name) {
        throw new Error('please provide country name');
      }
      await db.country.update({ name: req.body.name }, { where: { id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { message: 'updated' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllState(req, res) {
    try {
      let country_id = parseInt(req.params.id);
      if (req.method.toLowerCase() == 'POST' && req.params.id == undefined) {
        country_id = parseInt(req.body.id);
      }
      const allStates = await db.state.findAll({
        where: { isDeleted: false, country_id },
        orderBy: [['id']],
        attributes: ['name', 'id']
      });
      respGenerator.sendResponse(res, { allStates });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async addNewState(req, res) {
    try {
      if (!req.body.name) {
        throw new Error('please provide state name');
      }
      if (!req.body.countryId) {
        throw new Error('please provide country id');
      }
      const state = await db.state.create({ name: req.body.name, country_id: req.body.countryId });
      respGenerator.sendResponse(res, { state });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async removeState(req, res) {
    try {
      await db.state.update({ isDeleted: true }, { where: { id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { message: 'deleted' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async editState(req, res) {
    try {
      if (!req.body.name) {
        throw new Error('please provide state name');
      }
      await db.state.update({ name: req.body.name }, { where: { id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { message: 'updated' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async getAllCity(req, res) {
    try {
      let state_id = parseInt(req.params.id);
      if (req.method.toLowerCase() == 'POST' && req.params.id == undefined) {
        state_id = parseInt(req.body.id);
      }
      const allCity = await db.city.findAll({
        where: { isDeleted: false, state_id },
        orderBy: [['id']],
        attributes: ['name', 'id']
      });
      respGenerator.sendResponse(res, { allCity });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async addNewCity(req, res) {
    try {
      if (!req.body.name) {
        throw new Error('please provide city name');
      }
      if (!req.body.stateId) {
        throw new Error('please provide state id');
      }
      const city = await db.city.create({ name: req.body.name, state_id: req.body.stateId });
      respGenerator.sendResponse(res, { city });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async removeCity(req, res) {
    try {
      await db.city.update({ isDeleted: true }, { where: { id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { message: 'deleted' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async editCity(req, res) {
    try {
      if (!req.body.name) {
        throw new Error('please provide city name');
      }
      await db.city.update({ name: req.body.name }, { where: { id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { message: 'updated' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllParams(req, res) {
    try {
      const allParams = await db[req.params.param_id].findAll({
        where: { isDeleted: false },
        orderBy: [['id']],
        attributes: ['name', 'id']
      });
      respGenerator.sendResponse(res, { allParams });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async addNewParam(req, res) {
    try {
      if (!req.body.name) {
        throw new Error('please provide valid name');
      }
      const param = await db[req.params.param_id].create({ name: req.body.name });
      respGenerator.sendResponse(res, { param });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async removeParam(req, res) {
    try {
      await db[req.params.param_id].update({ isDeleted: true }, { where: { id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { message: 'deleted' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async editParam(req, res) {
    try {
      if (!req.body.name) {
        throw new Error('please provide valid name');
      }
      await db[req.params.param_id].update({ name: req.body.name }, { where: { id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { message: 'updated' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
}
module.exports = new AdminController();
