/* eslint-disable brace-style, max-statements-per-line */
const jwt = require('jsonwebtoken');
const { jwtConfig } = require('../config');
const respGenerator = require('../response/json-response');
const db = require('../models');
const mappers = require('../models/mapper/index');
const fs = require('fs');
const { CATEGORY_IDS, GAMES, PUBLIC_URL } = require('../response/constants');
const queryHelper = require('../lib/query');
const _ = require('lodash');

class UserController {

  login(req, res) {
    try {
      const user = {
        email: 'admin@xyz.com',
        password: 'msbJKQJ!#@3455'
      };
      if (user.email !== req.body.email) { throw new Error(105); }
      if (req.body.password !== user.password.toString()) { throw new Error(106); }
      const data = {
        id: 1111,
        email: user.email,
        role: 'Admin',
        type: 'Admin'
      };
      const token = jwt.sign(data, jwtConfig.secret);
      // insert token in database
      respGenerator.sendResponse(res, { token });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  logout(req, res) {
    try {
      // delete token from db
      respGenerator.sendResponse(res, { message: 'you have been logged out from this device' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllGames(req, res) {
    try {
      const allGames = await db.game.findAll({ orderBy: [['id']] });
      respGenerator.sendResponse(res, { allGames });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllEducationalVideos(req, res) {
    try {
      const where = {};
      let isEmpty = true;
      // if (req.query.category) { where.category = req.query.category.toLowerCase().trim(); }
      // if (req.query.language) { where.language = req.query.language.trim(); }
      if (req.query.category) {
        isEmpty = false;
        where[db.op.and] = [{ category: req.query.category.toLowerCase().trim() }];
      }
      if (req.query.language) {
        if (isEmpty) {
          where[db.op.and] = [{ language: req.query.language.trim() }];
        } else {
          where[db.op.and].push({ language: req.query.language.trim() });
        }
      }
      if (req.query.search && req.query.search.toString().trim().length >= 3) {
        if (isEmpty) {
          where[db.op.or] = [{ title: { [db.op.like]: `%${req.query.search.toString().trim()}%` } }];
        } else {
          where[db.op.and].push({
            [db.op.or]: [
              { title: { [db.op.like]: `%${req.query.search.toString().trim()}%` } }
            ]
          });
        }
      }
      let limit = 15;
      if (!isNaN(req.query.limit) && parseInt(req.query.limit) > 0) {
        limit *= (parseInt(req.query.limit) - 1);
      }
      let offset = 0;
      if (!isNaN(req.query.pageno) && parseInt(req.query.pageno) > 0) {
        offset = limit * (parseInt(req.query.pageno) - 1);
      }
      const videos = await db.educational_videos.findAll({ where, limit, offset, attributes: ['id', 'title', 'language', 'category', 'video'] }, { order: [['id']] });
      videos.forEach((video) => {
        video.dataValues.videoList = video.dataValues.video.split(',').map(videoValue => videoValue.trim());
      });
      if (videos.length) {
        respGenerator.sendResponse(res, { videos });
      } else {
        res.send({ isError: true, message: 'no data found' });
      }
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addEducationalVideo(req, res) {
    try {
      queryHelper.validateEducationalVideoBody(req.body);
      // const videoLink = `educational-videos-${Math.random().toString(36).substring(2)}-${req.files.video.name}`;
      // fs.writeFileSync(`${__dirname}/../public/${videoLink}`, req.files.video.data);
      const video = await db.educational_videos.create({
        video: req.body.video,
        title: req.body.videoTitle,
        language: req.body.language,
        category: req.body.category.toString().toLowerCase().trim()
      });
      respGenerator.sendResponse(res, { video });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async editEducationalVideo(req, res) {
    try {
      queryHelper.validateEducationalVideoBody(req.body);
      // const videoLink = `educational-videos-${Math.random().toString(36).substring(2)}-${req.files.video.name}`;
      // fs.writeFileSync(`${__dirname}/../public/${videoLink}`, req.files.video.data);
      const video = await db.educational_videos.update({
        video: req.body.video,
        title: req.body.videoTitle,
        language: req.body.language,
        category: req.body.category.toString().toLowerCase().trim()
      }, { where: { id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { video, message: 'updated' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async removeEducationalVideo(req, res) {
    try {
      const video = await db.educational_videos.findOne({ where: { id: req.params.id } });
      if (!video) { throw new Error(142); }
      // if (fs.existsSync(`${__dirname}/../public/${video.link}`)) {
      //   fs.unlink(`${__dirname}/../public/${video.link}`);
      // }
      await db.educational_videos.destroy({ where: { id: req.params.id } });
      respGenerator.sendResponse(res, { message: 'video has been removed' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async removeArticulationItem(req, res) {
    try {
      const picture = await db.atriculation_picture.findOne({ where: { id: req.params.id } });
      if (!picture) { throw new Error(142); }
      if (fs.existsSync(`${__dirname}/../public/${picture.picture_url}`)) {
        fs.unlink(`${__dirname}/../public/${picture.picture_url}`);
      }
      if (fs.existsSync(`${__dirname}/../public/${picture.audio_url}`)) {
        fs.unlink(`${__dirname}/../public/${picture.audio_url}`);
      }
      await db.atriculation_picture.destroy({ where: { id: req.params.id } });
      respGenerator.sendResponse(res, { message: 'picture has been removed' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllFeedback(req, res) {
    try {
      const feedback = await db.feedback.findAll({}, { orderBy: [['id']] });
      respGenerator.sendResponse(res, { feedback });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async removeFeedback(req, res) {
    try {
      await db.feedback.destroy({ where: { id: req.params.id } });
      respGenerator.sendResponse(res, { message: 'feedback has been removed' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async changeVisibility(req, res) {
    try {
      const feedback = await db.feedback.findOne({ where: { id: req.params.id } });
      if (!feedback) { throw new Error(142); }
      await db.feedback.update({ isVisibleToAdminOnly: !feedback.isVisibleToAdminOnly }, { where: { id: req.params.id } });
      respGenerator.sendResponse(res, { message: 'feedback has been updated' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addNewFaq(req, res) {
    try {
      if (!req.body.title) { throw new Error(138); }
      if (!req.body.language) { throw new Error(139); }
      if (!req.body.description) { throw new Error(141); }
      if (!req.body.category) { throw new Error(159); }
      let image = '';
      const video = req.body.video;
      if (req.files && req.files.image) {
        image = `${Math.random().toString(36).substring(2)}-${req.files.image.name}`;
        fs.writeFileSync(`${__dirname}/../public/${image}`, req.files.image.data);
      }
      // if (req.files && req.files.video) {
      //   video = `${Math.random().toString(36).substring(2)}-${req.files.video.name}`;
      //   fs.writeFileSync(`${__dirname}/../public/${video}`, req.files.video.data);
      // }
      const faq = await db.faq.create({ title: req.body.title, description: req.body.description, language: req.body.language, image, video, category: req.body.category.toString().toLowerCase().trim() });
      if (image) {
        faq.dataValues.image = `${PUBLIC_URL}/${faq.dataValues.image}`;
      }
      // if (video) {
      //   faq.dataValues.video = `${PUBLIC_URL}/${faq.dataValues.video}`;
      // }
      respGenerator.sendResponse(res, { faq });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async editFaq(req, res) {
    try {
      if (!req.body.title) { throw new Error(138); }
      if (!req.body.language) { throw new Error(139); }
      if (!req.body.description) { throw new Error(141); }
      if (!req.body.category) { throw new Error(159); }
      const faqData = await db.faq.findOne({ where: { id: req.params.id } });
      let image = faqData.dataValues.image;
      const video = req.body.video;
      if (req.files && req.files.image) {
        image = `${Math.random().toString(36).substring(2)}-${req.files.image.name}`;
        fs.writeFileSync(`${__dirname}/../public/${image}`, req.files.image.data);
        fs.unlink(`${__dirname}/../public/${faqData.image}`);
      }
      // if (req.files && req.files.video) {
      //   video = `${Math.random().toString(36).substring(2)}-${req.files.video.name}`;
      //   fs.writeFileSync(`${__dirname}/../public/${video}`, req.files.video.data);
      //   fs.unlink(`${__dirname}/../public/${faqData.video}`);
      // }
      await db.faq.update({ title: req.body.title, description: req.body.description, language: req.body.language, image, video, category: req.body.category.toString().toLowerCase().trim() }, { where: { id: req.params.id } });
      if (image) { image = `${PUBLIC_URL}/${image}`; }
      // if (video) { video = `${PUBLIC_URL}/${video}`; }
      respGenerator.sendResponse(res, {
        faq: {
          id: req.params.id,
          title: req.body.title,
          description: req.body.description,
          language: req.body.language,
          category: req.body.category.toString().toLowerCase().trim(),
          image,
          video
        }
      });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllFaq(req, res) {
    try {
      const where = {};
      if (req.query.category) {
        where.category = req.query.category.toLowerCase().trim();
      }
      if (req.query.language) {
        where.language = req.query.language.trim();
      }
      const limit = 15;
      let offset = 0;
      if (!isNaN(req.query.pageno) && parseInt(req.query.pageno) > 0) {
        offset = limit * (parseInt(req.query.pageno) - 1);
      }
      const faq = await db.faq.findAll({ where, limit, offset, attributes: ['id', 'title', 'description', 'image', 'video', 'category', 'language'] }, { order: [['id']] });
      faq.forEach((faqData) => {
        if (faqData.dataValues.image) {
          faqData.dataValues.image = `${PUBLIC_URL}/${faqData.dataValues.image}`;
        }
        // if (faqData.dataValues.video) {
        //   faqData.dataValues.video = `${PUBLIC_URL}/${faqData.dataValues.video}`;
        // }
      });
      if (faq.length) {
        respGenerator.sendResponse(res, { faq });
      } else {
        res.send({ isError: true, message: 'no data found' });
      }
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async removeFaq(req, res) {
    try {
      const faq = await db.faq.findOne({ where: { id: req.params.id } });
      await db.faq.destroy({ where: { id: req.params.id } });
      if (faq.image && fs.existsSync(`${__dirname}/../public/${faq.image}`)) {
        fs.unlink(`${__dirname}/../public/${faq.image}`);
      }
      // if (faq.video && fs.existsSync(`${__dirname}/../public/${faq.video}`)) {
      //   fs.unlink(`${__dirname}/../public/${faq.video}`);
      // }
      respGenerator.sendResponse(res, { message: 'faq has been removed' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addNewBlog(req, res) {
    try {
      if (!req.body.title) { throw new Error(138); }
      if (!req.body.language) { throw new Error(139); }
      if (!req.body.description) { throw new Error(141); }
      if (!req.body.category) { throw new Error(159); }
      let image = '';
      const video = req.body.video;
      if (req.files && req.files.image) {
        image = `${Math.random().toString(36).substring(2)}-${req.files.image.name}`;
        fs.writeFileSync(`${__dirname}/../public/${image}`, req.files.image.data);
      }
      // if (req.files && req.files.video) {
      //   video = `${Math.random().toString(36).substring(2)}-${req.files.video.name}`;
      //   fs.writeFileSync(`${__dirname}/../public/${video}`, req.files.video.data);
      // }
      const blog = await db.blog.create({ title: req.body.title, description: req.body.description, language: req.body.language, image, video, category: req.body.category.toString().toLowerCase().trim() });
      if (image) {
        blog.dataValues.image = `${PUBLIC_URL}/${blog.dataValues.image}`;
      }
      if (video) {
        blog.dataValues.video = `${PUBLIC_URL}/${blog.dataValues.video}`;
      }
      respGenerator.sendResponse(res, { blog });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async editBlog(req, res) {
    try {
      if (!req.body.title) { throw new Error(138); }
      if (!req.body.language) { throw new Error(139); }
      if (!req.body.description) { throw new Error(141); }
      if (!req.body.category) { throw new Error(159); }
      const blogData = await db.blog.findOne({ where: { id: parseInt(req.params.id) } });
      let image = blogData.dataValues.image;
      const video = req.body.video;
      if (req.files && req.files.image) {
        image = `${Math.random().toString(36).substring(2)}-${req.files.image.name}`;
        fs.writeFileSync(`${__dirname}/../public/${image}`, req.files.image.data);
        fs.unlink(`${__dirname}/../public/${blogData.image}`);
      }
      // if (req.files && req.files.video) {
      //   video = `${Math.random().toString(36).substring(2)}-${req.files.video.name}`;
      //   fs.writeFileSync(`${__dirname}/../public/${video}`, req.files.video.data);
      //   fs.unlink(`${__dirname}/../public/${blogData.video}`);
      // }
      await db.blog.update({ title: req.body.title, description: req.body.description, language: req.body.language, image, video, category: req.body.category.toString().toLowerCase().trim() }, { where: { id: req.params.id } });
      if (image) { image = `${PUBLIC_URL}/${image}`; }
      // if (video) { video = `${PUBLIC_URL}/${video}`; }
      respGenerator.sendResponse(res, {
        blog: {
          id: req.params.id,
          title: req.body.title,
          description: req.body.description,
          language: req.body.language,
          category: req.body.category.toString().toLowerCase().trim(),
          image,
          video
        }
      });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllBlog(req, res) {
    try {
      const where = {};
      let isEmpty = true;
      if (req.query.category) {
        isEmpty = false;
        where[db.op.and] = [{ category: req.query.category.toLowerCase().trim() }];
      }
      if (req.query.language) {
        if (isEmpty) {
          where[db.op.and] = [{ language: req.query.language.trim() }];
        } else {
          where[db.op.and].push({ language: req.query.language.trim() });
        }
      }
      if (req.query.search && req.query.search.toString().trim().length >= 3) {
        if (isEmpty) {
          where[db.op.or] = [{ title: { [db.op.like]: `%${req.query.search.toString().trim()}%` } }, { description: { [db.op.like]: `%${req.query.search.toString().trim()}%` } }];
        } else {
          where[db.op.and].push({
            [db.op.or]: [
              { title: { [db.op.like]: `%${req.query.search.toString().trim()}%` } },
              { description: { [db.op.like]: `%${req.query.search.toString().trim()}%` } }
            ]
          });
        }
      }
      const blog = await db.blog.findAll({ where, attributes: ['id', 'title', 'description', 'image', 'video', 'category', 'language'] }, { order: [['id']] });
      blog.forEach((blogData) => {
        if (blogData.dataValues.image) {
          blogData.dataValues.image = `${PUBLIC_URL}/${blogData.dataValues.image}`;
        }
        // if (blogData.dataValues.video) {
        //   blogData.dataValues.video = `${PUBLIC_URL}/${blogData.dataValues.video}`;
        // }
      });
      respGenerator.sendResponse(res, { blog });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async removeBlog(req, res) {
    try {
      const blog = await db.blog.findOne({ where: { id: parseInt(req.params.id) } });
      await db.blog.destroy({ where: { id: req.params.id } });
      if (blog.image && fs.existsSync(`${__dirname}/../public/${blog.image}`)) {
        fs.unlink(`${__dirname}/../public/${blog.image}`);
      }
      // if (blog.video && fs.existsSync(`${__dirname}/../public/${blog.video}`)) {
      //   fs.unlink(`${__dirname}/../public/${blog.video}`);
      // }
      respGenerator.sendResponse(res, { message: 'blog has been removed' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }


  async getCategories(req, res) {
    try {
      const categories = [];
      let allCategories = [];
      const where = {};
      if (req.query.language) {
        where.language = req.query.language.toString().toLowerCase().trim();
      }
      if (req.params.param === 'faq') {
        allCategories = await db.faq.aggregate('category', 'DISTINCT', { plain: false, where, orderBy: [['DISTINCT']] });
      } else if (req.params.param === 'videos') {
        allCategories = await db.educational_videos.aggregate('category', 'DISTINCT', { plain: false, where, orderBy: [['DISTINCT']] });
      } else if (req.params.param === 'blog') {
        allCategories = await db.blog.aggregate('category', 'DISTINCT', { plain: false, where, orderBy: [['DISTINCT']] });
      }
      allCategories.forEach((category) => {
        categories.push(category.DISTINCT);
      });
      respGenerator.sendResponse(res, { categories });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllGameCategories(req, res) {
    try {
      const allGameCategories = await db.game_category.findAll({ attributes: ['id', 'name'], orderBy: [['id']] });
      respGenerator.sendResponse(res, { allGameCategories });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addNewCategory(req, res) {
    try {
      if (!req.body.categoryName) { throw new Error(130); }
      const category = await db.game_category.create({ name: req.body.categoryName });
      respGenerator.sendResponse(res, { category });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllLevels(req, res) {
    try {
      const levelData = await db.game_data.aggregate('level', 'DISTINCT', { plain: false, orderBy: [['DISTINCT']] });
      const allLevels = [];
      if (levelData.length) {
        let lastValue = 1;
        levelData.forEach((levelValue) => {
          allLevels.push(levelValue.DISTINCT);
          lastValue = levelValue.DISTINCT;
        });
        allLevels.push(lastValue + 1);
      } else {
        allLevels.push(1);
      }
      respGenerator.sendResponse(res, { allLevels });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getGameData(req, res) {
    try {
      const gameData = await db.game.findAll({
        where: {},
        include: [
          {
            model: db.game_data,
            include: [
              { model: db.game_category, attributes: ['id', 'name'] },
              { model: db.game_pictures, attributes: ['id', 'picture', 'isMainPicture', 'isCorrect'] }
            ]
          }
        ],
        orderBy: [['id']]
      });
      const mappedGameData = _.sortBy(mappers.game.mapGameData(gameData), 'id');
      const vocabularyData = await db.vocabulary_data.findAll({
        include: [
          { model: db.game_category, attributes: ['id', 'name'] }
        ]
      });
      const vocData = [];
      vocabularyData.forEach((voc) => {
        vocData.push({
          id: voc.id,
          audio: voc.audio,
          instructions: voc.name,
          gamePictures: [{
            id: voc.id,
            picture: voc.picture
          }],
          gameCategory: voc.game_category
        });
      });
      mappedGameData.push({ id: 999, name: 'Vocabulary Building', gameData: vocData });
      respGenerator.sendResponse(res, { gameData: mappedGameData });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async removeVocData(req, res) {
    try {
      const vocabulary_data = await db.vocabulary_data.findOne({ where: { id: req.params.id } });
      if (!vocabulary_data) {
        throw new Error('not found');
      }
      if (vocabulary_data.dataValues.audio && fs.existsSync(`${__dirname}/../public/${vocabulary_data.dataValues.audio}`)) {
        fs.unlink(`${__dirname}/../public/${vocabulary_data.dataValues.audio}`);
      }
      if (vocabulary_data.dataValues.picture && fs.existsSync(`${__dirname}/../public/${vocabulary_data.dataValues.picture}`)) {
        fs.unlink(`${__dirname}/../public/${vocabulary_data.dataValues.picture}`);
      }
      await db.vocabulary_data.destroy({ where: { id: req.params.id } });
      respGenerator.sendResponse(res, { message: 'deleted' });
    } catch (err) {
      console.log(err, 'err');
      respGenerator.sendResponse(res, { message: 'deleted' });
    }
  }

  async removeGameData(req, res) {
    try {
      const gameData = await db.game_data.findOne({
        where: { id: req.params.id },
        include: [
          { model: db.game_pictures, attributes: ['id', 'picture'] }
        ]
      });
      if (gameData && gameData.dataValues) {
        if (gameData.dataValues.audio && fs.existsSync(`${__dirname}/../public/${gameData.dataValues.audio}`)) {
          fs.unlink(`${__dirname}/../public/${gameData.dataValues.audio}`);
        }
        if (gameData.dataValues.game_pictures && gameData.dataValues.game_pictures.length) {
          gameData.dataValues.game_pictures.forEach(async (picture) => {
            if (picture.picture && fs.existsSync(`${__dirname}/../public/${picture.picture}`)) {
              fs.unlink(`${__dirname}/../public/${picture.picture}`);
            }
            await db.game_pictures.destroy({ where: { id: picture.id } });
          });
        }
        db.game_data.destroy({ where: { id: req.params.id } });
      }
      respGenerator.sendResponse(res, { message: 'deleted', gameData });
    } catch (e) {
      console.log(e, 'err');
      respGenerator.sendResponse(res, { message: 'deleted' });
    }
  }

  async addVocabularyData(req, res) {
    try {
      const vocDataBody = {
        categoryId: parseInt(req.body.categoryId),
        audio: '',
        name: req.body.question,
        picture: ''
      };
      let picture;
      let audio;
      for (const file in req.files) {
        if (req.files.hasOwnProperty(file)) {
          if (file.startsWith('images-')) {
            picture = req.files[file];
            if (!req.files[file].mimetype.includes('image')) {
              throw new Error(134);
            }
          } else if (file.startsWith('audio')) {
            audio = req.files[file];
          }
        }
      }
      if (!picture) {
        throw new Error('please provide image');
      }
      if (!audio) {
        throw new Error('please provide audio');
      }
      const audioName = `${GAMES[req.body.gameId]}-${Math.random().toString(36).substring(2)}-${audio.name}`;
      fs.writeFileSync(`${__dirname}/../public/${audioName}`, audio.data);
      vocDataBody.audio = audioName;
      const pictureName = `${GAMES[req.body.gameId]}-${Math.random().toString(36).substring(2)}-${picture.name}`;
      fs.writeFileSync(`${__dirname}/../public/${pictureName}`, picture.data);
      vocDataBody.picture = pictureName;
      const result = await db.vocabulary_data.create(vocDataBody);
      result.dataValues.gamePictures = [{ picture: pictureName, id: result.id }];
      result.dataValues.instructions = req.body.question;
      respGenerator.sendResponse(res, { gameData: result });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addNewGameData(req, res) {
    try {
      if (GAMES[req.body.gameId] === 'VOCABULARY') {
        return new UserController().addVocabularyData(req, res);
      }
      queryHelper.validateGameData(req.body, req.files);
      let audio;
      const gameDataBody = {
        categoryId: parseInt(req.body.categoryId),
        gameId: parseInt(req.body.gameId),
        level: parseInt(req.body.level),
        audio
      };
      if (GAMES[req.body.gameId] === 'GUESS_ME' || GAMES[req.body.gameId] === 'IDENTIFICATION') {
        audio = `${GAMES[req.body.gameId]}-${Math.random().toString(36).substring(2)}-${req.files.audio.name}`;
        fs.writeFileSync(`${__dirname}/../public/${audio}`, req.files.audio.data);
        gameDataBody.audio = audio;
      }
      let audioCount = 0;
      if (GAMES[req.body.gameId] === 'MEMORY' || GAMES[req.body.gameId] === 'MEMORY_AND_SEQUENCING' || GAMES[req.body.gameId] === 'AUDITORY_DISCRIMINATION') {
        gameDataBody.audio = '';
        for (const file in req.files) {
          if (file.startsWith('audios-')) {
            audio = `${GAMES[req.body.gameId]}-${Math.random().toString(36).substring(2)}-${req.files[file].name}`;
            fs.writeFileSync(`${__dirname}/../public/${audio}`, req.files[file].data);
            gameDataBody.audio += `${audio} `;
            audioCount += 1;
          }
        }
        if (!gameDataBody.audio) {
          throw new Error('please provide atleast one audio');
        }
      }
      if (GAMES[req.body.gameId] === 'AUDITORY_DISCRIMINATION') {
        if (audioCount !== req.files.images.length) {
          gameDataBody.audio.split(' ').filter(Boolean).forEach((audioFile) => {
            if (fs.existsSync(`${__dirname}/../public/${audioFile}`)) {
              fs.unlink(`${__dirname}/../public/${audioFile}`);
            }
          });
          throw new Error('total number of pictures should be same as total number of audios');
        }
      }
      if (GAMES[req.body.gameId] === 'WHAT_DO_I_DO' || GAMES[req.body.gameId] === 'WHAT_DO_YOU_USE_FOR' || GAMES[req.body.gameId] === 'IDENTIFICATION') {
        gameDataBody.question = req.body.question;
      }
      let gameData = await db.game_data.create(gameDataBody);
      gameData = gameData.dataValues;
      let images = [];
      req.files.images.forEach((image, index) => {
        const imageName = `${GAMES[req.body.gameId]}-${Math.random().toString(36).substring(2)}-${image.name}`;
        fs.writeFileSync(`${__dirname}/../public/${imageName}`, image.data);
        images.push({
          gameDataId: gameData.id,
          picture: imageName,
          isCorrect: (GAMES[req.body.gameId] === 'IDENTIFICATION' || GAMES[req.body.gameId] === 'WHAT_DO_I_DO' || GAMES[req.body.gameId] === 'GUESS_ME' || GAMES[req.body.gameId] === 'ODD_ONE_OUT') ? req.body.correctImage == index : (GAMES[req.body.gameId] === 'WHAT_DO_YOU_USE_FOR' || GAMES[req.body.gameId] === 'MEMORY_AND_SEQUENCING' || GAMES[req.body.gameId] === 'MEMORY') ? req.body.correctImages.includes(index.toString()) : null,
          isMainPicture: (GAMES[req.body.gameId] === 'PICTURE_MATCHING' || GAMES[req.body.gameId] === 'SHADOW_MATCHING') ? req.body.correctImage == index : null
        });
      });
      images = await db.game_pictures.bulkCreate(images);
      gameData.gamePictures = images;
      respGenerator.sendResponse(res, { gameData });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllScreeningCategories(req, res) {
    try {
      const screeningCategories = await db.screening_category.findAll({}, {
        orderBy: [
          ['id']
        ]
      });
      respGenerator.sendResponse(res, screeningCategories);
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async editScreeningCategories(req, res) {
    try {
      const category = {
        name: req.body.name,
        description: req.body.description,
        youtube_link: req.body.youtube_link,
        youtube_link_list_success: req.body.youtube_link_list_success,
        youtube_link_list_error: req.body.youtube_link_list_error,
        bg_color: req.body.bg_color
      };
      if (req.files && req.files.image) {
        const pictureName = `screening_category-${Math.random().toString(36).substring(2)}-${req.params.id}-${req.files.image.name}`;
        fs.writeFileSync(`${__dirname}/../public/${pictureName}`, req.files.image.data);
        category.image = pictureName;
      }
      for (const field in category) {
        if (!category[field]) { throw new Error(`please profile value for - ${field}`); }
      }
      await db.screening_category.update(category, { where: { id: req.params.id } });
      respGenerator.sendResponse(res, { message: 'category has been updated' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async editScreeningDecidingValue(req, res) {
    try {
      const values = { decidingValue: (req.body.decidingValue.toString() == 'true') };
      if (req.body.decidingValue2 !== null && req.body.decidingValue2 !== undefined) {
        values.decidingValue2 = (req.body.decidingValue2.toString() == 'true');
      }
      await db.screening_category.update(values, { where: { id: req.params.id } });
      respGenerator.sendResponse(res, { message: 'category has been updated' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addArticulationPicture(req, res) {
    try {
      if (!req.files.articulationPicture || !req.files.articulationPicture.mimetype.includes('image')) { throw new Error(119); }
      if (!req.files.articulationAudio || !req.files.articulationAudio.mimetype.includes('audio')) { throw new Error(120); }
      if (!req.body.correct_word) { throw new Error(121); }
      if (!req.body.target_sound) { throw new Error(161); }
      const pictureName = `${Math.random().toString(36).substring(2)}-${req.files.articulationPicture.name}`;
      const audioName = `${Math.random().toString(36).substring(2)}-${req.files.articulationAudio.name}`;
      fs.writeFileSync(`${__dirname}/../public/${pictureName}`, req.files.articulationPicture.data);
      fs.writeFileSync(`${__dirname}/../public/${audioName}`, req.files.articulationAudio.data);
      const picture = await db.atriculation_picture.create({
        screening_id: null,
        picture_url: pictureName,
        audio_url: audioName,
        correct_word: req.body.correct_word,
        target_sound: req.body.target_sound
      });
      respGenerator.sendResponse(res, picture.dataValues);
    } catch (err) {
      // console.log(req.body, err);
      respGenerator.sendError(res, err);
    }
  }

  async editArticulationPicture(req, res) {
    try {
      if (!req.body.correct_word) { throw new Error(121); }
      if (!req.body.target_sound) { throw new Error(161); }
      const picture = await db.atriculation_picture.findOne({ where: { id: parseInt(req.params.id) } });
      if (!picture) {
        throw new Error(119);
      }
      let picture_url = picture.picture_url;
      let audio_url = picture.audio_url;
      if (req.files.articulationPicture) {
        picture_url = `${Math.random().toString(36).substring(2)}-${req.files.articulationPicture.name}`;
        fs.writeFileSync(`${__dirname}/../public/${picture_url}`, req.files.articulationPicture.data);
      }
      if (req.files.articulationAudio) {
        audio_url = `${Math.random().toString(36).substring(2)}-${req.files.articulationAudio.name}`;
        fs.writeFileSync(`${__dirname}/../public/${audio_url}`, req.files.articulationAudio.data);
      }
      await db.atriculation_picture.update({
        correct_word: req.body.correct_word,
        target_sound: req.body.target_sound,
        picture_url,
        audio_url
      }, { where: { id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { picture_url, audio_url });
    } catch (err) {
      // console.log(req.body, err);
      respGenerator.sendError(res, err);
    }
  }

  async getAllArticulationPictures(req, res) {
    try {
      const pictures = await db.atriculation_picture.findAll({});
      respGenerator.sendResponse(res, { pictures });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllSpeechLanDomains(req, res) {
    try {
      const result = [];
      let domains = await db.speech_language_domain.findAll({
        where: {},
        include: [{ model: db.speech_language_screening }]
      }, { order: [['id']] });
      domains = JSON.parse(JSON.stringify(domains));
      domains.forEach((domain) => {
        result.push({
          id: domain.id,
          name: domain.name,
          screening_id: domain.screening_id,
          created_at: domain.created_at,
          speech_language_screenings: []
        });
        domain.speech_language_screenings.forEach((question) => {
          if (!question.isDeleted) {
            result[result.length - 1].speech_language_screenings.push(question);
          }
        });
      });
      respGenerator.sendResponse(res, { domains: result });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllHearingScreeningTypes(req, res) {
    try {
      let hearingScreeningTypes = await db.hearing_screening_type.findAll({
        where: {},
        include: [{ model: db.hearing_screening }]
      }, { order: [['id']] });
      const result = [];
      hearingScreeningTypes = JSON.parse(JSON.stringify(hearingScreeningTypes));
      hearingScreeningTypes.forEach((hearingScreeningType) => {
        result.push({ id: hearingScreeningType.id, name: hearingScreeningType.name, hearing_screenings: [] });
        hearingScreeningType.hearing_screenings.forEach((question) => {
          if (!question.isDeleted) {
            result[result.length - 1].hearing_screenings.push(question);
          }
        });
      });
      respGenerator.sendResponse(res, { hearingScreeningTypes: result });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllCommonScreeningQuestions(req, res) {
    try {
      const questions = await db.common_screening.findAll({ where: { screening_id: req.params.id, isDeleted: false } }, { orderBy: [['id']] });
      respGenerator.sendResponse(res, { questions });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addCommonScreeningQuestion(req, res) {
    try {
      queryHelper.validateCommonScreeningQuestion(req.body);
      const question = await db.common_screening.create(req.body);
      respGenerator.sendResponse(res, { question });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async editCommonScreeningQuestion(req, res) {
    try {
      if (!req.body.question) {
        throw new Error(124);
      }
      await db.common_screening.update({ question: req.body.question }, { where: { id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { message: 'updated' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async deleteCommonScreeningQuestion(req, res) {
    try {
      req.params.categoryId = parseInt(req.params.categoryId);
      req.params.id = parseInt(req.params.id);
      if (![2, 3, 4, 5, 6].includes(req.params.categoryId)) { throw new Error(152); }
      // const answers = await db.screening_answer.findOne({ where: { screeningId: req.params.categoryId, questionId: req.params.id } });
      // if (answers || answers.length) { throw new Error(158); }
      if (req.params.categoryId == 2) {
        await db.speech_language_screening.update({ isDeleted: true }, { where: { id: req.params.id } });
      } else if (req.params.categoryId == 3) {
        await db.hearing_screening.update({ isDeleted: true }, { where: { id: req.params.id } });
      } else {
        await db.common_screening.update({ isDeleted: true }, { where: { id: req.params.id } });
      }
      respGenerator.sendResponse(res, { message: 'deleted' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addSpeechLanQuestion(req, res) {
    try {
      queryHelper.validateSpeechLanQuestion(req.body);
      const question = await db.speech_language_screening.create({
        question: req.body.question,
        domain_id: req.body.domain_id,
        age_start: req.body.age_start,
        age_end: req.body.age_end
      });
      respGenerator.sendResponse(res, { question });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async editSpeechLanQuestion(req, res) {
    try {
      queryHelper.validateEditSpeechLanQuestion(req.body);
      await db.speech_language_screening.update({
        question: req.body.question,
        age_start: req.body.age_start,
        age_end: req.body.age_end,
        domain_id: req.body.domain_id
      }, { where: { id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { message: 'updated' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addHearingQuestion(req, res) {
    try {
      queryHelper.validateHearingQuestion(req.body);
      const question = await db.hearing_screening.create({
        question: req.body.question,
        domain_id: req.body.domain_id,
        screening_id: CATEGORY_IDS.HEARING,
        screening_type: req.body.screening_type
      });
      respGenerator.sendResponse(res, { question });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async editHearingQuestion(req, res) {
    try {
      if (!req.body.question) {
        throw new Error(124);
      }
      await db.hearing_screening.update({ question: req.body.question }, { where: { id: parseInt(req.params.id) } });
      respGenerator.sendResponse(res, { message: 'updated' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async removeStory(req, res) {
    try {
      const story = await db.story.findOne({ where: { id: req.params.id } });
      if (!story) { throw new Error(142); }
      
      if (story.picture && fs.existsSync(`${__dirname}/../public/${story.picture}`)) {
        fs.unlink(`${__dirname}/../public/${story.picture}`);
      }
      if (story.audio && fs.existsSync(`${__dirname}/../public/${story.audio}`)) {
        fs.unlink(`${__dirname}/../public/${story.audio}`);
      }
      
      await db.story.destroy({ where: { id: req.params.id } });
      
      return respGenerator.sendResponse(res, { message: 'story deleted' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  
  async addStory(req, res) {
    try {
      let picture = '';
      let audio = '';
      if (req.files && req.files.picture) {
        picture = `${Math.random().toString(36).substring(2)}-${req.files.picture.name}`;
        fs.writeFileSync(`${__dirname}/../public/${picture}`, req.files.picture.data);
      }
      if (req.files && req.files.audio) {
        audio = `${Math.random().toString(36).substring(2)}-${req.files.audio.name}`;
        fs.writeFileSync(`${__dirname}/../public/${audio}`, req.files.audio.data);
      }
      const data = await db.story.create({
        title: req.body.title,
        content: req.body.content,
        targetSoundId: req.body.targetSoundId,
        audio,
        picture,
      });
      data.dataValues.picture = `${PUBLIC_URL}/${data.dataValues.picture}`;
      data.dataValues.audio = `${PUBLIC_URL}/${data.dataValues.audio}`;
      return respGenerator.sendResponse(res, data);
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async editStory(req, res) {
    try {
      const story = await db.story.findOne({ where: { id: req.params.id } });
      if (!story) { throw new Error(142); }
      
      const data = {
        title: req.body.title,
        content: req.body.content,
        targetSoundId: req.body.targetSoundId,
      };
      
      if (story.picture && fs.existsSync(`${__dirname}/../public/${story.picture}`) && req.files && req.files.picture) {
        fs.unlink(`${__dirname}/../public/${story.picture}`);
      }
      if (story.audio && fs.existsSync(`${__dirname}/../public/${story.audio}`) && req.files && req.files.audio) {
        fs.unlink(`${__dirname}/../public/${story.audio}`);
      }

      if (req.files && req.files.picture) {
        data.picture = `${Math.random().toString(36).substring(2)}-${req.files.picture.name}`;
        fs.writeFileSync(`${__dirname}/../public/${data.picture}`, req.files.picture.data);
      }
      if (req.files && req.files.audio) {
        data.audio = `${Math.random().toString(36).substring(2)}-${req.files.audio.name}`;
        fs.writeFileSync(`${__dirname}/../public/${data.audio}`, req.files.audio.data);
      }
      await db.story.update(data, { where: { id: parseInt(req.params.id) } });
      data.picture = `${PUBLIC_URL}/${data.picture}`;
      data.audio = `${PUBLIC_URL}/${data.audio}`;
      return respGenerator.sendResponse(res, data);
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
}

module.exports = new UserController();
