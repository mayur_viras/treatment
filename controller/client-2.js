/* eslint-disable brace-style, max-statements-per-line, no-sync */
const respGenerator = require('../response/json-response');
const db = require('../models');
const moment = require('moment');
const { PUBLIC_URL } = require('../response/constants');

class UserController {
  async getProfessionals(req, res) {
    try {
      let query = 'select client_users.id, client_users.name, professionals.address, client_users.profile from client_users, professionals where client_users.id = professionals.client_id ';
      if (req.body.designation_name) {
        query += ` and professionals.designation_name like '%${req.body.designation_name}%' `;
      }
      if (req.body.country_id && !isNaN(req.body.country_id)) {
        query += ` and client_users.country_id=${parseInt(req.body.country_id)} `;
      }
      if (req.body.state_id && !isNaN(req.body.state_id)) {
        query += ` and client_users.state_id=${parseInt(req.body.state_id)} `;
      }
      if (req.body.city_id && !isNaN(req.body.city_id)) {
        query += ` and professionals.city_id=${parseInt(req.body.city_id)} `;
      }
      if (req.body.district_id && !isNaN(req.body.district_id)) {
        query += ` and professionals.city_id=${parseInt(req.body.district_id)}`;
      }
      const professionals = await db.executeQuery(query);
      professionals.forEach((prof) => {
        if (prof.profile) {
          prof.profile = `${PUBLIC_URL}/${prof.profile}`;
        }
      });
      respGenerator.sendResponse(res, { professionals });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getProfessionalById(req, res) {
    try {
      const query = `select * from client_users, professionals where client_users.id = professionals.client_id and client_users.id=${parseInt(req.params.id)}`;
      const professional = await db.executeQuery(query);
      if (!professional.length) {
        throw new Error('professional id is invalid');
      }
      if (professional[0].profile) {
        professional[0].profile = `${PUBLIC_URL}/${professional[0].profile}`;
      }
      respGenerator.sendResponse(res, { professional: professional[0] });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async bookAppointment(req, res) {
    try {
      if (req.user.user_type == 2) {
        throw new Error('only parent/general user and student can book appointments');
      }
      const professional = await db.professional.findOne({ where: { client_id: req.body.professional_id }, attributes: ['client_id'] });
      if (!professional) {
        throw new Error('professional does not exist');
      }
      if (!moment(req.body.date, 'YYYY-MM-DD', true).isValid()) {
        throw new Error('date should be in YYYY-MM-DD format');
      }
      if (!req.body.appointment_type) {
        throw new Error('please provide appointment type');
      }
      if (!req.body.time_slot_from) {
        throw new Error('please provide time-slot - from');
      }
      if (!req.body.time_slot_to) {
        throw new Error('please provide time-slot - to');
      }
      req.body.date_str = moment(req.body.date, 'YYYY-MM-DD');
      req.body.user_id = req.user.id;
      const appointment = await db.appointment.create(req.body);
      respGenerator.sendResponse(res, { appointment });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllBookedTimeSlot(req, res) {
    try {
      const timeSlots = await db.appointment.findAll({
        where: {
          professional_id: req.body.professional_id,
          appointment_type: req.body.appointment_type,
          date: req.body.date
        },
        attributes: ['time_slot_from', 'time_slot_to']
      });
      respGenerator.sendResponse(res, { timeSlots });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
}
module.exports = new UserController();
