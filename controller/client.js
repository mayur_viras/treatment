/* eslint-disable brace-style, max-statements-per-line, no-sync */
const jwt = require('jsonwebtoken');
const { jwtConfig } = require('../config');
const respGenerator = require('../response/json-response');
const db = require('../models');
const mappers = require('../models/mapper/index');
const queryHelper = require('../lib/query');
const queryHelper2 = require('../lib/query-2');
const { CATEGORY_IDS, answerResponse, PUBLIC_URL, SERVER_URL } = require('../response/constants');
const _ = require('lodash');
const fs = require('fs');
const mailer = require('../helpers/mailer');

class UserController {

  async getStoryContent(req, res) {
    try {
        const story = await db.story.findOne({
          where: {
            id: req.params.story_id
          }
        });
        if (!story) {
          throw new Error('story does not exists')
        }
        if (story.dataValues.audio) {
          story.dataValues.audio = `${PUBLIC_URL}/${story.dataValues.audio}`;
        }
        if (story.dataValues.picture) {
          story.dataValues.picture = `${PUBLIC_URL}/${story.dataValues.picture}`;
        }
        return respGenerator.sendResponse(res, { story }, 'data found');
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getStoriesList(req, res) {
    try {
        const where = {};
        if (req.query.target_sound_id) {
          where.targetSoundId = parseInt(req.query.target_sound_id);
        }
        const stories = await db.story.findAll({
          where,
          orderBy: [['id', 'desc']],
          attributes: ['title', 'id']
        })
        return respGenerator.sendResponse(res, { stories }, 'data found');
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addStory(req, res) {
    try {
      if (!req.body.title || !req.body.content || !req.files.audio || !req.files.audio.mimetype.includes('audio') || !req.body.targetSoundId) {
        return respGenerator.sendError(res, new Error('please provide valid data'));
      }
      let audio = `story-audio-${Math.random().toString(36).substring(2)}-${req.files.audio.name}`;
      fs.writeFileSync(`${__dirname}/../public/${audio}`, req.files.audio.data);
      const story = await db.story.create({
        title: req.body.title,
        content: req.body.content,
        audio,
        targetSoundId: parseInt(req.body.targetSoundId)
      });
      story.dataValues.audio = `${PUBLIC_URL}/${story.dataValues.audio}`;
      return respGenerator.sendResponse(res, { story }, 'data found');
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getTargetSound(req, res) {
    try {
        const target_sounds = await db.story_target_sound.findAll({
          orderBy: [['id', 'desc']],
          attributes: ['name', 'id']
        })
        return respGenerator.sendResponse(res, { target_sounds }, 'data found');
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addTargetSound(req, res) {
    try {
      if (req.body.name) {
        const data = await db.story_target_sound.create({
          name: req.body.name
        })
        return respGenerator.sendResponse(res, { data }, 'data found');
      }
      return respGenerator.sendResponse(res, { message: 'invalida data' }, 'data found');      
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addWord(req, res) {
    try {
      if (!req.body.name || !req.body.categoryId || !req.files || !req.files.picture || !req.files.picture.mimetype.includes('image')) {
        return respGenerator.sendError(res, new Error('please provide valid data'));
      }
      let picture = `word-picture-${Math.random().toString(36).substring(2)}-${req.files.picture.name}`;
      fs.writeFileSync(`${__dirname}/../public/${picture}`, req.files.picture.data);

      if (req.body.name && req.body.categoryId) {
        const data = await db.word.create({
          name: req.body.name,
          categoryId: req.body.categoryId,
          picture
        });
        data.dataValues.picture = `${PUBLIC_URL}/${data.dataValues.picture}`;
        return respGenerator.sendResponse(res, { data }, 'data found');
      }
      return respGenerator.sendResponse(res, { message: 'invalida data' }, 'data found');      
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addPhraseSentence(req, res) {
    try {
      let model = 'word';
      if (req.url.includes('phrase')) {
        model = 'phrase';
      } else if (req.url.includes('sentence')) {
        model = 'sentence';
      }
      if (req.body.name && req.body.categoryId) {
        const data = await db[model].create({
          name: req.body.name,
          categoryId: req.body.categoryId
        })
        return respGenerator.sendResponse(res, { data }, 'data found');
      }
      return respGenerator.sendResponse(res, { message: 'invalida data' }, 'data found');      
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getWordPhraseSentenceCategories(req, res) {
    try {
      let model = 'word_category';
      if (req.url.includes('getPhraseCategories')) {
        model = 'phrase_category';
      } else if (req.url.includes('getSentenceCategories')) {
        model = 'sentence_category';
      }
      const categories = await db[model].findAll({ orderBy: [['id', 'desc']], attributes: ['id', 'name'] });
      respGenerator.sendResponse(res, { categories }, 'data found');
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getWords(req, res) {
    try {
      if (!req.query.category_id) {
        throw new Error('please provide categoryID')
      }
      const where = {};
      if (req.query.category_id) {
        where.categoryId = parseInt(req.query.category_id);
      }
      const data = await db.word.findAll({ where, orderBy: [['id', 'desc']], attributes: ['id', 'name', 'picture'] });
      data.forEach(d => {
        d.dataValues.picture = `${PUBLIC_URL}/${d.dataValues.picture}`
      });
      respGenerator.sendResponse(res, { words: data }, 'data found');
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getPhraseSentenceData(req, res) {
    try {
      if (!req.query.category_id) {
        throw new Error('please provide categoryID')
      }
      let model = 'word';
      let keyword = 'words';
      if (req.url.includes('getPhrases')) {
        model = 'phrase';
        keyword = 'phrases';
      } else if (req.url.includes('getSentences')) {
        model = 'sentence';
        keyword = 'sentences';
      }
      const where = {};
      if (req.query.category_id) {
        where.categoryId = parseInt(req.query.category_id);
      }
      const data = await db[model].findAll({ where, orderBy: [['id', 'desc']], attributes: ['id', 'name'] });
      respGenerator.sendResponse(res, { [keyword]: data }, 'data found');
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async editProfile(req, res) {
    try {
      const userData = await db.client_user.findOne({ where: { id: req.user.id } });
      if (!userData) {
        throw new Error('please provide valid token in header');
      }
      if (userData && userData.profile && req.files && req.files.profile) {
        fs.unlink(`${__dirname}/../public/${userData.profile}`);
      }
      const body = await queryHelper.validateEditProfile(req);
      body.profile = '';
      if (req.files && req.files.profile) {
        body.profile = `profile-${Math.random().toString(36).substring(2)}-${req.files.profile.name}`;
        fs.writeFileSync(`${__dirname}/../public/${body.profile}`, req.files.profile.data);
      }
      if (userData.user_type == 2) {
        const professionalBody = await queryHelper2.validateNewProfessional(req.body);
        await db.professional.update(professionalBody, { where: { client_id: userData.id } });
        for (const field in professionalBody) {
          userData.dataValues[field] = professionalBody[field];
        }
      } else if (userData.user_type == 3) {
        const studentBody = await queryHelper2.validateNewStudent(req.body);
        if (req.files && req.files.identity_proof) {
          studentBody.identity_proof = `identity_proof-${Math.random().toString(36).substring(2)}-${req.files.identity_proof.name}`;
          fs.writeFileSync(`${__dirname}/../public/${studentBody.identity_proof}`, req.files.identity_proof.data);
        }
        await db.student.update(studentBody, { where: { client_id: userData.id } });
        for (const field in studentBody) {
          userData.dataValues[field] = studentBody[field];
        }
      }
      await db.client_user.update(body, { where: { id: req.user.id } });
      for (const variable in body) {
        if (body[variable]) {
          userData.dataValues[variable] = body[variable];
        }
      }
      if (userData.dataValues.profile) {
        userData.dataValues.profile = `${PUBLIC_URL}/${userData.dataValues.profile}`;
      }
      if (userData.dataValues.identity_proof) {
        userData.dataValues.identity_proof = `${PUBLIC_URL}/${userData.dataValues.identity_proof}`;
      }
      respGenerator.sendResponse(res, { userData });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async registerNewUser(req, res) {
    try {
      const body = await queryHelper.validateNewUser(req.body);
      body.profile = '';
      if (req.files && req.files.profile) {
        body.profile = `profile-${Math.random().toString(36).substring(2)}-${req.files.profile.name}`;
        fs.writeFileSync(`${__dirname}/../public/${body.profile}`, req.files.profile.data);
      }
      const userData = await db.client_user.create(body);
      const data = { id: userData.id, user_type: body.user_type, type: 'user', email: body.email };
      const token = jwt.sign(data, jwtConfig.secret);
      if (userData.dataValues.profile) {
        userData.dataValues.profile = `${PUBLIC_URL}/${userData.dataValues.profile}`;
      }
      respGenerator.sendResponse(res, { token, userData });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllProfessionalAddOns(req, res) {
    try {
      const data = {
        qualification: await db.qualification.findAll({ where: { isDeleted: false }, attributes: ['id', 'name'] }),
        designation: await db.designation.findAll({ where: { isDeleted: false }, attributes: ['id', 'name'] }),
        working_at: await db.working_at.findAll({ where: { isDeleted: false }, attributes: ['id', 'name'] }),
        identity_proof: await db.identity_proof.findAll({ where: { isDeleted: false }, attributes: ['id', 'name'] }),
        consultation_currency: await db.consultation_currency.findAll({ where: { isDeleted: false }, attributes: ['id', 'name'] }),
        student_languages: await db.student_language.findAll({ where: { isDeleted: false }, attributes: ['id', 'name'] })
      };
      respGenerator.sendResponse(res, { data });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async registerNewProfessional(req, res) {
    try {
      const body = await queryHelper.validateNewUser(req.body);
      const professionalBody = await queryHelper2.validateNewProfessional(req.body);
      body.profile = '';
      if (req.files && req.files.profile) {
        body.profile = `profile-${Math.random().toString(36).substring(2)}-${req.files.profile.name}`;
        fs.writeFileSync(`${__dirname}/../public/${body.profile}`, req.files.profile.data);
      }
      body.user_type = 2;
      const userData = await db.client_user.create(body);
      professionalBody.client_id = userData.id;
      const professionalData = await db.professional.create(professionalBody);
      const data = { id: userData.id, user_type: body.user_type, type: 'user', email: body.email };
      const token = jwt.sign(data, jwtConfig.secret);
      if (userData.dataValues.profile) {
        userData.dataValues.profile = `${PUBLIC_URL}/${userData.dataValues.profile}`;
      }
      for (const field in professionalData.dataValues) {
        userData.dataValues[field] = professionalData.dataValues[field];
      }
      respGenerator.sendResponse(res, { token, userData });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async registerNewStudent(req, res) {
    try {
      const body = await queryHelper.validateNewUser(req.body);
      const studentBody = await queryHelper2.validateNewStudent(req.body);
      body.profile = '';
      if (req.files && req.files.profile) {
        body.profile = `profile-${Math.random().toString(36).substring(2)}-${req.files.profile.name}`;
        fs.writeFileSync(`${__dirname}/../public/${body.profile}`, req.files.profile.data);
      }
      if (req.files && req.files.identity_proof) {
        studentBody.identity_proof = `identity_proof-${Math.random().toString(36).substring(2)}-${req.files.identity_proof.name}`;
        fs.writeFileSync(`${__dirname}/../public/${studentBody.identity_proof}`, req.files.identity_proof.data);
      }
      body.user_type = 3;
      const userData = await db.client_user.create(body);
      studentBody.client_id = userData.id;
      const studentData = await db.student.create(studentBody);
      const data = { id: userData.id, user_type: body.user_type, type: 'user', email: body.email };
      const token = jwt.sign(data, jwtConfig.secret);
      if (userData.dataValues.profile) {
        userData.dataValues.profile = `${PUBLIC_URL}/${userData.dataValues.profile}`;
      }
      for (const field in studentData.dataValues) {
        userData.dataValues[field] = studentData.dataValues[field];
      }
      if (userData.dataValues.identity_proof) {
        userData.dataValues.identity_proof = `${PUBLIC_URL}/${userData.dataValues.identity_proof}`;
      }
      respGenerator.sendResponse(res, { token, userData });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getMyProfile(req, res) {
    try {
      const userData = await db.client_user.findOne({ where: { id: req.user.id } });
      userData.dataValues.profile = `${PUBLIC_URL}/${userData.dataValues.profile}`;
      respGenerator.sendResponse(res, { userData });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async changePassword(req, res) {
    try {
      const userData = await db.client_user.findOne({ where: { id: req.user.id } });
      if (userData.password !== req.body.old_password) {
        throw new Error('old password does not match with actual password');
      }
      if (!req.body.new_password) {
        throw new Error('please provide new password');
      }
      await db.client_user.update({ password: req.body.new_password }, { where: { id: req.user.id } });
      respGenerator.sendResponse(res, { message: 'password has been changed' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async forgotPassword(req, res) {
    try {
      console.log(req.body, 'forgot pass');
      if (!req.body.email) {
        throw new Error('please provide email');
      }
      const userData = await db.client_user.findOne({ where: { email: req.body.email.toString().toLowerCase().trim() }, attributes: ['id', 'email'] });
      if (!userData) {
        throw new Error('your email does not exist in our records');
      }
      const passwordCode = (100000 + Math.floor(Math.random() * 900000)).toString();
      await db.client_user.update({ passwordCode }, { where: { id: userData.dataValues.id } });
      mailer.sendEmail(userData.email, 'Password reset instructions', `code to reset your password is <b>${passwordCode}</b>`);
      respGenerator.sendResponse(res, { message: 'password generation code has been sent to your email' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async resetPassword(req, res) {
    try {
      console.log(req.body, 'reset pass');
      if (!req.body.password_code) {
        throw new Error('please provide password code');
      }
      if (!req.body.new_password) {
        throw new Error('please provide new password');
      }
      const userData = await db.client_user.findOne({ where: { passwordCode: req.body.password_code }, attributes: ['id', 'email'] });
      if (!userData) {
        throw new Error('user with given password code does not exist in our records');
      }
      await db.client_user.update({ password: req.body.new_password, passwordCode: '' }, { where: { id: userData.dataValues.id } });
      respGenerator.sendResponse(res, { message: 'your password has been updated' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async login(req, res) {
    try {
      if (!req.body.email) {
        throw new Error('please provide email');
      }
      const userData = await db.client_user.findOne({
        where: { email: req.body.email.toLowerCase() },
        include: [
          { model: db.student, attributes: { exclude: ['created_at', 'updated_at', 'id', 'client_id'] } },
          { model: db.professional, attributes: { exclude: ['created_at', 'updated_at', 'id', 'client_id'] } }
        ],
        attributes: { exclude: ['created_at', 'updated_at'] }
      });
      if (!userData) {
        console.log(userData.dataValues, req.body);
        throw new Error('login info is invalid');
      }
      if (req.body.password !== userData.password.toString()) {
       throw new Error('either username or password is incorrect');
      }
      const data = { id: userData.id, user_type: userData.user_type, type: 'user', email: userData.email };
      const token = jwt.sign(data, jwtConfig.secret);
      if (userData.dataValues.profile) {
        userData.dataValues.profile = `${PUBLIC_URL}/${userData.dataValues.profile}`;
      }
      if (userData.students && userData.students.length) {
        userData.students = userData.students[0];
        for (const field in userData.students.dataValues) {
          userData.dataValues[field] = userData.students.dataValues[field];
        }
        userData.dataValues.user = 'student';
      } else if (userData.professionals && userData.professionals.length) {
        userData.professionals = userData.professionals[0];
        for (const field in userData.professionals.dataValues) {
          userData.dataValues[field] = userData.professionals.dataValues[field];
        }
        userData.dataValues.user = 'professional';
      } else {
        userData.user = 'parent/general user';
      }
      delete userData.dataValues.students;
      delete userData.dataValues.professionals;
      respGenerator.sendResponse(res, { token, userData });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async register(req, res) {
    try {
      if (!req.body.deviceId) {
        throw new Error('143');
      }
      const device = await db.device.upsert({ device_id: req.body.deviceId.toString() });
      const data = { deviceId: req.body.deviceId.toString(), type: 'device', device_id: device.id };
      const token = jwt.sign(data, jwtConfig.secret);
      respGenerator.sendResponse(res, { token });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getLanguages(req, res) {
    const languages = ['English', 'Hindi', 'Kannada'];
    let query = 'select DISTINCT language from ';
    if (req.params.param === 'faq') {
      query += 'faqs ';
      // allLanguages = await db.faq.aggregate('language', 'DISTINCT', { plain: false, orderBy: [['DISTINCT']] });
    } else if (req.params.param === 'videos' || req.params.param === 'video') {
      query += 'educational_videos ';
      // allLanguages = await db.educational_videos.aggregate('language', 'DISTINCT', { plain: false, orderBy: [['DISTINCT']] });
    } else if (req.params.param === 'blog') {
      query += 'blogs ';
      // allLanguages = await db.blog.aggregate('language', 'DISTINCT', { plain: false, orderBy: [['DISTINCT']] });
    } else {
      throw new Error('please provide valid parameter');
    }
    query += 'where "isDeleted" = false';
    const allLanguages = await db.executeQuery(query);
    allLanguages.forEach((language) => {
      console.log(language.language, query);
      if (!languages.includes(language.language.toString())) {
        languages.push(language.language.toString());
      }
    });
    console.log(languages);
    respGenerator.sendResponse(res, { languages });
  }

  async addScreeningUser(req, res) {
    try {
      const device = await db.device.findOne({ where: { device_id: req.user.deviceId } });
      if (!device) {
        throw new Error(147);
      }
      req.body.device_id = device.id;
      queryHelper.validateScreeningUser(req.body);
      const screeningUser = await db.screening_user.create(req.body);
      respGenerator.sendResponse(res, { screeningUser });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllScreeningUsers(req, res) {
    try {
      const device = await db.device.findOne({ where: { device_id: req.user.deviceId } });
      if (!device) {
        throw new Error(147);
      }
      const screeningUsers = await db.screening_user.findAll({ where: { device_id: device.id }, attributes: ['id', 'name', 'age_year', 'age_month'] });
      respGenerator.sendResponse(res, { screeningUsers });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllGames(req, res) {
    try {
      const allGames = await db.game.findAll({ orderBy: [['id']], attributes: ['id', 'name'] });
      respGenerator.sendResponse(res, { allGames });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllFeedback(req, res) {
    try {
      const feedback = await db.feedback.findAll({ orderBy: [['id']], where: { isVisibleToAdminOnly: false }, attributes: ['id', 'title', 'user'] });
      respGenerator.sendResponse(res, { feedback });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addFeedback(req, res) {
    try {
      if (!req.body.title) {
        throw new Error('please provide feedback title');
      }
      if (req.body.isVisibleToAdminOnly === undefined || req.body.isVisibleToAdminOnly === null) {
        throw new Error('please provide is-visible-to-admin flag title');
      }
      const feedback = await db.feedback.create({
        title: req.body.title,
        isVisibleToAdminOnly: Boolean(req.body.isVisibleToAdminOnly),
        user: req.body.user,
        userId: req.user.id
      });
      respGenerator.sendResponse(res, { feedback });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllGameCategories(req, res) {
    try {
      const allGameCategories = await db.game_category.findAll({ attributes: ['id', 'name'], orderBy: [['id']] });
      respGenerator.sendResponse(res, { allGameCategories });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getGameData(req, res) {
    try {
      const gameData = await db.game.findAll({
        where: {},
        include: [
          {
            model: db.game_data,
            include: [
              { model: db.game_category, attributes: ['id', 'name'] },
              { model: db.game_pictures, attributes: ['id', 'picture', 'isMainPicture', 'isCorrect'] }
            ]
          }
        ],
        orderBy: [['id']]
      });
      respGenerator.sendResponse(res, { gameData: mappers.game.mapGameData(gameData) });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getVocabularyItems(req, res) {
    try {
      const where = {};
      if (req.body.category_id) {
        where.categoryId = parseInt(req.body.category_id);
      }
      if (req.body.is_practice_with_all !== undefined && req.body.is_practice_with_all === false) {
        where.status = {
          [db.op.notLike]: 'DEFAULT'
        }
      }
      const vocabularyItems = await db.vocabulary_item.findAll({ where, attributes: ['id', 'name', 'status', 'remark', 'categoryId'], orderBy: [['id']] });
      respGenerator.sendResponse(res, { vocabularyItems });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async updateVocabularyItem(req, res) {
    try {
      if (!req.body.id) {
        throw new Error('please provide item ID');
      }
      const where = { id: parseInt(req.body.id) };
      await db.vocabulary_item.update({
        status: req.body.status,
        remark: req.body.remark
      }, { where });
      respGenerator.sendResponse(res, { message: "Item updated" });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getVocabularyData(req, res) {
    try {
      const where = {};
      if (req.body.category_id) {
        where.categoryId = parseInt(req.body.category_id);
      }
      const vocabularyData = await db.vocabulary_data.findAll({ where, attributes: ['id', 'name', 'audio', 'picture'], orderBy: [['id']] });
      vocabularyData.forEach((voc) => {
        voc.dataValues.audio = `${PUBLIC_URL}/${voc.dataValues.audio}`;
        voc.dataValues.picture = `${PUBLIC_URL}/${voc.dataValues.picture}`;
      });
      respGenerator.sendResponse(res, { vocabularyData });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getGameDataByGameId(req, res) {
    try {
      const gameDataWhere = {};
      const gameCatWhere = {};
      if (req.body.level) {
        gameDataWhere.level = req.body.level;
      }
      if (req.body.category_id) {
        gameDataWhere.categoryId = req.body.category_id;
      }
      const gameData = await db.game.findAll({
        where: { id: req.body.game_id },
        include: [
          {
            model: db.game_data,
            where: gameDataWhere,
            include: [
              {
                model: db.game_category,
                attributes: ['id', 'name'],
                where: gameCatWhere
              },
              { model: db.game_pictures, attributes: ['id', 'picture', 'isMainPicture', 'isCorrect'] }
            ]
          }
        ],
        orderBy: [['id']]
      });
      if (gameData.length) {
        const game = mappers.game.mapGameData(gameData)[0];
        game.prefixUrl = 'http://13.127.40.47:8080/public';
        game.message = 'please add prefixUrl/audio-url or prefixUrl/picture-url, to get full audio/picture of any game';
        return respGenerator.sendResponse(res, { game });
      }
      return respGenerator.sendResponse(res, { game: [] });
    } catch (err) {
      return respGenerator.sendError(res, err);
    }
  }

  async getAllScreeningCategories(req, res) {
    try {
      const screeningCategories = await db.screening_category.findAll({
        order: [['id']],
        attributes: ['id', 'name', 'type', 'youtube_link', 'description', 'image', 'bg_color']
      });
      for (let i = 0; i < screeningCategories.length; i += 1) {
        screeningCategories[i].dataValues.image = `${PUBLIC_URL}/${screeningCategories[i].dataValues.image}`;
      }
      respGenerator.sendResponse(res, { screening_categories: screeningCategories });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllArticulationPictures(req, res) {
    try {
      const pictures = await db.atriculation_picture.findAll({ attributes: ['id', 'picture_url', 'audio_url', 'correct_word', 'target_sound'], orderBy: [['id']], order: [['id']] });
      pictures.forEach((picture) => {
        picture.dataValues.picture_url = `${PUBLIC_URL}/${picture.dataValues.picture_url}`;
        picture.dataValues.audio_url = `${PUBLIC_URL}/${picture.dataValues.audio_url}`;
      });
      respGenerator.sendResponse(res, { pictures });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllSpeechLanDomains(req, res) {
    try {
      if (!req.query.screeningUserId) {
        throw new Error(149);
      }
      const screeningUser = await db.screening_user.findOne({ where: { id: req.query.screeningUserId } });
      if (!screeningUser) {
        throw new Error(150);
      }
      const userAgeInMonth = (parseInt(screeningUser.age_year) * 12) + parseInt(screeningUser.age_month);
      const questions = await db.speech_language_screening.findAll({
        where: { [db.op.and]: [{ age_start: { [db.op.lte]: userAgeInMonth } }, { age_end: { [db.op.gte]: userAgeInMonth } }, { isDeleted: false }] },
        attributes: ['id', 'question', 'age_start', 'age_end'],
        include: [{ model: db.speech_language_domain, attributes: ['id', 'name'] }],
        orderBy: [['domain_id', 'asc'], ['id', 'asc']]
      });
      const result = [];
      for (let i = 0; i < questions.length; i += 1) {
        result.push({
          id: questions[i].id,
          question: questions[i].question,
          age_start: questions[i].age_start,
          age_end: questions[i].age_end,
          domainData: questions[i].speech_language_domain
        });
      }
      respGenerator.sendResponse(res, { questions: result });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllHearingScreeningTypes(req, res) {
    try {
      if (!req.query.screeningUserId) {
        throw new Error(149);
      }
      const screeningUser = await db.screening_user.findOne({ where: { id: req.query.screeningUserId } });
      if (!screeningUser) {
        throw new Error(150);
      }
      const userAgeInMonth = (parseInt(screeningUser.age_year) * 12) + parseInt(screeningUser.age_month);
      const where = { screening_type: (userAgeInMonth > (18 * 12)) ? 2 : 1, isDeleted: false };
      const questions = await db.hearing_screening.findAll({
        where,
        attributes: ['id', 'question'],
        orderBy: [['id', 'asc']]
      });
      respGenerator.sendResponse(res, { questions });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async getAllCommonScreeningQuestions(req, res) {
    try {
      let userAgeInMonth;
      if (req.params.id == CATEGORY_IDS.SPEECH_AND_LANGUAGE || req.params.id == CATEGORY_IDS.HEARING) {
        if (!req.query.screeningUserId) {
          throw new Error(149);
        }
        const screeningUser = await db.screening_user.findOne({ where: { id: req.query.screeningUserId } });
        if (!screeningUser) {
          throw new Error(150);
        }
        userAgeInMonth = (parseInt(screeningUser.age_year) * 12) + parseInt(screeningUser.age_month);
      }
      if (req.params.id == CATEGORY_IDS.SPEECH_AND_LANGUAGE) {
        if (userAgeInMonth >= 60) {
          userAgeInMonth = 59;
        }
        const questions = await db.speech_language_screening.findAll({
          where: { [db.op.and]: [{ age_start: { [db.op.lte]: userAgeInMonth } }, { age_end: { [db.op.gte]: userAgeInMonth } }, { isDeleted: false }] },
          attributes: ['id', 'question'],
          include: [{ model: db.speech_language_domain, attributes: ['id', 'name'] }],
          orderBy: [['domain_id', 'asc'], ['id', 'asc']]
        });
        const result = [];
        for (let i = 0; i < questions.length; i += 1) {
          result.push({
            id: questions[i].id,
            question: questions[i].question,
            domain_name: questions[i].speech_language_domain.name
          });
        }
        return respGenerator.sendResponse(res, { questions: result });
      } else if (req.params.id == CATEGORY_IDS.HEARING) {
        const where = { isDeleted: false };
        if (req.query && req.query.type && ['child', 'adult'].includes(req.query.type.toString().toLowerCase())) {
          where.screening_type = (req.query.type.toString().toLowerCase() === 'child') ? 1 : 2;
        }
        // const where = { screening_type: (userAgeInMonth > (18 * 12)) ? 2 : 1 };
        const questions = await db.hearing_screening.findAll({
          where,
          attributes: ['id', 'question'],
          orderBy: [['id', 'asc']]
        });
        return respGenerator.sendResponse(res, { questions });
      }
      const questions = await db.common_screening.findAll({ where: { screening_id: req.params.id, isDeleted: false }, orderBy: [['id']], attributes: ['id', 'question'] });
      return respGenerator.sendResponse(res, { questions });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addScreeningAnswer(req, res) {
    try {
      console.log(req.body);
      const device = await db.device.findOne({ where: { device_id: req.user.deviceId } });
      if (!device) {
        throw new Error(147);
      }
      req.body.deviceId = device.id;
      const category = await db.screening_category.findOne({ where: { id: parseInt(req.body.screeningId) } });
      const { answers, userAgeInMonth } = await queryHelper.validateScreeningAnswer(req.body, device.id);
      const screeningId = req.body.screeningId;
      let decidingValue = answerResponse[answers[0].screeningId].decidingValue;
      let decidingData = answerResponse[answers[0].screeningId].data;
      let wrongAnswers = 0;
      if (decidingValue === undefined) {
        decidingValue = answerResponse[answers[0].screeningId][req.body.type.toString().toLowerCase().trim()].decidingValue;
        decidingData = answerResponse[answers[0].screeningId][req.body.type.toString().toLowerCase().trim()].data;
        if (req.body.type.toString().toLowerCase().trim() === 'child') {
          answers.forEach((answer, index) => {
            if ((index > 8 && answer.answer == true) || (index <= 8 && answer.answer == false)) {
              wrongAnswers += 1;
            }
          });
        } else {
          answers.forEach((answer) => {
            if (answer.answer == true) {
              wrongAnswers += 1;
            }
          });
        }
      }
      if (category && [true, false].includes(category.decidingValue)) {
        console.log(category.decidingValue, 'category.decidingValue');
        decidingValue = category.decidingValue;
        if (category.id == 3 && req.body.type.toString().toLowerCase().trim() == 'adult') {
          decidingValue = category.decidingValue2;
        }
      }
      if (screeningId != 3) {
        wrongAnswers = _.filter(answers, answer => answer.answer === decidingValue);
      } else {
        wrongAnswers = { length: wrongAnswers };
      }
      console.log(decidingValue, decidingData.length, wrongAnswers.length);
      let message = 'answer has been recorded';
      let url = 'https://www.youtube.com/watch?v=IAIGnS9BPKs';
      let selected_smiley = 0;
      decidingData.forEach((text, index) => {
        if ((wrongAnswers.length >= text.start && wrongAnswers.length <= text.end) || (text.end === null && wrongAnswers.length >= text.start)) {
          message = text.text;
          url = text.youtube;
          selected_smiley = index;
        }
      });
      await db.screening_answer.bulkCreate(answers);
      const youtubeLinks = [];
      if (category.youtube_link_list_success && selected_smiley == 0) {
        category.youtube_link_list_success.split(',').forEach((link) => {
          youtubeLinks.push(link.toString().trim());
        });
      }
      if (category.youtube_link_list_error && selected_smiley != 0) {
        category.youtube_link_list_error.split(',').forEach((link) => {
          youtubeLinks.push(link.toString().trim());
        });
      }
      respGenerator.sendResponse(res, { message, url, total_smileys: decidingData.length, selected_smiley, youtubeLinks });
      // respGenerator.sendResponse(res, { message: `${message.substr(0, message.length - 14)} <br/> <a href='${url}'>click here </a>${message.substr(message.length - 14)}`, url });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }

  async addArticulationAnswer(req, res) {
    try {
      const device = await db.device.findOne({ where: { device_id: req.user.deviceId } });
      if (!device) {
        throw new Error(147);
      }
      const category = await db.screening_category.findOne({ where: { id: 1 } });
      req.body.deviceId = device.id;
      const { answers } = await queryHelper.validateArticulationAnswer(req.body, device.id);
      const wrongAnswers = _.filter(answers, answer => (answer.answer === 'incorrect' || answer.answer === 'partially correct'));
      let message = 'Based upon your responses your child have some issue with articulation or pronunciation. Check the Sound acquisition pattern to know more about it.\n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region.\n\nTo know more about the Speech Language Pathologist and Speech Therapy click of the following links.';
      let selected_smiley = 1;
      if (wrongAnswers.length === 0) {
        message = 'Based upon your responses your child does not have any issue with articulation or pronunciation but if you still feel your child does not produce certain sounds which are not included here you can consult your nearby Speech Language Pathologist.\n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region.\n\nTo know more about the Speech Language Pathologist and Speech Therapy click of the following links.';
        selected_smiley = 0;
      }
      const youtubeLinks = [];
      if (category.youtube_link_list_success && selected_smiley == 0) {
        category.youtube_link_list_success.split(',').forEach((link) => {
          youtubeLinks.push(link.toString().trim());
        });
      }
      if (category.youtube_link_list_error && selected_smiley != 0) {
        category.youtube_link_list_error.split(',').forEach((link) => {
          youtubeLinks.push(link.toString().trim());
        });
      }
      console.log(youtubeLinks, wrongAnswers.length);
      const articulationAnswers = await db.articulation_answer.bulkCreate(answers);
      respGenerator.sendResponse(res, { message, url: 'https://www.youtube.com/watch?v=IAIGnS9BPKs', articulationAnswers, total_smileys: 2, selected_smiley, fileUrl: `${SERVER_URL}/sound/sound.pdf`, youtubeLinks });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
}

module.exports = new UserController();
// db.screening_user.update({ age_year: 0, age_month: 2 }, { where: {} });
