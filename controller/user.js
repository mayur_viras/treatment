const jwt = require('jsonwebtoken');
const { jwtConfig } = require('../config');
const respGenerator = require('../response/json-response');
const mailer = require('../lib/mailer');
const db = require('../helpers/mysql');

class UserController {

  async login(req, res) {
    try {
      const user = {}; // find user from db by email/username
      if (!user) {
        throw new Error(105);
      }
      if (req.body.password !== user.password.toString()) {
        throw new Error(106);
      }
      if (user.status.toLowerCase() !== 'active') {
        throw new Error(114);
      }
      const data = {
        id: user.id,
        email: user.email,
        role: user.role.toLowerCase()
      };
      const token = jwt.sign(data, jwtConfig.secret);
      // insert token in database
      respGenerator.sendResponse(res, {
        token,
        user: Object.assign({
          firstName: user.firstName,
          lastName: user.lastName,
          userName: user.userName
        }, data)
      });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async logout(req, res) {
    try {
      // delete token from db
      respGenerator.sendResponse(res, { message: 'you have been logged out from this device' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async forgetPassword(req, res) {
    try {
      const user = {
        passwordCode: '',
        email: ''
      }; // find user from db
      if (!user) {
        throw new Error(115);
      }
      user.passwordCode = Math.floor(Math.random() * 899999 + 100000);
      mailer.sendMail('reset your password of your blah blah account', `your reset password code is : <b>${user.passwordCode}</b>`, user.email);
      // db query to  update user with new passwordCode
      respGenerator.sendResponse(res, { message: 'Reset password link sent to your email address' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async resetPassword(req, res) {
    try {
      const user = {
        password: '',
        passwordCode: ''
      };
      if (!user) {
        throw new Error(119);
      }
      user.passwordCode = null;
      user.password = req.body.password;
      // db query to  update user's password
      mailer.sendMail('password change', 'your password has been changed successfully', user.email);
      respGenerator.sendResponse(res, { message: 'your password has been changed' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async changePassword(req, res) {
    try {
      const user = { password: '' };
      if (req.body.oldPassword !== user.password.toString()) {
        throw new Error(106);
      }
      user.password = req.body.newPassword;
      // db query to change password
      respGenerator.sendResponse(res, { message: 'your password has been changed successfully' });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
  async getProfile(req, res) {
    try {
      const user = {}; // db query to get user profile
      respGenerator.sendResponse(res, { user });
    } catch (err) {
      respGenerator.sendError(res, err);
    }
  }
}

module.exports = new UserController();
