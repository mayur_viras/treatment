const AWS = require('aws-sdk');
const awsConfig = require('../config/aws');

AWS.config.update({
  region: awsConfig.region,
  credentials: {
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey
  }
});

module.exports = AWS;
