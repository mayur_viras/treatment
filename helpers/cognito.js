const cognito = require('../config/cognito');
const AWS = require('./aws');

const sts = new AWS.STS();

class CognitoHelper {

  assumeRoleWithWebIdentity(webIdentityToken) {
    return new Promise((resolve, reject) => {
      const params = {
        DurationSeconds: cognito.TOKEN_DURATION,
        RoleArn: cognito.ASSUME_ROLE_ARN,
        RoleSessionName: cognito.SESSION_ROLE_NAME,
        WebIdentityToken: webIdentityToken
      };
      sts.assumeRoleWithWebIdentity(params, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }
  getOpenIdDeveloperToken() {
    return new Promise((resolve, reject) => {
      const params = {
        IdentityPoolId: cognito.IDENTITY_POOL_ID,
        Logins: { 'com.whatever-your-dns.com': 'test' },
        TokenDuration: cognito.TOKEN_DURATION
      };
      const cognitoidentity = new AWS.CognitoIdentity();
      cognitoidentity.getOpenIdTokenForDeveloperIdentity(params, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }
}
module.exports = new CognitoHelper();
