const mongoose = require('mongoose');
const { mongoConfig } = require('../config');

/*
mongoConfig: {
  url: `mongodb://user:pass@host:27017/db`,
}
*/
mongoose.connect(mongoConfig.url, {
  useMongoClient: true,
  socketTimeoutMS: 0,
  keepAlive: true,
  reconnectTries: 30
});

mongoose.connection.on('connected', () => {
  console.log('Mongoose default connection open to ', mongoConfig.url);
});

mongoose.connection.on('error', (err) => {
  console.log(`Mongoose default connection error: ${err}`);
});

mongoose.connection.on('disconnected', () => {
  console.log('Mongoose default connection disconnected');
});

mongoose.Promise = global.Promise;
module.exports = mongoose;

const stopHandler = () => {
  console.log('mongoose connection closed forcefully!!');
  mongoose.connection.close();
  process.exit(0);
};

process.on('SIGTERM', stopHandler);
process.on('SIGINT', stopHandler);
process.on('SIGHUP', stopHandler);
