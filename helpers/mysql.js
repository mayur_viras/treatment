const config = require('../config');
const Sequelize = require('sequelize');

/*
{
  username: '',
  password: '',
  database: '',
  host: '',
  dialect: '',
  dialectOptions: { decimalNumbers: true },
  logging: false,
  operatorsAliases: sequelizeAllias,
  pool: {
    max: 20,
    min: 0,
    idle: 20000,
    acquire: 20000,
    handleDisconnects: true,
  },
}
*/
const sequelize = new Sequelize(config.databaseConfig);
sequelize
  .authenticate()
  .then(() => {
    console.log('sequelize connection has been established successfully.');
  })
  .catch((err) => {
   console.error('Unable to connect to the database:', err);
  });

module.exports = sequelize;

const stopHandler = () => {
  console.log('sequelize connection closed forcefully!!');
  sequelize.close();
  process.exit(0);
};

process.on('SIGTERM', stopHandler);
process.on('SIGINT', stopHandler);
process.on('SIGHUP', stopHandler);
