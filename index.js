const express = require('express');
const bodyParser = require('body-parser');
const validate = require('express-validation');
const { application } = require('./config');
const index = require('./routes/index');
const path = require('path');
const bugsnag = require('./lib/bugsnag');
const compression = require('compression');
const app = express();
const morgan = require('morgan');
// setTimeout(() => {
// const setup = require('./setup');
//   setup();
// }, 10000);
const cors = require('cors');
const busboyBodyParser = require('busboy-body-parser');
app.use(busboyBodyParser());
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(bugsnag.requestHandler);

app.use(bugsnag.errorHandler);
app.get(['/ping'], (req, resp) => {
  resp.status(200).send({ isError: false, message: 'ping' });
});

app.use(cors());
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/apidocs', express.static(path.join(__dirname, 'apidocs')));

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  next();
});

app.use('/api/v1', index);
app.use('/sound/sound.pdf', (req, res) => {
  res.sendFile(path.join(__dirname, 'sound.pdf'));
});

app.use(compression());
app.use(express.static(path.resolve(__dirname, './3', 'dist'), { maxAge: (1 * 365 * 24 * 60 * 60 * 1000) }));
app.get('*', (req, resp) => resp.sendFile(path.resolve(__dirname, './3', 'dist', 'index.html')));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});


app.use((req, res) => {
  res.status(404).json({ isError: true, message: 'API not found' });
});

app.use((err, req, res) => {
  if (err instanceof validate.ValidationError) {
    return res.status(err.status).json({ isError: true, type: 'validationError', error: err.errors, message: (err.errors && err.errors.length && err.errors[0].messages && err.errors[0].messages.length) ? err.errors[0].messages[0] : undefined });
  }
  if (err.name === 'ValidationError') {
    return res.status(400).json({ isError: true, message: err.message });
  }
  if (process.env.NODE_ENV === 'development') {
    return res.status(500).json({ isError: true, message: err.message, error: err });
  }
  bugsnag.notify(err);
  return res.status(500);
});

app.listen(process.env.PORT || application.port, () => {
  console.log('Server started on port', application.port);
});

const stopHandler = () => {
  console.log('Stopped backend forcefully!!');
  process.exit(0);
};

process.on('SIGTERM', stopHandler);
process.on('SIGINT', stopHandler);
process.on('SIGHUP', stopHandler);
