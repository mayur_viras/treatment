const bugsnag = require('bugsnag');
const {
  bugsnagConfig,
  application
} = require('../config');

bugsnag.register({
  apiKey: bugsnagConfig.apiKey,
  releaseStage: process.env.NODE_ENV || application.env
});
bugsnag.register(bugsnagConfig.apiKey);

module.exports.requestHandler = bugsnag.requestHandler;
module.exports.errorHandler = bugsnag.errorHandler;
module.exports.notify = bugsnag.notify;
