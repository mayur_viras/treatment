
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const { emailConfig } = require('../config');
const bugsnag = require('./bugsnag');

const transporter = nodemailer.createTransport(smtpTransport({
  service: 'SES',
  secure: false,
  port: 587,
  auth: {
    user: emailConfig.username,
    pass: emailConfig.password
  }
}));

class Mailer {
  async sendMail(subject, html, email) {
    try {
      const mailOptions = {
        from: `"${emailConfig.name}" <${emailConfig.sender}>`,
        to: `${email}`,
        subject,
        text: '',
        html
      };
      const mailRes = await transporter.sendMail(mailOptions);
      console.log(mailRes, 'mailRes');
    } catch (error) {
      bugsnag.notify(error);
    }
  }
}

module.exports = new Mailer();
module.exports.transporter = transporter;
