const db = require('../models');

class QueryHelper {
  async validateNewProfessional(body) {
    // const city = await db.city.findOne({ where: { id: parseInt(body.city_id || 0) }, attributes: ['id'] });
    // if (!city) {
    //   throw new Error('please provide a valid city');
    // }
    // if (body.qualification_id) {
    //   const qualification = await db.qualification.findOne({ where: { id: parseInt(body.qualification_id || 0) }, attributes: ['id'] });
    //   if (!qualification) {
    //     throw new Error('please provide a valid qualification');
    //   }
    // }
    // if (body.designation_id) {
    //   const designation = await db.designation.findOne({
    //     where: { isDeleted: false, id: parseInt(body.designation_id || 0) },
    //     attributes: ['id']
    //   });
    //   if (!designation) {
    //     throw new Error('please provide a valid designation');
    //   }
    // }
    // if (body.working_at_id) {
    //   const working_at = await db.working_at.findOne({
    //     where: { isDeleted: false, id: parseInt(body.working_at_id || 0) },
    //     attributes: ['id']
    //   });
    //   if (!working_at) {
    //     throw new Error('please provide a valid working_at');
    //   }
    // }
    // if (body.identity_proof_id) {
    //   const identity_proof = await db.identity_proof.findOne({
    //     where: { isDeleted: false, id: parseInt(body.identity_proof_id || 0) },
    //     attributes: ['id']
    //   });
    //   if (!identity_proof) {
    //     throw new Error('please provide a valid identity_proof');
    //   }
    // }
    // const consultation_charges_currency = await db.consultation_currency.findOne({
    //   where: { isDeleted: false, id: parseInt(body.consultation_charges_currency_id) },
    //   attributes: ['id']
    // });
    // if (!consultation_charges_currency) {
    //   throw new Error('please provide a valid currency');
    // }
    return {
      city_id: parseInt(body.city_id) || null,
      address: body.address || '',
      // qualification_id: parseInt(body.qualification_id),
      qualification_name: body.qualification_name || '',
      // designation_id: parseInt(body.designation_id),
      designation_name: body.designation_name || '',
      // working_at_id: parseInt(body.working_at_id),
      working_at_name: body.working_at_name || '',
      working_time_from: body.working_time_from,
      working_time_to: body.working_time_to,
      services_name: body.services_name || '',
      days_available: body.days_available || '',
      // identity_proof_id: parseInt(body.identity_proof_id),
      identity_proof_name: body.identity_proof_name,
      association_reg_name: body.association_reg_name || '',
      association_reg_number: body.association_reg_number || '',
      languages_known: body.languages_known || '',
      consultation_charges: body.consultation_charges || 0,
      // consultation_charges_currency_id: parseInt(body.consultation_charges_currency_id),
      consultation_charges_currency: body.consultation_charges_currency,
      account_number: body.account_number || '',
      ifsc_code: body.ifsc_code || '',
      extra_details: body.extra_details || ''
    };
  }

  async validateNewStudent(body) {
    // const student_course = await db.student_course.findOne({ where: { id: parseInt(body.course_id || 0) }, attributes: ['id'] });
    // if (!student_course) {
    //   throw new Error('please provide a valid course');
    // }
    // const identity_proof = await db.identity_proof.findOne({
    //   where: { isDeleted: false, id: parseInt(body.identity_proof_id || 0) },
    //   attributes: ['id']
    // });
    // if (!identity_proof) {
    //   throw new Error('please provide a valid identity_proof');
    // }
    // const student_language = await db.consultation_currency.findOne({
    //   where: { isDeleted: false, id: parseInt(body.languages_known_id) },
    //   attributes: ['id']
    // });
    // if (!student_language) {
    //   throw new Error('please provide a valid language');
    // }
    return {
      // course_id: parseInt(body.course_id),
      course_name: body.course_name,
      year: body.year,
      institute: body.institute,
      identity_proof: body.identity_proof,
      // identity_proof_id: parseInt(body.identity_proof_id),
      identity_proof_name: body.identity_proof_name,
      association_reg_name: body.association_reg_name || '',
      association_reg_number: body.association_reg_number || '',
      // languages_known_id: parseInt(body.languages_known_id),
      languages_known_name: body.languages_known_name,
      extra_details: body.extra_details || ''
    };
  }
}

module.exports = new QueryHelper();
