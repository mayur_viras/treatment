const { SPEECH_AND_LANGUAGE_DOMAIN_IDS, HEARING_TYPES, COMMON_SCREENING_IDS, GAMES, CATEGORY_IDS_REVERSE, CATEGORY_IDS } = require('../response/constants');
const db = require('../models');
const uuid = require('uuid');

class QueryHelper {
  async validateEditProfile(req) {
    if (!req.body.name) {
      throw new Error('please provide name');
    }
    if (!this.isNumeric(req.body.age)) {
      throw new Error('please provide numeric age');
    }
    if (![1, 2, 3].includes(parseInt(req.body.user_type))) {
      throw new Error(`please provide valid user type - ${req.body.user_type} is not valid`);
    }
    const body = {
      name: req.body.name,
      mobile_no: req.body.mobile_no,
      age: parseInt(req.body.age),
      gender: req.body.gender,
      country_id: parseInt(req.body.country_id),
      state_id: parseInt(req.body.state_id),
      user_type: parseInt(req.body.user_type)
    };
    const state = await db.state.findOne({ where: { id: parseInt(body.state_id) }, attributes: ['id', 'country_id'] });
    if (!state) {
      throw new Error('please provide a valid state');
    }
    if (state.country_id != body.country_id) {
      throw new Error('country does not match with the given state');
    }
    return body;
  }
  async validateNewUser(body) {
    if (!body.email) {
      throw new Error('please provide email');
    }
    if (!body.password) {
      throw new Error('please provide password');
    }
    if (!body.name) {
      throw new Error('please provide name');
    }
    if (!this.isNumeric(body.age)) {
      throw new Error('please provide numeric age');
    }
    if (![1, 2, 3].includes(parseInt(body.user_type))) {
      throw new Error(`please provide valid user type - ${body.user_type} is not valid`);
    }
    const existingUser = await db.client_user.findOne({ where: { email: body.email.toLowerCase() }, attributes: ['id', 'email'] });
    if (existingUser) {
      throw new Error('email already exists');
    }
    const state = await db.state.findOne({ where: { id: parseInt(body.state_id) }, attributes: ['id', 'country_id'] });
    if (!state) {
      throw new Error('please provide a valid state');
    }
    if (state.country_id != body.country_id) {
      throw new Error('country does not match with the given state');
    }
    return {
      name: body.name,
      email: body.email.toLowerCase(),
      password: body.password,
      device_id: body.device_id || '',
      passwordCode: (100000 + Math.floor(Math.random() * 900000)).toString(),
      mobile_no: body.mobile_no,
      age: parseInt(body.age),
      gender: body.gender,
      country_id: parseInt(body.country_id),
      state_id: parseInt(body.state_id),
      user_type: 1
    };
  }
  isNumeric(val) {
    return val !== undefined && val !== null && !isNaN(val) && val !== Infinity;
  }
  validateSpeechLanQuestion(body) {
    if (!this.isNumeric(body.age_start)) {
      throw new Error(122);
    }
    if (!this.isNumeric(body.age_end)) {
      throw new Error(123);
    }
    if (this.age_start > this.age_end) {
      throw new Error(127);
    }
    if (!body.question) {
      throw new Error(124);
    }
    if (!body.domain_id || !SPEECH_AND_LANGUAGE_DOMAIN_IDS[body.domain_id]) {
      throw new Error(126);
    }
  }

  validateEditSpeechLanQuestion(body) {
    if (!this.isNumeric(body.age_start)) {
      throw new Error(122);
    }
    if (!this.isNumeric(body.age_end)) {
      throw new Error(123);
    }
    if (this.age_start > this.age_end) {
      throw new Error(127);
    }
    if (!body.question) {
      throw new Error(124);
    }
  }

  validateHearingQuestion(body) {
    if (!body.question) {
      throw new Error(124);
    }
    if (!body.screening_type || !HEARING_TYPES[body.screening_type]) {
      throw new Error(128);
    }
  }

  validateCommonScreeningQuestion(body) {
    if (!body.question) {
      throw new Error(124);
    }
    if (!body.screening_id || !COMMON_SCREENING_IDS[body.screening_id]) {
      throw new Error(129);
    }
  }

  validateEducationalVideoBody(body) {
    if (!body.language) {
      throw new Error(138);
    }
    if (!body.videoTitle) {
      throw new Error(139);
    }
    if (!body.category) {
      throw new Error(159);
    }
    if (!body.video) {
      throw new Error(140);
    }
  }

  validateGameData(body, files) {
    if (!body.gameId || !GAMES[body.gameId]) {
      throw new Error(131);
    }
    if (!body.categoryId || !this.isNumeric(body.categoryId)) {
      throw new Error(132);
    }
    if (!body.level || !this.isNumeric(body.level)) {
      throw new Error(133);
    }
    if ((GAMES[body.gameId] === 'IDENTIFICATION' || GAMES[body.gameId] === 'WHAT_DO_I_DO' || GAMES[body.gameId] === 'WHAT_DO_YOU_USE_FOR') && !body.question) {
      throw new Error(124);
    }
    if (
      GAMES[body.gameId] === 'IDENTIFICATION'
      || GAMES[body.gameId] === 'PICTURE_MATCHING'
      || GAMES[body.gameId] === 'SHADOW_MATCHING'
      || GAMES[body.gameId] === 'WHAT_DO_I_DO'
      || GAMES[body.gameId] === 'WHAT_DO_YOU_USE_FOR'
      || GAMES[body.gameId] === 'GUESS_ME'
      || GAMES[body.gameId] === 'ODD_ONE_OUT'
      || GAMES[body.gameId] === 'MEMORY_AND_SEQUENCING'
      || GAMES[body.gameId] === 'MEMORY'
      || GAMES[body.gameId] === 'AUDITORY_DISCRIMINATION'
    ) {
      const images = [];
      for (const file in files) {
        if (files.hasOwnProperty(file)) {
          if (file.startsWith('images-')) {
            images.push(files[file]);
            if (!files[file].mimetype.includes('image')) {
              throw new Error(134);
            }
          }
        }
      }
      if (!images || !images.length || images.length < 2) {
        throw new Error(134);
      }
      if (
        GAMES[body.gameId] !== 'WHAT_DO_YOU_USE_FOR'
        && GAMES[body.gameId] !== 'MEMORY_AND_SEQUENCING'
        && GAMES[body.gameId] !== 'MEMORY'
        && GAMES[body.gameId] !== 'AUDITORY_DISCRIMINATION'
        && (
          !body.correctImage
          || !this.isNumeric(body.correctImage)
          || parseInt(body.correctImage) < 0
          || parseInt(body.correctImage) >= images.length
        )
      ) {
        throw new Error(137);
      }
      if (GAMES[body.gameId] === 'WHAT_DO_YOU_USE_FOR' || GAMES[body.gameId] === 'MEMORY_AND_SEQUENCING' || GAMES[body.gameId] === 'MEMORY') {
        if (!body.correctImages || !body.correctImages.length) {
          throw new Error(137);
        }
        body.correctImages = body.correctImages.split(',');
        body.correctImages.forEach((correctImage) => {
          if (!this.isNumeric(correctImage)
          || parseInt(correctImage) < 0
          || parseInt(correctImage) >= images.length) {
            throw new Error(137);
          }
        });
      }
      files.images = images;
      if ((GAMES[body.gameId] === 'GUESS_ME' || GAMES[body.gameId] === 'IDENTIFICATION') && (!files.audio || !files.audio.mimetype.includes('audio'))) {
        throw new Error(135);
      }
    } else {
      throw new Error(136);
    }
  }
  validateScreeningUser(data) {
    if (!data.name) {
      throw new Error(144);
    }
    if (!data.gender) {
      throw new Error(146);
    }
    if (data.dob) {
      const timeDiff = (new Date().getTime() - new Date(data.dob).getTime()) / (1000 * 60 * 60 * 24 * 365);
      data.age_year = Math.floor(timeDiff);
      data.age_month = Math.round((timeDiff - data.age_year) * 30);
    }
    if (!this.isNumeric(data.age_month) || !this.isNumeric(data.age_year)) {
      throw new Error(145);
    }
    if (data.age_month < 0 || data.age_month > 11) {
      throw new Error(148);
    }
  }
  async validateScreeningAnswer(data, deviceId) {
    if (!data.screeningUserId || !this.isNumeric(data.screeningUserId)) {
      throw new Error(155);
    }
    const screeningUser = await db.screening_user.findOne({ where: { id: data.screeningUserId } });
    if (!screeningUser || screeningUser.device_id !== deviceId) {
      throw new Error(155);
    }
    let userAgeInMonth = (parseInt(screeningUser.age_year) * 12) + parseInt(screeningUser.age_month);
    if (!data.screeningId || !CATEGORY_IDS_REVERSE[data.screeningId] || data.screeningId == '1') {
      throw new Error(152);
    }
    if (!data.answer) {
      throw new Error(153);
    }
    data.answer = data.answer.split(',');
    if (!data.answer || !Array.isArray(data.answer)) {
      throw new Error(153);
    }
    data.answer.forEach((answer, index) => {
      if (!['true', 'false'].includes(answer.toString().trim())) {
        throw new Error(153);
      }
      data.answer[index] = (answer.toString().trim().toLowerCase()) === 'true';
    });
    let questions = null;
    if (data.screeningId == CATEGORY_IDS.SPEECH_AND_LANGUAGE) {
      if (userAgeInMonth >= 60) {
        userAgeInMonth = 59;
      }
      questions = await db.speech_language_screening.findAll({
        where: { [db.op.and]: [{ age_start: { [db.op.lte]: userAgeInMonth } }, { age_end: { [db.op.gte]: userAgeInMonth } }, { isDeleted: false }] },
        attributes: ['id'],
        orderBy: [['domain_id', 'asc'], ['id', 'asc']]
      });
    } else if (data.screeningId == CATEGORY_IDS.HEARING) {
      if (!data.type || !['child', 'adult'].includes(data.type.toString().toLowerCase())) {
        throw new Error(160);
      }
      questions = await db.hearing_screening.findAll({
        // where: { screening_type: (userAgeInMonth > (18 * 12)) ? 2 : 1 },
        where: { screening_type: (data.type.toString().toLowerCase() === 'child') ? 1 : 2, isDeleted: false },
        attributes: ['id'],
        orderBy: [['id', 'asc']]
      });
    } else {
      questions = await db.common_screening.findAll({ where: { screening_id: data.screeningId, isDeleted: false }, attributes: ['id'], orderBy: [['id', 'asc']] });
    }
    if (!questions || questions.length !== data.answer.length) {
      throw new Error(156);
    }
    const answerBulk = [];
    const uuidOfScreening = uuid();
    data.answer.forEach((answer, index) => {
      answerBulk.push({
        screeningId: data.screeningId,
        deviceId: data.deviceId,
        screeningUserId: data.screeningUserId,
        questionId: questions[index].id,
        answer,
        uuid: uuidOfScreening
      });
    });
    return { userAgeInMonth, answers: answerBulk };
  }

  async validateArticulationAnswer(data, deviceId) {
    if (!data.screeningUserId || !this.isNumeric(data.screeningUserId)) {
      throw new Error(155);
    }
    const screeningUser = await db.screening_user.findOne({ where: { id: data.screeningUserId } });
    if (!screeningUser || screeningUser.device_id !== deviceId) {
      throw new Error(155);
    }
    if (!data.answer) {
      throw new Error(153);
    }
    data.answer = data.answer.split(',');
    if (!data.answer || !Array.isArray(data.answer)) {
      throw new Error(153);
    }
    data.answer.forEach((answer, index) => {
      if (!['correct', 'incorrect', 'partially correct'].includes(answer.toString().trim())) {
        throw new Error(157);
      }
      data.answer[index] = answer.toString().trim();
    });
    const pictures = await db.atriculation_picture.findAll({ attributes: ['id'], orderBy: [['id', 'asc']] });
    if (!pictures || pictures.length !== data.answer.length) {
      throw new Error(156);
    }
    const answerBulk = [];
    const uuidOfScreening = uuid();
    data.answer.forEach((answer, index) => {
      answerBulk.push({
        deviceId: data.deviceId,
        screeningUserId: data.screeningUserId,
        pictureId: pictures[index].id,
        answer,
        uuid: uuidOfScreening
      });
    });
    return { answers: answerBulk };
  }
}

module.exports = new QueryHelper();
