module.exports = (sequelize, DataType) => sequelize.define('appointment', {
  user_id: { type: DataType.INTEGER },
  professional_id: { type: DataType.INTEGER },
  appointment_type: { type: DataType.STRING },
  professional_name: { type: DataType.STRING },
  date: { type: DataType.STRING },
  date_str: { type: DataType.DATE },
  time_slot_from: { type: DataType.STRING },
  time_slot_to: { type: DataType.STRING }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
