module.exports = (sequelize, DataType) => sequelize.define('articulation_answer', {
  pictureId: { type: DataType.INTEGER },
  deviceId: { type: DataType.INTEGER },
  screeningUserId: { type: DataType.INTEGER },
  answer: { type: DataType.STRING(35) },
  uuid: { type: DataType.TEXT }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
