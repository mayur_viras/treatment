module.exports = (sequelize, DataType) => sequelize.define('atriculation_picture', {
  screening_id: { type: DataType.INTEGER },
  picture_url: { type: DataType.TEXT },
  audio_url: { type: DataType.TEXT },
  correct_word: { type: DataType.TEXT },
  target_sound: { type: DataType.TEXT }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
