module.exports = (sequelize, DataType) => sequelize.define('blog', {
  title: { type: DataType.TEXT },
  language: { type: DataType.TEXT },
  description: { type: DataType.TEXT },
  image: { type: DataType.TEXT, defaultValue: '' },
  video: { type: DataType.TEXT, defaultValue: '' },
  category: { type: DataType.TEXT, required: true }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
