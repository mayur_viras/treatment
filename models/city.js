module.exports = (sequelize, DataType) => sequelize.define('city', {
  name: { type: DataType.TEXT },
  state_id: { type: DataType.INTEGER },
  isDeleted: { type: DataType.BOOLEAN, defaultValue: false }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
