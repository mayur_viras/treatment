module.exports = (sequelize, DataType) => sequelize.define('client_user', {
  name: { type: DataType.TEXT },
  device_id: { type: DataType.TEXT },
  email: { type: DataType.TEXT },
  mobile_no: { type: DataType.TEXT },
  age: { type: DataType.INTEGER },
  gender: { type: DataType.TEXT },
  country_id: { type: DataType.INTEGER },
  state_id: { type: DataType.INTEGER },
  user_type: { type: DataType.INTEGER },
  password: { type: DataType.TEXT },
  passwordCode: { type: DataType.TEXT },
  profile: { type: DataType.TEXT },
  isVerified: { type: DataType.BOOLEAN, defaultValue: false }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
