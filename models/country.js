module.exports = (sequelize, DataType) => sequelize.define('country', {
  name: { type: DataType.TEXT },
  isDeleted: { type: DataType.BOOLEAN, defaultValue: false }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
