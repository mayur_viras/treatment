module.exports = (sequelize, DataType) => sequelize.define('device', { device_id: { type: DataType.TEXT } }, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
