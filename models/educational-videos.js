module.exports = (sequelize, DataType) => sequelize.define('educational_videos', {
  title: { type: DataType.TEXT },
  language: { type: DataType.TEXT },
  video: { type: DataType.TEXT, defaultValue: '' },
  category: { type: DataType.TEXT, required: true }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
