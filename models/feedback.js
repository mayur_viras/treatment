module.exports = (sequelize, DataType) => sequelize.define('feedback', {
  title: { type: DataType.TEXT, required: true },
  isVisibleToAdminOnly: { type: DataType.BOOLEAN, required: true },
  user: { type: DataType.TEXT },
  userId: { type: DataType.INTEGER, required: true }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
