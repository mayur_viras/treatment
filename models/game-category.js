module.exports = (sequelize, DataType) => sequelize.define('game_category', { name: { type: DataType.TEXT } }, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
