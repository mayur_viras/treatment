module.exports = (sequelize, DataType) => sequelize.define('game_data', {
  categoryId: { type: DataType.INTEGER },
  gameId: { type: DataType.INTEGER },
  audio: { type: DataType.TEXT },
  question: { type: DataType.TEXT },
  level: { type: DataType.INTEGER }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
