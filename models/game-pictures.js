module.exports = (sequelize, DataType) => sequelize.define('game_pictures', {
  gameDataId: { type: DataType.INTEGER },
  isMainPicture: { type: DataType.BOOLEAN, allowNull: true },
  isCorrect: { type: DataType.BOOLEAN, allowNull: true },
  picture: { type: DataType.TEXT }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
