module.exports = (sequelize, DataType) => sequelize.define('game', { name: { type: DataType.TEXT } }, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
