module.exports = (sequelize, DataType) => sequelize.define('hearing_screening_type', { name: { type: DataType.TEXT } }, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
