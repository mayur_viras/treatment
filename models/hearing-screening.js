module.exports = (sequelize, DataType) => sequelize.define('hearing_screening', {
  question: { type: DataType.TEXT },
  youtube_link: { type: DataType.TEXT },
  screening_id: { type: DataType.INTEGER },
  screening_type: { type: DataType.INTEGER },
  isDeleted: { type: DataType.BOOLEAN, defaultValue: false }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
