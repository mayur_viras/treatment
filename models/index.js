const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const sequelize = require('../helpers/mysql.js');
let db = {};
fs.readdirSync('./models').forEach((file) => {
  if (file !== 'index.js' && file !== 'relations.js' && path.extname(file) === '.js') {
    const fullpath = path.join(__dirname, file);
    const filemodule = sequelize.import(fullpath);
    db[filemodule.name] = filemodule;
  } else if (file === 'client-add-ons') {
    fs.readdirSync(`./models/${file}`).forEach((clientAddOns) => {
      const fullFilepath = path.join(__dirname, file, clientAddOns);
      const clientAddOnsmodule = sequelize.import(fullFilepath);
      db[clientAddOnsmodule.name] = clientAddOnsmodule;
    });
  }
});

db.Sequelize = Sequelize;
db = require('./relations.js')(db);
db.sequelize = db.user.sequelize;
db.sequelize.sync();
// db.sequelize.sync({ alter: true });
module.exports = db;

module.exports.executeQuery = async (query, type) => {
  const result = await sequelize.query(query, { type: sequelize.QueryTypes[type || 'SELECT'] });
  return result;
};

module.exports.op = sequelize.Sequelize.Op;
