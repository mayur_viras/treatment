const { GAMES } = require('../../response/constants');

class GameMapper {
  mapGameData(gameData) {
    const gameDataValues = [];
    gameData.forEach((gameDataValue) => {
      const gameDataObj = {
        id: gameDataValue.id,
        name: gameDataValue.name,
        gameData: []
      };
      if (
        GAMES[gameDataObj.id] === 'IDENTIFICATION' || GAMES[gameDataObj.id] === 'PICTURE_MATCHING'
        || GAMES[gameDataObj.id] === 'SHADOW_MATCHING' || GAMES[gameDataObj.id] === 'WHAT_DO_I_DO'
        || GAMES[gameDataObj.id] === 'WHAT_DO_YOU_USE_FOR' || GAMES[gameDataObj.id] === 'GUESS_ME'
        || GAMES[gameDataObj.id] === 'ODD_ONE_OUT' || GAMES[gameDataObj.id] === 'MEMORY_AND_SEQUENCING'
        || GAMES[gameDataObj.id] === 'AUDITORY_DISCRIMINATION' || GAMES[gameDataObj.id] === 'MEMORY'
      ) {
        gameDataValue.game_data.forEach((game_data) => {
          game_data.game_pictures.forEach((picture, index) => {
            if (GAMES[gameDataObj.id] === 'PICTURE_MATCHING' || GAMES[gameDataObj.id] === 'SHADOW_MATCHING') {
              picture.dataValues.pictureToMatch = (index === 0);
              delete picture.dataValues.isCorrect;
              picture.dataValues.isCorrect = picture.dataValues.isMainPicture;
              delete picture.dataValues.isMainPicture;
            } else if (
              GAMES[gameDataObj.id] === 'IDENTIFICATION' || GAMES[gameDataObj.id] === 'WHAT_DO_I_DO'
              || GAMES[gameDataObj.id] === 'WHAT_DO_YOU_USE_FOR' || GAMES[gameDataObj.id] === 'GUESS_ME'
              || GAMES[gameDataObj.id] === 'ODD_ONE_OUT' || GAMES[gameDataObj.id] === 'MEMORY_AND_SEQUENCING'
              || GAMES[gameDataObj.id] === 'MEMORY'
            ) {
              delete picture.dataValues.isMainPicture;
            } else if (GAMES[gameDataObj.id] === 'AUDITORY_DISCRIMINATION') {
              delete picture.dataValues.isMainPicture;
              delete picture.dataValues.isCorrect;
            }
          });
          const gameDataObject = {
            audio: game_data.audio,
            gameCategory: game_data.game_category,
            id: game_data.id,
            level: game_data.level,
            gamePictures: game_data.game_pictures
          };
          if (GAMES[gameDataObj.id] === 'MEMORY_AND_SEQUENCING') {
            gameDataObject.audios = gameDataObject.audio.split(' ').filter(Boolean);
            delete gameDataObject.audio;
          }
          if (GAMES[gameDataObj.id] === 'AUDITORY_DISCRIMINATION' || GAMES[gameDataObj.id] === 'MEMORY') {
            gameDataObject.audios = (gameDataObject.audio || '').split(' ').filter(Boolean);
            gameDataObject.gamePictures.forEach((picture, index) => {
              picture.dataValues.audio = gameDataObject.audios[index] || '';
            });
            delete gameDataObject.audio;
          }
          if (!gameDataObject.audio) {
            delete gameDataObject.audio;
          }
          if (GAMES[gameDataObj.id] === 'WHAT_DO_I_DO' || GAMES[gameDataObj.id] === 'WHAT_DO_YOU_USE_FOR') {
            gameDataObject.question = game_data.question;
          }
          if (GAMES[gameDataObj.id] === 'IDENTIFICATION') {
            gameDataObject.instructions = game_data.question;
          }
          gameDataObj.gameData.push(gameDataObject);
        });
      }
      gameDataValues.push(gameDataObj);
    });
    return gameDataValues;
  }
}

module.exports = new GameMapper();
