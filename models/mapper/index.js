const fs = require('fs');
const path = require('path');

const basename = path.basename(__filename);

const mappers = {};

fs.readdirSync(__dirname).filter(file => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')).forEach((file) => {
  mappers[file.substr(0, file.length - 3)] = require(`./${file}`);
});

module.exports = mappers;
