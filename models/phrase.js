module.exports = (sequelize, DataType) => sequelize.define('phrase', {
    name: { type: DataType.TEXT },
    categoryId: { type: DataType.INTEGER },
}, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  });
  