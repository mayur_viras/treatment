module.exports = (sequelize, DataType) => sequelize.define('professional', {
  client_id: { type: DataType.INTEGER },
  city_id: { type: DataType.INTEGER },
  address: { type: DataType.TEXT },
  qualification_id: { type: DataType.INTEGER },
  qualification_name: { type: DataType.STRING },
  designation_id: { type: DataType.INTEGER },
  designation_name: { type: DataType.STRING },
  working_at_id: { type: DataType.INTEGER },
  working_at_name: { type: DataType.STRING },
  working_time_from: { type: DataType.STRING },
  working_time_to: { type: DataType.STRING },
  services_name: { type: DataType.STRING },
  days_available: { type: DataType.STRING },
  identity_proof_id: { type: DataType.INTEGER },
  identity_proof_name: { type: DataType.STRING },
  association_reg_name: { type: DataType.STRING },
  association_reg_number: { type: DataType.STRING },
  languages_known: { type: DataType.STRING },
  consultation_charges: { type: DataType.INTEGER },
  consultation_charges_currency_id: { type: DataType.INTEGER },
  consultation_charges_currency: { type: DataType.STRING },
  account_number: { type: DataType.STRING },
  ifsc_code: { type: DataType.STRING },
  extra_details: { type: DataType.STRING }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
