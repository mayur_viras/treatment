module.exports = (db) => {
  db.user.hasMany(db.token);
  db.token.belongsTo(db.user, { foreignKey: 'userId', targetKey: 'id' });

  db.screening_category.hasMany(db.speech_language_domain, { foreignKey: 'id', targetKey: 'screening_id' });
  db.speech_language_domain.belongsTo(db.screening_category, { foreignKey: 'screening_id', targetKey: 'id' });

  // db.screening_category.hasMany(db.atriculation_picture, { foreignKey: 'id', targetKey: 'screening_id' });
  // db.atriculation_picture.belongsTo(db.screening_category, { foreignKey: 'screening_id', targetKey: 'id' });

  db.screening_category.hasMany(db.common_screening, { foreignKey: 'screening_id', targetKey: 'id' });

  db.screening_category.hasMany(db.hearing_screening, { foreignKey: 'id', targetKey: 'screening_id' });
  db.hearing_screening.belongsTo(db.screening_category, { foreignKey: 'screening_id', targetKey: 'id' });
  db.hearing_screening_type.hasMany(db.hearing_screening, { foreignKey: 'screening_type', targetKey: 'id' });
  db.speech_language_domain.hasMany(db.speech_language_screening, { foreignKey: 'domain_id', targetKey: 'id' });
  db.speech_language_screening.belongsTo(db.speech_language_domain, { foreignKey: 'domain_id', targetKey: 'id' });

  db.game_category.hasMany(db.game_data, { foreignKey: 'categoryId', targetKey: 'id' });
  db.game_data.belongsTo(db.game_category, { foreignKey: 'categoryId', targetKey: 'id' });
  db.game.hasMany(db.game_data, { foreignKey: 'gameId', targetKey: 'id' });
  db.game_data.belongsTo(db.game, { foreignKey: 'gameId', targetKey: 'id' });
  db.game_data.hasMany(db.game_pictures, { foreignKey: 'gameDataId', targetKey: 'id' });
  db.game_pictures.belongsTo(db.game_data, { foreignKey: 'gameDataId', targetKey: 'id' });

  db.device.hasMany(db.screening_user, { foreignKey: 'device_id', targetKey: 'id' });
  db.screening_user.belongsTo(db.device, { foreignKey: 'device_id', targetKey: 'id' });

  db.screening_user.hasMany(db.screening_answer, { foreignKey: 'screeningUserId', targetKey: 'id' });
  db.screening_category.hasMany(db.screening_answer, { foreignKey: 'screeningId', targetKey: 'id' });
  db.device.hasMany(db.screening_answer, { foreignKey: 'deviceId', targetKey: 'id' });

  db.device.hasMany(db.articulation_answer, { foreignKey: 'deviceId', targetKey: 'id' });
  db.screening_user.hasMany(db.articulation_answer, { foreignKey: 'screeningUserId', targetKey: 'id' });

  db.country.hasMany(db.state, { foreignKey: 'country_id', targetKey: 'id' });
  db.state.belongsTo(db.country, { foreignKey: 'country_id', targetKey: 'id' });
  db.state.hasMany(db.city, { foreignKey: 'state_id', targetKey: 'id' });
  db.city.belongsTo(db.state, { foreignKey: 'state_id', targetKey: 'id' });

  db.country.hasMany(db.client_user, { foreignKey: 'country_id', targetKey: 'id' });
  db.state.hasMany(db.client_user, { foreignKey: 'state_id', targetKey: 'id' });

  db.client_user.hasMany(db.professional, { foreignKey: 'client_id', targetKey: 'id' });
  db.city.hasMany(db.professional, { foreignKey: 'city_id', targetKey: 'id' });
  db.qualification.hasMany(db.professional, { foreignKey: 'qualification_id', targetKey: 'id' });
  db.designation.hasMany(db.professional, { foreignKey: 'designation_id', targetKey: 'id' });
  db.working_at.hasMany(db.professional, { foreignKey: 'working_at_id', targetKey: 'id' });
  db.identity_proof.hasMany(db.professional, { foreignKey: 'identity_proof_id', targetKey: 'id' });
  db.consultation_currency.hasMany(db.professional, { foreignKey: 'consultation_charges_currency_id', targetKey: 'id' });

  db.professional.belongsTo(db.client_user, { foreignKey: 'client_id', targetKey: 'id' });
  db.professional.belongsTo(db.city, { foreignKey: 'client_id', targetKey: 'id' });
  db.professional.belongsTo(db.qualification, { foreignKey: 'qualification_id', targetKey: 'id' });
  db.professional.belongsTo(db.designation, { foreignKey: 'designation_id', targetKey: 'id' });
  db.professional.belongsTo(db.working_at, { foreignKey: 'working_at_id', targetKey: 'id' });
  db.professional.belongsTo(db.identity_proof, { foreignKey: 'identity_proof_id', targetKey: 'id' });
  db.professional.belongsTo(db.consultation_currency, { foreignKey: 'consultation_charges_currency_id', targetKey: 'id' });

  db.client_user.hasMany(db.student, { foreignKey: 'client_id', targetKey: 'id' });
  db.identity_proof.hasMany(db.student, { foreignKey: 'identity_proof_id', targetKey: 'id', as: 'student_identity_proof' });
  db.student_language.hasMany(db.student, { foreignKey: 'languages_known_id', targetKey: 'id' });
  db.student_course.hasMany(db.student, { foreignKey: 'course_id', targetKey: 'id' });

  db.student.belongsTo(db.client_user, { foreignKey: 'client_id', targetKey: 'id' });
  db.student.belongsTo(db.identity_proof, { foreignKey: 'identity_proof_id', targetKey: 'id', as: 'student_identity_proof' });
  db.student.belongsTo(db.student_language, { foreignKey: 'languages_known_id', targetKey: 'id' });
  db.student.belongsTo(db.student_course, { foreignKey: 'course_id', targetKey: 'id' });

  db.client_user.hasMany(db.appointment, { foreignKey: 'user_id', targetKey: 'id' });
  db.appointment.belongsTo(db.client_user, { foreignKey: 'user_id', targetKey: 'id' });

  db.appointment.belongsTo(db.client_user, { foreignKey: 'professional_id', targetKey: 'id' });
  db.client_user.hasMany(db.appointment, { foreignKey: 'professional_id', targetKey: 'id' });

  db.game_category.hasMany(db.vocabulary_data, { foreignKey: 'categoryId', targetKey: 'id' });
  db.vocabulary_data.belongsTo(db.game_category, { foreignKey: 'categoryId', targetKey: 'id' });

  db.game_category.hasMany(db.vocabulary_item, { foreignKey: 'categoryId', targetKey: 'id' });
  db.vocabulary_item.belongsTo(db.game_category, { foreignKey: 'categoryId', targetKey: 'id' });

  db.client_user.hasMany(db.feedback, { foreignKey: 'userId', targetKey: 'id' });
  db.feedback.belongsTo(db.client_user, { foreignKey: 'userId', targetKey: 'id' });

  db.word_category.hasMany(db.word, { foreignKey: 'categoryId', targetKey: 'id' });
  db.word.belongsTo(db.word_category, { foreignKey: 'categoryId', targetKey: 'id' });

  db.phrase_category.hasMany(db.phrase, { foreignKey: 'categoryId', targetKey: 'id' });
  db.phrase.belongsTo(db.phrase_category, { foreignKey: 'categoryId', targetKey: 'id' });

  db.sentence_category.hasMany(db.sentence, { foreignKey: 'categoryId', targetKey: 'id' });
  db.sentence.belongsTo(db.sentence_category, { foreignKey: 'categoryId', targetKey: 'id' });
  
  return db;
};
