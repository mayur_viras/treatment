module.exports = (sequelize, DataType) => sequelize.define('screening_answer', {
  questionId: { type: DataType.INTEGER },
  screeningId: { type: DataType.INTEGER },
  deviceId: { type: DataType.INTEGER },
  screeningUserId: { type: DataType.INTEGER },
  answer: { type: DataType.BOOLEAN },
  uuid: { type: DataType.TEXT }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
