module.exports = (sequelize, DataType) => sequelize.define('screening_category', {
  name: { type: DataType.STRING(100) },
  type: { type: DataType.SMALLINT },
  youtube_link: { type: DataType.TEXT },
  youtube_link_list_success: { type: DataType.TEXT },
  youtube_link_list_error: { type: DataType.TEXT },
  description: { type: DataType.TEXT },
  image: { type: DataType.TEXT },
  is_having_levels: { type: DataType.BOOLEAN },
  bg_color: { type: DataType.STRING(10) },
  decidingValue: { type: DataType.BOOLEAN },
  decidingValue2: { type: DataType.BOOLEAN }
}, {
  indexes: [{
    name: 'idx_name',
    fields: ['name']
  }],
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
