module.exports = (sequelize, DataType) => sequelize.define('screening_user', {
  device_id: { type: DataType.INTEGER },
  name: { type: DataType.TEXT },
  age_year: { type: DataType.INTEGER },
  age_month: { type: DataType.INTEGER },
  dob: { type: DataType.TEXT }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
