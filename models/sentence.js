module.exports = (sequelize, DataType) => sequelize.define('sentence', {
    name: { type: DataType.TEXT },
    categoryId: { type: DataType.INTEGER },
}, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  });
  