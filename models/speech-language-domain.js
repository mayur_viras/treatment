module.exports = (sequelize, DataType) => sequelize.define('speech_language_domain', {
  name: { type: DataType.TEXT },
  screening_id: { type: DataType.INTEGER }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
