module.exports = (sequelize, DataType) => sequelize.define('speech_language_screening', {
  question: { type: DataType.TEXT },
  domain_id: { type: DataType.INTEGER },
  age_start: { type: DataType.INTEGER },
  age_end: { type: DataType.INTEGER },
  youtube_link: { type: DataType.TEXT },
  isDeleted: { type: DataType.BOOLEAN, defaultValue: false }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
