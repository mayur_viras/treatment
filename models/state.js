module.exports = (sequelize, DataType) => sequelize.define('state', {
  name: { type: DataType.TEXT },
  country_id: { type: DataType.INTEGER },
  isDeleted: { type: DataType.BOOLEAN, defaultValue: false }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
