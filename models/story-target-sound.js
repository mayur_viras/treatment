module.exports = (sequelize, DataType) => sequelize.define('story_target_sound', {
    name: { type: DataType.TEXT },
    categoryId: { type: DataType.INTEGER },
}, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  });
  