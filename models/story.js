module.exports = (sequelize, DataType) => sequelize.define('story', {
    title: { type: DataType.TEXT },
    content: { type: DataType.TEXT },
    audio: { type: DataType.TEXT },
    picture: { type: DataType.TEXT },
    targetSoundId: { type: DataType.INTEGER }
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  });
  