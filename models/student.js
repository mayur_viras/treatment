module.exports = (sequelize, DataType) => sequelize.define('student', {
  client_id: { type: DataType.INTEGER },
  course_id: { type: DataType.INTEGER },
  course_name: { type: DataType.STRING },
  year: { type: DataType.STRING },
  institute: { type: DataType.STRING },
  identity_proof_id: { type: DataType.INTEGER },
  identity_proof_name: { type: DataType.STRING },
  identity_proof: { type: DataType.STRING },
  association_reg_name: { type: DataType.STRING },
  association_reg_number: { type: DataType.STRING },
  languages_known_id: { type: DataType.INTEGER },
  languages_known_name: { type: DataType.STRING },
  extra_details: { type: DataType.STRING }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
