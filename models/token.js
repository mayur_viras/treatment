module.exports = (sequelize, DataType) => {
  const Token = sequelize.define('token', {
    token: { type: DataType.TEXT },
    userId: {
      type: DataType.INTEGER,
      allowNull: false
    }
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  });
  return Token;
};
