module.exports = (sequelize, DataType) => sequelize.define('user', {
  first_name: { type: DataType.STRING(20) },
  last_name: { type: DataType.STRING(25) },
  email: {
    type: DataType.STRING(55),
    allowNull: false,
    unique: true
  },
  contant_no: { type: DataType.STRING(15) },
  password: {
    type: DataType.BLOB,
    allowNull: false
  },
  age: {
    type: DataType.INTEGER,
    allowNull: false
  },
  gender: {
    type: DataType.STRING(15),
    allowNull: false
  }
}, {
  indexes: [{
    name: 'idx_email',
    fields: ['email']
  }],
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
