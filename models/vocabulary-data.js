module.exports = (sequelize, DataType) => sequelize.define('vocabulary_data', {
  categoryId: { type: DataType.INTEGER },
  audio: { type: DataType.TEXT },
  name: { type: DataType.TEXT },
  picture: { type: DataType.TEXT }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});
