module.exports = (sequelize, DataType) => sequelize.define('vocabulary_item', {
    categoryId: { type: DataType.INTEGER },
    name: { type: DataType.TEXT },
    status: { type: DataType.TEXT },
    remark: { type: DataType.TEXT },
  }, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  });
  