module.exports = (sequelize, DataType) => sequelize.define('word', {
    name: { type: DataType.TEXT },
    categoryId: { type: DataType.INTEGER },
    picture: { type: DataType.TEXT },
}, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  });
  