module.exports = {
  PUBLIC_URL: 'http://13.127.40.47:8080/public',
  SERVER_URL: 'http://13.127.40.47:8080',
  errorCodes: {
    101: { status: 401, message: 'Failed to authenticate token' },
    102: { status: 500, message: 'something went wrong, please try again.' },
    103: { status: 200, message: 'Provided values are invalid' },
    104: { status: 404, message: 'Requested resource not found on server' },
    105: { status: 401, message: 'User with given email/username does not exist' },
    106: { status: 401, message: 'provided password is incorrect' },
    107: { status: 401, message: 'Please provide a valid token' },
    108: { status: 200, message: 'A user with same email already exists!' },
    109: { status: 200, message: 'username is taken' },
    110: { status: 200, message: 'A user with same contact number already exists!' },
    111: { status: 200, message: 'User not found' },
    112: { status: 401, message: 'you are not authorized to perform this action' },
    113: { status: 200, message: 'user with given id does not exist' },
    114: { status: 401, message: 'seems like your login has been blocked by admin!!' },
    115: { status: 401, message: 'either you have been logged out or your account has been suspended! please try to login again or contact admin for further details' },
    116: { status: 200, message: 'page number should start from 1' },
    117: { status: 200, message: 'limit should be greater than 0' },
    118: { status: 200, message: 'offset should be greater than or equal to 0' },
    119: { status: 200, message: 'please provide valid articulation picture' },
    120: { status: 200, message: 'please provide valid articulation audio' },
    121: { status: 200, message: 'please provide correct word in of picture' },
    122: { status: 200, message: 'please provide valid start-age' },
    123: { status: 200, message: 'please provide valid end-age' },
    124: { status: 200, message: 'please provide question' },
    125: { status: 200, message: 'please provide youtube-link' },
    126: { status: 200, message: 'please provide valid domain id' },
    127: { status: 200, message: 'start-age must be less than or equal to end-age' },
    128: { status: 200, message: 'please provide valid screening type' },
    129: { status: 200, message: 'please provide valid screening id' },
    130: { status: 200, message: 'please provide valid category name' },
    131: { status: 200, message: 'please provide game id' },
    132: { status: 200, message: 'please provide game category id' },
    133: { status: 200, message: 'please provide level' },
    134: { status: 200, message: 'please provide atleast 2 valid images' },
    135: { status: 200, message: 'please provide valid audio' },
    136: { status: 200, message: 'game is not available' },
    137: { status: 200, message: 'please provide id of correct image' },
    138: { status: 200, message: 'please provide title' },
    139: { status: 200, message: 'please provide language' },
    140: { status: 200, message: 'please provide video' },
    141: { status: 200, message: 'please provide description' },
    142: { status: 200, message: 'requested resource does not exists' },
    143: { status: 200, message: 'please provide device id' },
    144: { status: 200, message: 'please provide user\'s name' },
    145: { status: 200, message: 'please provide users\'s age' },
    146: { status: 200, message: 'please provide user\'s gender' },
    147: { status: 200, message: 'your device id does not exist in our records' },
    148: { status: 200, message: 'month should be less than 12 and greater than or equal to 0' },
    149: { status: 200, message: 'please provide screening-user id' },
    150: { status: 200, message: 'screening user does not exists' },
    151: { status: 200, message: 'please provide question id' },
    152: { status: 200, message: 'please provide valid screening id' },
    153: { status: 200, message: 'please provide valid answer' },
    154: { status: 200, message: 'you can not submit answer for same question again' },
    155: { status: 200, message: 'please provide valid screening user id' },
    156: { status: 200, message: 'number of answers does not match with number of questions' },
    157: { status: 200, message: 'allowed answers are "correct", "incorrect" and "partially correct"' },
    158: { status: 200, message: 'question can\'t be deleted, as it is having some answers recorded' },
    159: { status: 200, message: 'please provide cateogry' },
    160: { status: 200, message: 'please provide user-type ("adult" or "child")' },
    161: { status: 200, message: 'please provide target sound word' }
  },
  answerResponse: {
    2: {
      decidingValue: false,
      data: [{
        start: 0,
        end: 0,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'Based upon your responses your child does not seem to be having any issue with language skills but still if you feel that there are some issues with respect to speech & language, fluency of speech, hearing, behaviors and any other area of communication you can consult qualified professionals. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region. \n\nTo know more about the Speech Language Pathologist and Audiologists click on the following links.'
      }, {
        start: 1,
        end: 1,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'Based upon your responses your child does not seem to be having any big issue with language skills but still if you feel that there are some issues with respect to speech & language, hearing, behaviors and any other area of communication you can consult qualified professionals. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region. \n\nTo know more about the Speech Language Pathologist and Speech Therapy click of the following links.'
      }, {
        start: 2,
        end: 7,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'Based upon your responses “your child seems to be having delay in development of speech and language skills. We suggest you to have detailed evaluation by a qualified Speech Language Pathologist. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region. \n\nTo know more about the Speech Language Pathologist and Speech Therapy click of the following links.'
      }, {
        start: 8,
        end: null,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'Based upon your responses “your child might be having major delay in development of speech and language skills. We suggest that you must go for a “Detailed Assessment” as early as possible by a qualified Speech Language Pathologist. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region. \n\nTo know more about the Speech Language Pathologist and Speech Therapy click of the following links.'
      }]
    },
    3: {
      child: {
        decidingValue: false,
        data: [{
          start: 0,
          end: 0,
          youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
          text: 'Based upon your responses your child is at No risk of hearing loss, but still if you feel there are some issues with your child’s hearing which are not mentioned here or any issue with speech & language, fluency of speech, behaviors or any other area of communication you can consult qualified professionals. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist/ Audiologist in your region. \n\nTo know more about the Speech Language Pathologist/Audiologist click on the following links.'
        }, {
          start: 1,
          end: 3,
          youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
          text: 'Based upon your responses your child may be at risk of hearing loss. We suggest you to have detailed audiological evaluation by a qualified Audiologist to know the status of ears and hearing of your child. \n\nGo to find “Nearby Professionals” option and search for Audiologist in your region. \n\nTo know more about the Audiologist and Hearing Loss click on the following links.'
        }, {
          start: 4,
          end: 8,
          youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
          text: 'Based upon your responses it seems that your child is at high risk of hearing loss. We suggest you to have detailed audiological evaluation by a qualified Audiologist to know the status of ears and hearing of your child. \n\nGo to find “Nearby Professionals” option and search for Audiologist in your region. \n\nTo know more about the Audiologist and Hearing Loss click on the following links.'
        }, {
          start: 9,
          end: null,
          youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
          text: 'Based upon your responses your child is at very high risk of hearing loss. We suggest you to have detailed audiological evaluation by a qualified Audiologist to know the status of ears and hearing of your child. \n\nGo to find “Nearby Professionals” option and search for Audiologist in your region. \n\nTo know more about the Audiologists and Hearing Loss click on the following links.'
        }]
      },
      adult: {
        decidingValue: true,
        data: [{
          start: 0,
          end: 0,
          youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
          text: 'Based upon your responses you are at No risk of hearing loss, but still if you feel there are some issues with your hearing and balance which are not mentioned here you can consult qualified professionals. \n\nGo to find “Nearby Professionals” option and search for Audiologist in your region. \n\nTo know more about the Audiologist and Hearing Loss click on the following links.'
        }, {
          start: 1,
          end: 3,
          youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
          text: 'Based upon your responses you may be at risk of hearing loss. We suggest you to have detailed audiological evaluation by a qualified Audiologist to know the status of your ears and hearing. \n\nGo to find “Nearby Professionals” option and search for Audiologist in your region. \n\nTo know more about the Audiologist and Hearing Loss click on the following links.'
        }, {
          start: 4,
          end: null,
          youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
          text: 'Based upon your responses it seems that you are at risk of hearing loss. We suggest you to have detailed audiological evaluation by a qualified Audiologist to know the status of your ears and hearing. \n\nGo to find “Nearby Professionals” option and search for Audiologist in your region. \n\nTo know more about the Audiologist and Hearing Loss click on the following links.'
        }]
      }
    },
    4: {
      decidingValue: true,
      data: [{
        start: 0,
        end: 0,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'Your child is not at risk for stuttering, but still if you feel there are some issues with speech & language, fluency of speech, hearing, behaviors or any other area of communication you can consult qualified professionals. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist/ Audiologist in your region. \n\nTo know more about the Speech Language Pathologist and Speech therapy click on the following links.'
      }, {
        start: 1,
        end: 3,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'You have responded few questions which are indicative of stuttering. Your child may be at risk of stuttering. We suggest you to have detailed evaluation by a qualified Speech Language Pathologist. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region. \n\nTo know more about the Speech Language Pathologist and Stuttering click on the following links.'
      }, {
        start: 4,
        end: 8,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'You have responded several questions which are indicative of stuttering. It seems that your child is at high risk of stuttering. We suggest you to have detailed evaluation by a qualified Speech Language Pathologist. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region. \n\nTo know more about the Speech Language Pathologist and Stuttering click on the following links.'
      }, {
        start: 8,
        end: null,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'You have responded many questions which are indicative of stuttering. It seems that your child is at very high risk of stuttering. We suggest you to have detailed evaluation by a qualified Speech Language Pathologist. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region. \n\nTo know more about the Speech Language Pathologist and Stuttering click on the following links.'
      }]
    },
    5: {
      decidingValue: true,
      data: [{
        start: 0,
        end: 0,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'Based upon your responses it seems that you have no issues with your Voice, but still if you feel that there is something wrong with your voice which is not covered here you can consult qualified professionals. \n\nGo to find “Nearby Professionals” option” and search for Speech Language Pathologist in your region.'
      }, {
        start: 1,
        end: null,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'You have responded for certain questions which indicates that you have some issues with your Voice. We suggest you to have detailed evaluation by a qualified Speech Language Pathologist. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region. \n\nTo know more about the Speech Language Pathologist and Voice disorders click on the following links.'
      }]
    },
    6: {
      decidingValue: true,
      data: [{
        start: 0,
        end: 0,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'Based upon your responses it looks that your child does not have any behavioral issues, but still if you feel there are some issues which are not mentioned here or any issue with speech & language, fluency of speech, hearing or any other area of communication you can consult qualified professionals. \n\nGo to find “Nearby Professionals” option and search for “Speech Language Pathologist/ Audiologist” in your region. \n\nTo know more about the Speech Language Pathologist and Audiologist click on the following links.'
      }, {
        start: 1,
        end: 4,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'Based upon your responses it looks that your child have few behavioral issues. We suggest you to have detailed evaluation by a qualified Speech Language Pathologist. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region. \n\nTo know more about the Speech Language Pathologist and Behavioral issues click on the following links.'
      }, {
        start: 4,
        end: 8,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'Based upon your responses it looks that your child have many behavioral issues. We suggest you to have detailed evaluation by a qualified Speech Language Pathologist. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region. \n\nTo know more about the Speech Language Pathologist and Behavioral issues click on the following links.'
      }, {
        start: 8,
        end: null,
        youtube: 'https://www.youtube.com/watch?v=IAIGnS9BPKs',
        text: 'Based upon your responses it looks that your child have so many behavioral issues. We suggest you to have detailed evaluation by a qualified Speech Language Pathologist. \n\nGo to find “Nearby Professionals” option and search for Speech Language Pathologist in your region. \n\nTo know more about the Speech Language Pathologist and Behavioral issues click on the following links.'
      }]
    }
  },
  CATEGORY_IDS: { ARTICULATION: 1, SPEECH_AND_LANGUAGE: 2, HEARING: 3, FLUENCY: 4, VOICE: 5, BEHAVIORS: 6 },
  CATEGORY_IDS_REVERSE: { 1: 'ARTICULATION', 2: 'SPEECH_AND_LANGUAGE', 3: 'HEARING', 4: 'FLUENCY', 5: 'VOICE', 6: 'BEHAVIORS' },
  SPEECH_AND_LANGUAGE_DOMAIN_IDS: { 1: 'Receptive Language', 2: 'Expressive Language' },
  HEARING_TYPES: { 1: 'Children', 2: 'Adult' },
  COMMON_SCREENING_IDS: { 4: 'Fluency', 5: 'Voice', 6: 'Behaviors' },
  GAMES: {
    '1': 'IDENTIFICATION',
    '2': 'PICTURE_MATCHING',
    '3': 'SHADOW_MATCHING',
    '4': 'WHAT_DO_I_DO',
    '5': 'MEMORY',
    '6': 'MEMORY_AND_SEQUENCING',
    '7': 'WHAT_DO_YOU_USE_FOR',
    '8': 'GUESS_ME',
    '9': 'AUDITORY_DISCRIMINATION',
    '10': 'FIND_THE_PATH',
    '11': 'PUZZLES',
    '12': 'TRACE_IT',
    '13': 'ODD_ONE_OUT',
    '999': 'VOCABULARY'
  }
};
