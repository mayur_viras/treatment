const constants = require('./constants');
const bugsnag = require('../lib/bugsnag');

class ResponseHandler {
  sendError(res, err) {
    if (constants.errorCodes[err.message]) {
      res.status(constants.errorCodes[err.message].status);
      res.send({
        isError: true,
        message: constants.errorCodes[err.message].message
      });
    } else {
      if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        console.log(err, 'unhandled error');
      } else {
        bugsnag.notify(err);
      }
      // res.status(500);
      console.log(err, 'error data');
      res.send({
        isError: true,
        message: err.message
      });
    }
  }

  sendResponse(res, data, message) {
    res.send({
      isError: false,
      data,
      message: message
    });
  }
}


module.exports = new ResponseHandler();
