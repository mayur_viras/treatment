const express = require('express');
const router = express.Router();
const adminController = require('../controller/admin-2');
const authValidator = require('../validator/auth-validator');
router.get('/country', authValidator.verifyAdmin, adminController.getAllCountries);
router.post('/country', authValidator.verifyAdmin, adminController.addNewCountry);
router.delete('/country/:id', authValidator.verifyAdmin, adminController.removeCountry);
router.put('/country/:id', authValidator.verifyAdmin, adminController.editCountry);

router.get('/state/:id', authValidator.verifyAdmin, adminController.getAllState);
router.post('/state', authValidator.verifyAdmin, adminController.addNewState);
router.delete('/state/:id', authValidator.verifyAdmin, adminController.removeState);
router.put('/state/:id', authValidator.verifyAdmin, adminController.editState);

router.get('/city/:id', authValidator.verifyAdmin, adminController.getAllCity);
router.post('/city', authValidator.verifyAdmin, adminController.addNewCity);
router.delete('/city/:id', authValidator.verifyAdmin, adminController.removeCity);
router.put('/city/:id', authValidator.verifyAdmin, adminController.editCity);

router.get('/params/:param_id', authValidator.verifyAdmin, adminController.getAllParams);
router.post('/params/:param_id', authValidator.verifyAdmin, adminController.addNewParam);
router.delete('/params/:param_id/:id', authValidator.verifyAdmin, adminController.removeParam);
router.put('/params/:param_id/:id', authValidator.verifyAdmin, adminController.editParam);
module.exports = router;
