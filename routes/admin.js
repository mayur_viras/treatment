const express = require('express');
const router = express.Router();
const adminController = require('../controller/admin');
const authValidator = require('../validator/auth-validator');
router.post('/login', adminController.login);
router.post('/logout', authValidator.verifyAdmin, adminController.logout);

router.get('/educational-videos', authValidator.verifyAdmin, adminController.getAllEducationalVideos);
router.post('/educational-video', authValidator.verifyAdmin, adminController.addEducationalVideo);
router.put('/educational-video/:id', authValidator.verifyAdmin, adminController.editEducationalVideo);
router.delete('/educational-video/:id', authValidator.verifyAdmin, adminController.removeEducationalVideo);

router.get('/categories/:param', adminController.getCategories);
router.get('/faq', authValidator.verifyAdmin, adminController.getAllFaq);
router.post('/faq', authValidator.verifyAdmin, adminController.addNewFaq);
router.put('/faq/:id', authValidator.verifyAdmin, adminController.editFaq);
router.delete('/faq/:id', authValidator.verifyAdmin, adminController.removeFaq);

router.get('/blog', authValidator.verifyAdmin, adminController.getAllBlog);
router.post('/blog', authValidator.verifyAdmin, adminController.addNewBlog);
router.put('/blog/:id', authValidator.verifyAdmin, adminController.editBlog);
router.delete('/blog/:id', authValidator.verifyAdmin, adminController.removeBlog);

router.get('/feedback', authValidator.verifyAdmin, adminController.getAllFeedback);
router.delete('/feedback/:id', authValidator.verifyAdmin, adminController.removeFeedback);
router.put('/feedback/visibility/:id', authValidator.verifyAdmin, adminController.changeVisibility);

router.get('/games', authValidator.verifyAdmin, adminController.getAllGames);
router.get('/games/levels', authValidator.verifyAdmin, adminController.getAllLevels);
router.get('/games/data', authValidator.verifyAdmin, adminController.getGameData);
router.delete('/games/data/:id', authValidator.verifyAdmin, adminController.removeGameData);
router.delete('/vocabulary/data/:id', authValidator.verifyAdmin, adminController.removeVocData);
router.post('/games/data', authValidator.verifyAdmin, adminController.addNewGameData);
router.get('/games/categories', authValidator.verifyAdmin, adminController.getAllGameCategories);
router.post('/games/categories', authValidator.verifyAdmin, adminController.addNewCategory);

router.get('/screening-categories', authValidator.verifyAdmin, adminController.getAllScreeningCategories);
router.put('/screening-category/:id', authValidator.verifyAdmin, adminController.editScreeningCategories);
router.put('/screening-category/deciding-value/:id', authValidator.verifyAdmin, adminController.editScreeningDecidingValue);

router.post('/screening-category/articulation', authValidator.verifyAdmin, adminController.addArticulationPicture);
router.put('/screening-category/articulation/:id', authValidator.verifyAdmin, adminController.editArticulationPicture);
router.get('/screening-category/articulation', authValidator.verifyAdmin, adminController.getAllArticulationPictures);
router.delete('/screening-category/articulation/:id', authValidator.verifyAdmin, adminController.removeArticulationItem);

router.get('/screening-category/speech-language/domains', authValidator.verifyAdmin, adminController.getAllSpeechLanDomains);
router.post('/screening-category/speech-language/question', authValidator.verifyAdmin, adminController.addSpeechLanQuestion);
router.put('/screening-category/speech-language/question/:id', authValidator.verifyAdmin, adminController.editSpeechLanQuestion);

router.post('/screening-category/hearing/question', authValidator.verifyAdmin, adminController.addHearingQuestion);
router.put('/screening-category/hearing/question/:id', authValidator.verifyAdmin, adminController.editHearingQuestion);
router.get('/screening-category/hearing/types', authValidator.verifyAdmin, adminController.getAllHearingScreeningTypes);

router.get('/screening-category/common/:id', authValidator.verifyAdmin, adminController.getAllCommonScreeningQuestions);
router.post('/screening-category/common/question/:id', authValidator.verifyAdmin, adminController.addCommonScreeningQuestion);
router.put('/screening-category/common/question/:id', authValidator.verifyAdmin, adminController.editCommonScreeningQuestion);
router.delete('/screening-category/common/question/:categoryId/:id', authValidator.verifyAdmin, adminController.deleteCommonScreeningQuestion);

router.delete('/treatmentVersion/story/:id', authValidator.verifyAdmin, adminController.removeStory);
router.post('/treatmentVersion/story', authValidator.verifyAdmin, adminController.addStory);
router.put('/treatmentVersion/story/:id', authValidator.verifyAdmin, adminController.editStory);

module.exports = router;
