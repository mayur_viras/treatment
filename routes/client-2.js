const express = require('express');
const router = express.Router();
const client2Controller = require('../controller/client-2');
const authValidator = require('../validator/auth-validator');
router.post('/professionals/search', client2Controller.getProfessionals);
router.get('/professionals/:id', client2Controller.getProfessionalById);
router.post('/professionals/appointment', authValidator.verifyClientUser, client2Controller.bookAppointment);
router.post('/professionals/time-slot', authValidator.verifyClientUser, client2Controller.getAllBookedTimeSlot);
module.exports = router;
