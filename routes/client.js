const express = require('express');
const router = express.Router();
const clientController = require('../controller/client');
const adminController = require('../controller/admin');
const admin2Controller = require('../controller/admin-2');
const authValidator = require('../validator/auth-validator');
router.post('/register-device', clientController.register);
router.post('/register-user', clientController.registerNewUser);
router.post('/edit-profile', authValidator.verifyClientUser, clientController.editProfile);
router.post('/professional/edit-profile', authValidator.verifyClientUser, clientController.editProfile);
router.post('/student/edit-profile', authValidator.verifyClientUser, clientController.editProfile);
router.post('/user/login', clientController.login);
router.get('/user/profile', authValidator.verifyClientUser, clientController.getMyProfile);
router.post('/user/change-password', authValidator.verifyClientUser, clientController.changePassword);
router.post('/user/forgot-password', clientController.forgotPassword);
router.post('/user/reset-password', clientController.resetPassword);
router.post('/screening', authValidator.verifyUser, clientController.addScreeningAnswer);
router.post('/articulation-screening', authValidator.verifyUser, clientController.addArticulationAnswer);
router.post('/screening-user', authValidator.verifyUser, clientController.addScreeningUser);
router.get('/screening-user', authValidator.verifyUser, clientController.getAllScreeningUsers);
router.get('/educational-videos', authValidator.verifyUser, adminController.getAllEducationalVideos);
router.get('/educational-videos/search', authValidator.verifyUser, adminController.getAllEducationalVideos);
router.get('/category/:param', authValidator.verifyUser, adminController.getCategories);
router.get('/language/:param', clientController.getLanguages);
router.get('/faq', authValidator.verifyUser, adminController.getAllFaq);
router.get('/blog', authValidator.verifyUser, adminController.getAllBlog);
router.get('/blog/search', authValidator.verifyUser, adminController.getAllBlog);

router.get('/feedback', authValidator.verifyUser, clientController.getAllFeedback);
router.post('/feedback', authValidator.verifyClientUser, clientController.addFeedback);
router.get('/games', clientController.getAllGames);
router.get('/games/data', clientController.getGameData);
router.post('/games/data', clientController.getGameDataByGameId);
router.post('/vocabulary/data', clientController.getVocabularyData);
router.post('/vocabulary/dataForCustomization', clientController.getVocabularyItems);
router.post('/vocabulary/practiceWithAllOrCustomized', clientController.getVocabularyItems);
router.post('/vocabulary/updateItemStatusOrRemark', clientController.updateVocabularyItem);

router.get('/games/categories', clientController.getAllGameCategories);
router.get('/screening-categories', clientController.getAllScreeningCategories);
router.get('/screening-category/articulation', clientController.getAllArticulationPictures);
router.get('/screening-category/speech-language/domains', clientController.getAllSpeechLanDomains);
router.get('/screening-category/hearing/types', clientController.getAllHearingScreeningTypes);
router.get('/screening-category/common/:id', clientController.getAllCommonScreeningQuestions);

router.get('/country', admin2Controller.getAllCountries);
router.get('/state/:id', admin2Controller.getAllState);

router.post('/state/:id', admin2Controller.getAllState);
router.post('/state', admin2Controller.getAllState);

router.get('/city/:id', admin2Controller.getAllCity);
router.post('/city', admin2Controller.getAllCity);

router.get('/user/parameters', clientController.getAllProfessionalAddOns);
router.post('/register-professional', clientController.registerNewProfessional);
router.post('/register-student', clientController.registerNewStudent);

router.get('/treatmentVersion/getWordCategories', clientController.getWordPhraseSentenceCategories);
router.get('/treatmentVersion/getPhraseCategories', clientController.getWordPhraseSentenceCategories);
router.get('/treatmentVersion/getSentenceCategories', clientController.getWordPhraseSentenceCategories);

router.get('/treatmentVersion/getWords', clientController.getWords);
router.get('/treatmentVersion/getPhrases', clientController.getPhraseSentenceData);
router.get('/treatmentVersion/getSentences', clientController.getPhraseSentenceData);

router.post('/treatmentVersion/add/word', clientController.addWord);
router.post('/treatmentVersion/add/:type', clientController.addPhraseSentence);

router.post('/treatmentVersion/target-sound', clientController.addTargetSound);
router.get('/treatmentVersion/getStoryTargetSounds', clientController.getTargetSound);

router.post('/treatmentVersion/story', clientController.addStory);
router.get('/treatmentVersion/getStoriesList', clientController.getStoriesList);
router.get('/treatmentVersion/getStoryContent/:story_id', clientController.getStoryContent);

module.exports = router;
