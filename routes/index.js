const express = require('express');
const router = express.Router();
const user = require('./user.js');
const client = require('./client.js');
const client2 = require('./client-2.js');
const admin = require('./admin.js');
const admin2 = require('./admin-2.js');
const authValidator = require('../validator/auth-validator');

router.use('/', client);
router.use('/', client2);
/**
 * base route of server it check whether user is loggedin
 */
router.use('/', authValidator.verifyAdmin);


/**
 * parent of all user's routes
 */
router.use('/user', user);
router.use('/admin', admin);
router.use('/admin', admin2);

module.exports = router;
