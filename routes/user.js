const express = require('express');
const validate = require('express-validation');
const validation = require('../validator/user');
const authValidator = require('../validator/auth-validator');
const models = require('../models');
const router = express.Router();

const userController = require('../controller/user');

/**
* use for login of users,validate user by

* @api {post} /api/v1/user/login login user
* @apiGroup user
* @apiSuccess {String} isError Error-Status
* @apiSuccess {String} data

* @apiError {String} isError true.
* @apiError {String} message  message to be displayed.

* @apiParamExample {json} Request-Example:
* {
* 	"userName":"ewre",
* 	"password":"dewferf"
* }

*  @apiSuccessExample {json} Success-Response:
*  {
*      "isError": false,
*      "data": {
*          "token": YOUR_TOKEN,
*           "user": {
*                     "firstName": "shuqwdwe",
*                     "lastName": "swiude",
*                     "userName": "dwhudye",
*                     "id": 11,
*                     "email": "dfere.denwuyfdeg@gmail.com",
*                     "role": "admin"
*                 }
*      }
*  }
*  @apiErrorExample {json} Error-Response:
* {
*     "isError": true,
*     "message": "user with given email/username does not exist"
* }
*/
router.post('/login', validate(validation.login), validation.checkAuth, userController.login);

/**
* logout from given device

* @api {post} /api/v1/user/logout user logout from given device
* @apiGroup user
* @apiHeaderExample {json} Header-Example:
*     {
*       "x-access-token": YOUR_TOKEN
*     }
*  @apiSuccessExample {json} Success-Response:
*  {
*      "isError": false,
*      "data": {
*          "message": "you have been logged out from this device"
*      }
*  }
*  @apiErrorExample {json} Error-Response:
* {
*     "isError": true,
*     "message": "user with given email/username does not exist"
* }
*/
router.post('/logout', userController.logout);


/**
* use for user forget password

* @api {post} /api/v1/user/password/forget forget password
* @apiGroup user
* @apiSuccess {String} isError Error-Status
* @apiSuccess {String} data

* @apiError {String} isError true.
* @apiError {String} message  message to be displayed.

* @apiParamExample {json} Request-Example:
* {
* 	"userName":"hdyudgfbcr"
* }

*  @apiSuccessExample {json} Success-Response:
*  {
*      "isError": false,
*      "data": {
*          "message": "Reset password link sent to your email address"
*      }
*  }
*  @apiErrorExample {json} Error-Response:
* {
*     "isError": true,
*     "message": "user with given email/username does not exist"
* }
*/
router.post('/password/forget', validate(validation.forgetPassword), userController.forgetPassword);


/**
* use for user reset password
* @api {put} /api/v1/user/password/reset reset password
* @apiGroup user
* @apiSuccess {String} isError Error-Status
* @apiSuccess {String} data

* @apiError {String} isError true.
* @apiError {String} message  message to be displayed.

* @apiParamExample {json} Request-Example:
* {
* 	"password":"abcdef",
*   "passwordCode": "111111111"
* }

*  @apiSuccessExample {json} Success-Response:
*  {
*      "isError": false,
*      "data": {
*          "message": "your password has been changed"
*      }
*  }
*  @apiErrorExample {json} Error-Response:
* {
*     "isError": true,
*     "message": "user with given email/username does not exist"
* }
*/
router.put('/password/reset', validate(validation.resetPassword), userController.resetPassword);


/**
* use for user change password
* @api {put} /api/v1/user/password/change change password
* @apiGroup user
* @apiHeaderExample {json} Header-Example:
*     {
*       "x-access-token": YOUR_TOKEN
*     }
* @apiSuccess {String} isError Error-Status
* @apiSuccess {String} data

* @apiError {String} isError true.
* @apiError {String} message  message to be displayed.

* @apiParamExample {json} Request-Example:
* {
* 	"oldPassword":"abcdef",
*   "newPassword": "abcdef123"
* }

*  @apiSuccessExample {json} Success-Response:
*  {
*      "isError": false,
*      "data": {
*          "message": "your password has been changed successfully"
*      }
*  }
*  @apiErrorExample {json} Error-Response:
* {
*     "isError": true,
*     "message": "user with given email/username does not exist"
* }
*/
router.put('/password/change', validate(validation.changePassword), userController.changePassword);

/**
* use for user get his/her profile
* @api {get} /api/v1/user/profile get user profile
* @apiGroup user
* @apiHeaderExample {json} Header-Example:
*     {
*       "x-access-token": YOUR_TOKEN
*     }
* @apiSuccess {String} isError Error-Status
* @apiSuccess {String} data

* @apiError {String} isError true.
* @apiError {String} message  message to be displayed.
* @apiSuccessExample {json} Success-Response:
*  {
*      "isError": false,
*      "data": {
*          "user": {
*              "firstName": "dwde",
*              "lastName": "eddewf",
*              "userName": "dyugehy",
*              "phone": "",
*              "role": "admin"
*          }
*      }
*  }
*  @apiErrorExample {json} Error-Response:
* {
*     "isError": true,
*     "message": "something went wrong"
* }
*/
router.get('/profile', userController.getProfile);

module.exports = router;
