const db = require('./models');
module.exports = async () => {
  console.log('setting up basic data');
  // setup categories
  const allCategories = [{
    id: 1,
    name: 'Articulation',
    type: 1,
    youtube_link: 'https://www.youtube.com/watch?v=T_CSrz7v1hw',
    description: 'Articulation Screening description would come here, Screening will have set of selected 18 words, which will remain same across trials.',
    image: 'http://mommyspeechtherapy.com/wp-content/uploads/2012/08/feature_image-750x500.jpg',
    is_having_levels: false,
    bg_color: '#ffffff',
    decidingValue: true
  }, {
    id: 2,
    name: 'Speech And Language',
    type: 2,
    youtube_link: 'https://www.youtube.com/watch?v=QPpKujheypE',
    description: 'Speech And Language Screening description would come here.',
    image: 'https://www.home-speech-home.com/images/speech-language-screeners.png',
    is_having_levels: true,
    bg_color: '#ffffff',
    decidingValue: true
  }, {
    id: 3,
    name: 'Hearing',
    type: 2,
    youtube_link: 'https://www.youtube.com/watch?v=Sqdv-FUr9AI',
    description: 'Hearing Screening description would come here.',
    image: 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201707/newborn-hearing-screening-647_071917055655.jpg',
    is_having_levels: true,
    bg_color: '#ffffff',
    decidingValue: true
  }, {
    id: 4,
    name: 'Fluency',
    type: 2,
    youtube_link: 'https://www.youtube.com/watch?v=R0X3IH6yar4',
    description: 'Fluency Screening description would come here.',
    image: 'http://www.readingrockets.org/sites/default/files/atoz_fluency_1.jpg',
    is_having_levels: false,
    bg_color: '#ffffff',
    decidingValue: true
  }, {
    id: 5,
    name: 'Voice',
    type: 2,
    youtube_link: 'https://www.youtube.com/watch?v=dbLPNv__29s',
    description: 'Voice Screening description would come here.',
    image: 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201707/newborn-hearing-screening-647_071917055655.jpg',
    is_having_levels: false,
    bg_color: '#ffffff',
    decidingValue: true
  }, {
    id: 6,
    name: 'Behaviors',
    type: 2,
    youtube_link: 'https://www.youtube.com/watch?v=Sqdv-FUr9AI',
    description: 'Behaviors Screening description would come here.',
    image: 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201707/newborn-hearing-screening-647_071917055655.jpg',
    is_having_levels: false,
    bg_color: '#ffffff',
    decidingValue: true
  }];
  const categories = await db.screening_category.findAll({});
  console.log(categories.length, 'categories');
  if (categories && !categories.length) {
    for (let i = 0; i < allCategories.length; i += 1) {
      await db.screening_category.create(allCategories[i]);
    }
  }

  // setup domains
  const domains = await db.speech_language_domain.findAll({});
  console.log(domains.length, 'domains');
  if (domains && !domains.length) {
    const allDomains = [{ id: 1, name: 'Receptive Language', screening_id: 2 }, { id: 2, name: 'Expressive Language', screening_id: 2 }];
    for (let i = 0; i < allDomains.length; i += 1) {
      await db.speech_language_domain.create(allDomains[i]);
    }
  }


  // setup hearing screening types
  const hearingScreeningTypes = await db.hearing_screening_type.findAll({});
  console.log(hearingScreeningTypes.length, 'hearingScreeningTypes');
  if (hearingScreeningTypes && !hearingScreeningTypes.length) {
    const allTypes = [{ id: 1, name: 'Children' }, { id: 2, name: 'Adult' }];
    for (let i = 0; i < allTypes.length; i += 1) {
      await db.hearing_screening_type.create(allTypes[i]);
    }
  }

  // setup hearing screening types
  const games = await db.game.findAll({});
  console.log(games.length, 'games');
  if (games && !games.length) {
    const allGames = [
      { id: 1, name: 'Identification' },
      { id: 2, name: 'Picture Matching' },
      { id: 3, name: 'Shadow Matching' },
      { id: 4, name: 'What do I do?' },
      { id: 5, name: 'Memory' },
      { id: 6, name: 'Memory and Sequencing' },
      { id: 7, name: 'What do you use for?' },
      { id: 8, name: 'Guess me' },
      { id: 9, name: 'Auditory Discrimination' },
      { id: 10, name: 'Find the path' },
      { id: 11, name: 'Puzzles' },
      { id: 12, name: 'Trace it' },
      { id: 13, name: 'Odd one out' }
    ];
    for (let i = 0; i < allGames.length; i += 1) {
      await db.game.create(allGames[i]);
    }
  }
};
