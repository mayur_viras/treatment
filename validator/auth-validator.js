const _ = require('lodash');
const jwt = require('jsonwebtoken');
const { jwtConfig } = require('../config');
const respGenerator = require('../response/json-response');

class AuthValidator {
  async verifyUser(req, res, next) {
    const token = req.headers['x-access-token'];
    try {
      const user = await jwt.verify(token, jwtConfig.secret);
      if (!user || user.type !== 'device') {
        throw new Error(112);
      }
      req.user = user;
      return next();
    } catch (err) {
      if (parseInt(err.message)) {
        return respGenerator.sendError(res, new Error(115));
      } else if (!_.includes(jwtConfig.path, req.path)) {
        return respGenerator.sendError(res, new Error((token) ? 101 : 107));
      }
      return next();
    }
  }

  async verifyClientUser(req, res, next) {
    const token = req.headers['x-access-token'];
    try {
      const user = await jwt.verify(token, jwtConfig.secret);
      if (!user || user.type !== 'user') {
        throw new Error(112);
      }
      req.user = user;
      return next();
    } catch (err) {
      if (parseInt(err.message)) {
        return respGenerator.sendError(res, new Error(115));
      } else if (!_.includes(jwtConfig.path, req.path)) {
        return respGenerator.sendError(res, new Error((token) ? 101 : 107));
      }
      return next();
    }
  }

  async verifyAdmin(req, res, next) {
    const token = req.headers['x-access-token'];
    try {
      const user = await jwt.verify(token, jwtConfig.secret);
      if (!user || user.type !== 'Admin') {
        throw new Error(112);
      }
      req.user = user;
      return next();
    } catch (err) {
      if (parseInt(err.message)) {
        return respGenerator.sendError(res, new Error(115));
      } else if (!_.includes(jwtConfig.path, req.path)) {
        return respGenerator.sendError(res, new Error((token) ? 101 : 107));
      }
      return next();
    }
  }

}

module.exports = new AuthValidator();
