const respGenerator = require('../response/json-response.js');
const Joi = require('joi');

module.exports = {
  login: { headers: { authorization: Joi.string().required() } },
  forgetPassword: { body: { userName: Joi.string().required().label('user name') } },
  resetPassword: {
    body: {
      password: Joi.string().required().min(8).max(16)
      .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d$@$!%*?&]{8,16}/).label('new password'),
      passwordCode: Joi.string().required().label('code')
    }
  },
  changePassword: {
    body: {
      oldPassword: Joi.string().required().label('old password'),
      newPassword: Joi.string().required().min(8).max(16)
      .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d$@$!%*?&]{8,16}/).label('new password')
    }
  }
};

module.exports.checkAuth = async (req, res, next) => {
  if (!req.headers.authorization || !req.headers.authorization.startsWith('Basic ')) {
    return res.status(401).json({ isError: true, message: 'basic authorization failed' });
  }
  const auth = req.headers.authorization.split(' ');
  const plain_auth = new Buffer(auth[1], 'base64').toString();
  const creds = plain_auth.split(':');
  const userName = creds[0];
  const password = creds[1];
  req.body = { userName, password };
  if (!userName) {
    return res.status(400).json({ isError: true, message: 'username is required' });
  } else if (!password) {
    return res.status(400).json({ isError: true, message: 'password is required' });
  }
  next();
};
